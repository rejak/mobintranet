import 'dart:core';

import 'package:mobintranet/model/Db8Model.dart';
import 'package:mobintranet/util/DbImages.dart';
import 'package:mobintranet/util/models/KeloladCutiModels.dart';
import 'package:mobintranet/util/images.dart';

import 'DbColors.dart';

List<DB8Scene> getScene() {
  List<DB8Scene> categories = List<DB8Scene>();
  categories.add(DB8Scene(hukum, "Hukum",fmerah1,"hukum"));
  categories.add(DB8Scene(sdm, "SDM",forange1,"sdm"));
  categories.add(DB8Scene(surat, "Persuratan",fhijau1,"surat"));
  // categories.add(DB8Scene(db8_ic_alarm, "Get up"));

  return categories;
}

List<DB8Rooms> getRooms() {
  List<DB8Rooms> categories = List<DB8Rooms>();
  categories.add(DB8Rooms("Ikatan Widyaiswara Indonesia (IWI) Bertransformasi Menjadi Asosiasi Profesi Widyaiswara Indonesia (APWI)", pengumuman1, "Jakarta – Ikatan Widyaiswara Indonesia (IWI) bertransformasi menjadi Asosiasi Profesi Widyaiswara Indonesia (APWI)."));
  categories.add(DB8Rooms("LAN Sukses Raih WTP 13 Kali Berturut-turut", pengumuman2, "Jakarta – Lembaga Administrasi Negara (LAN) kembali meraih Predikat Wajar Tanpa Pengecualian (WTP),"));
  categories.add(DB8Rooms("LAN Gelar Kongres ke – VIII Ikatan Widyaiswara Indonesia", pengumuman3, "Jakarta – Lembaga Administrasi Negara (LAN) bekerjasama dengan Dewan Pengurus Pusat Ikatan Widyaiswara Indonesia (DPP-IWI) menggelar Kongres ke-VIII IWI Tahun 2020 di Aula Prof. Dr. Agus Dwiyanto, MPA, Kantor LAN Jalan Veteran 10, Jakarta Pusat, Jumat (18/9)."));

  return categories;
}

List<T1Model> getListings() {
  List<T1Model> list = List<T1Model>();
  list.add(T1Model("John", "12 min ago", "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", "ceo", t1_ic_user1));
  list.add(T1Model("John", "12 min ago", "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", "ceo", t1_ic_user2));
  list.add(T1Model("John", "12 min ago", "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", "ceo", t1_ic_user3));
  list.add(T1Model("John", "12 min ago", "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", "ceo", t1_ic_user1));
  list.add(T1Model("John", "12 min ago", "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", "ceo", t1_ic_user2));
  list.add(T1Model("John", "12 min ago", "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", "ceo", t1_ic_user3));
  list.add(T1Model("John", "12 min ago", "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", "ceo", t1_ic_user1));
  list.add(T1Model("John", "12 min ago", "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", "ceo", t1_ic_user2));
  list.add(T1Model("John", "12 min ago", "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", "ceo", t1_ic_user3));
  list.add(T1Model("John", "12 min ago", "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", "ceo", t1_ic_user1));
  list.add(T1Model("John", "12 min ago", "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", "ceo", t1_ic_user2));
  list.add(T1Model("John", "12 min ago", "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", "ceo", t1_ic_user3));
  list.add(T1Model("John", "12 min ago", "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", "ceo", t1_ic_user1));
  list.add(T1Model("John", "12 min ago", "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", "ceo", t1_ic_user2));
  list.add(T1Model("John", "12 min ago", "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", "ceo", t1_ic_user3));
  list.add(T1Model("John", "12 min ago", "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", "ceo", t1_ic_user1));
  list.add(T1Model("John", "12 min ago", "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", "ceo", t1_ic_user2));
  list.add(T1Model("John", "12 min ago", "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", "ceo", t1_ic_user3));
  list.add(T1Model("John", "12 min ago", "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", "ceo", t1_ic_user1));
  list.add(T1Model("John", "12 min ago", "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", "ceo", t1_ic_user2));
  list.add(T1Model("John", "12 min ago", "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", "ceo", t1_ic_user3));
  list.add(T1Model("John", "12 min ago", "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", "ceo", t1_ic_user1));
  list.add(T1Model("John", "12 min ago", "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", "ceo", t1_ic_user2));
  list.add(T1Model("John", "12 min ago", "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", "ceo", t1_ic_user3));
  list.add(T1Model("John", "12 min ago", "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", "ceo", t1_ic_user1));
  list.add(T1Model("John", "12 min ago", "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", "ceo", t1_ic_user2));
  list.add(T1Model("John", "12 min ago", "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", "ceo", t1_ic_user3));
  return list;
}
