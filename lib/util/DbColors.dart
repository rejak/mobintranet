import 'package:flutter/material.dart';

//<!--Dashboard 1-->
const db1_colorPrimary = Color(0xFFeb4b51);
const db1_white = Color(0xFFffffff);
const db1_textColorPrimary = Color(0xFF212121);
const db1_textColorSecondary = Color(0xFF757575);
const db1_grey = Color(0xFFB9B9B9);
const db1_yellow = Color(0xFFFBC02D);
const db1_purple = Color(0xFFB88DDD);
const db1_green = Color(0xFFC2DB77);
const db1_orange = Color(0xFFF5D270);
const db1_dark_blue = Color(0xFF5FACC9);
//<integer-array name="db2_category_colors">
//<item>@color/db2_purple</item>
//<item>@color/db2_green</item>
//<item>@color/db2_orange</item>
//<item>@color/db2_darkblue</item>
//<item>@color/db2_darkpurple</item>
//<item>@color/db2_darkgreen</item>
//</integer-array>

const db2_purple = Color(0xFFB88DDD);
const db2_green = Color(0xFFC2DB77);
const db2_orange = Color(0xFFF5D270);
const db2_dark_blue = Color(0xFF5FACC9);
const db2_dark_purple = Color(0xFFB285CC);
const db2_dark_green = Color(0xFFB6DD6E);

//<!--Dashboard 2-->
const db2_colorPrimary = Color(0xFFFF7C6E);
const db2_colorSecondary = Color(0xFFF5317F);
const db2_textColorPrimary = Color(0xFF212121);
const db2_textColorSecondary = Color(0xFF757575);
const db2_white = Color(0xFFffffff);
const db2_black = Color(0xFF000000);

//<!--Dashboard 3-->
const db3_colorPrimary = Color(0xFFFF7C6E);
const db3_colorSecondary = Color(0xFFF5317F);
const db3_textColorPrimary = Color(0xFF212121);
const db3_textColorSecondary = Color(0xFF757575);
const db3_white = Color(0xFFffffff);
const db3_black = Color(0xFF000000);
const db3_grey = Color(0xFFB9B9B9);
const db3_white_trans = Color(0xFFF1F1F1);

//<!--Dashboard 4-->
const db4_colorPrimary = Color(0xFF130925);
const db4_colorSecondary = Color(0xFF130925);
const db4_textColorPrimary = Color(0xFF130925);
const db4_textColorSecondary = Color(0xFF757575);
const db4_white = Color(0xFFffffff);
const db4_black = Color(0xFF000000);
const db4_grey = Color(0xFFB9B9B9);
const db4_white_trans = Color(0xFFF1F1F1);
const db4_LayoutBackgroundWhite = Color(0xFFF6F7FA);
const db4_cat_1 = Color(0xFF45c7db);
const db4_cat_2 = Color(0xFF510AD7);
const db4_cat_3 = Color(0xFFe43649);
const db4_cat_4 = Color(0xFFf4b428);
const db4_cat_5 = Color(0xFF22ce9a);
const db4_cat_6 = Color(0xFF203afb);
const db4_ColorPrimaryLight = Color(0x505104D7);

//<!--Dashboard 5-->
const db5_colorPrimary = Color(0xFF2F95A1);
const db5_colorSecondary = Color(0xFF2F95A1);
const db5_textColorPrimary = Color(0xFF2F95A1);
const db5_textColorSecondary = Color(0xFF757575);
const db5_white = Color(0xFFffffff);
const db5_black = Color(0xFF000000);
const db5_grey = Color(0xFFB9B9B9);
const db5_viewColor = Color(0xFFEEEEF1);
const db5_yellow = Color(0xFFf4b428);
const db5_white_trans = Color(0xFFF1F1F1);
const db5_hotel = Color(0xFFF0F5F7);
const db5_flight = Color(0xFFFFF4F4);
const db5_food = Color(0xFFFFF6F1);
const db5_event = Color(0xFFF3F1FA);
const db5_icon_color = Color(0xFF747474);
const db5_black_trans = Color(0xFF56303030);
const db5_light_blue = Color(0xFFD6DBF0);

//Dashboard 6
const db6_colorPrimary = Color(0xFF0060D5);
const db6_colorPrimaryDark = Color(0xFF0060D5);
const db6_colorSecondary = Color(0xFF0c82DD);
const db6_textColorPrimary = Color(0xFF0060D5);
const db6_textColorSecondary = Color(0xFF757575);
const db6_white = Color(0xFFffffff);
const db6_black = Color(0xFF000000);
const db6_grey = Color(0xFFB9B9B9);
const db6_yellow = Color(0xFFf4b428);
const db6_viewColor = Color(0xFFEEEEF1);
const db6_layout_background = Color(0xFFF6F7FA);
const db6_white_trans = Color(0xFFF1F1F1);

// Dashboard 7
const db7_colorPrimary = Color(0xFF19D077);
const db7_colorPrimaryDark = Color(0xFF19D077);
const db7_colorSecondary = Color(0xFF0c82DD);
const db7_textColorPrimary = Color(0xFF130925);
const db7_textColorSecondary = Color(0xFF757575);
const db7_white = Color(0xFFFFFFFF);
const db7_viewColor = Color(0xFFDADADA);
const db7_layout_background = Color(0xFFF6F7FA);
const db7_light_green = Color(0xFFE2F5E9);
const db7_light_blue = Color(0xFFE9EAFF);
const db7_dark_blue = Color(0xFF5B8CFF);
const db7_dark_yellow = Color(0xFFFFAB35);
const db7_dark_red = Color(0xFFFF7670);
const db7_light_yellow = Color(0xFFFEF2E5);
const db7_light_red = Color(0xFFFEE9EA);
const opTextSecondaryColor = Color.fromRGBO(53,153,218,1);

// Dashboard 8
const db8_colorPrimary = Color.fromRGBO(1, 119, 204, 1);
const db8_colorPrimaryDark = Color(0xFFB79A7F);
const db8_colorSecondary = Color(0xFF0c82DD);
const db8_textColorPrimary = Color(0xFF444444);
const db8_textColorSecondary = Color(0xFF757575);
const db8_white = Color(0xFFffffff);
const db8_black = Color(0xFF000000);
const db8_grey = Color(0xFFF2F2F2);
const db8_viewColor = Color(0xFFEEEEF1);
const db8_layout_background = Color(0xFFF6F7FA);

const merah = Color.fromRGBO(255, 15, 51,1);
const hijau = Color.fromRGBO(15, 255, 95,1);
const kuning = Color.fromRGBO(255, 227, 15,1);
const orange = Color.fromRGBO(255, 111, 15,1);
const biru = Color.fromRGBO(15, 147, 255,1);

//pastel
const pmerah = Color.fromRGBO(255, 178, 186,1);
const phijau = Color.fromRGBO(186, 255, 200,1);
const pkuning = Color.fromRGBO(254, 255, 186,1);
const porange = Color.fromRGBO(255, 223, 185,1);
const pbiru = Color.fromRGBO(187, 225, 255,1);

//flatcolor
const fhijau1 = Color.fromRGBO(28, 188, 155,1);
const fhijau2 = Color.fromRGBO(47, 204, 113,1);
const fhijau3 = Color.fromRGBO(21, 161, 132,1);
const fhijau4 = Color.fromRGBO(37, 174, 96,1);
const fbiru1 = Color.fromRGBO(53, 153, 218,1);
const fbiru2 = Color.fromRGBO(42, 127, 185,1);
const fungu = Color.fromRGBO(155, 91, 182,1);
const fungu2 = Color.fromRGBO(144, 70, 174,1);
const fkuning1 = Color.fromRGBO(241, 196, 14,1);
const fkuning2 = Color.fromRGBO(244, 156, 18,1);
const forange1 = Color.fromRGBO(211, 84, 3,1);
const forange2 = Color.fromRGBO(211, 84, 3,1);
const fmerah1 = Color.fromRGBO(234, 91, 74,1);
const fmerah2 = Color.fromRGBO(192, 58, 43,1);
const fabu1 = Color.fromRGBO(236, 240, 241,1);
const fabu2 = Color.fromRGBO(189, 195, 199,1);
const fabu3 = Color.fromRGBO(149, 165, 166,1);
const fabu4 = Color.fromRGBO(126, 140, 141,1);

const dbShadowColor = Color(0x95E9EBF0);
const social_app_background_color = Color(0xFFF8F7F7);