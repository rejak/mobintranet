import 'package:flutter/material.dart';
import 'package:mobintranet/util/DbConstant.dart';
import 'package:mobintranet/util/colors.dart';

back(var context) {
  Navigator.pop(context);
}

Text headerText(var text) {
  return Text(
    text,
    maxLines: 2,
    style: TextStyle(
        fontFamily: fontBold, fontSize: 22, color: t1_textColorPrimary),
  );
}
