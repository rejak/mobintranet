import 'package:mobintranet/util/constant.dart';

const bg_bottom_bar = "images/splashScreen/bg_bottom_bar.png";
const card = "images/splashScreen/card.png";
const chip = "images/splashScreen/chip.png";
const ic_app_background = "$BaseUrl/images/shophop/ic_app_background.png";
const ic_app_icon = "images/splashScreen/logo1.png";
const ic_app_icon2 = "images/splashScreen/logo2.png";
const ic_reward_back = "images/splashScreen/ic_reward_back.jpg";
const ic_scratch_pattern = "images/splashScreen/ic_scratch_pattern.png";
const ic_walk = "images/splashScreen/ic_walk.jpeg";
const splash_bg = "images/splashScreen/splash_bg.png";
const splash_img = "images/splashScreen/splash_img.png";
const whitegradient = "images/splashScreen/whitegradient.png";
const ic_walk_1 = "$BaseUrl/images/shophop/ic_walk_1.png";
const ic_walk_2 = "$BaseUrl/images/shophop/ic_walk_2.png";
const ic_walk_3 = "$BaseUrl/images/shophop/ic_walk_3.png";
const t1_ic_user1 = "$BaseUrl/images/theme1/t1_ic_user1.png";
const t1_ic_user2 = "$BaseUrl/images/theme1/t1_ic_user2.png";
const t1_ic_user3 = "$BaseUrl/images/theme1/t1_ic_user3.png";
const ic_user = "images/splashScreen/ic_user.png";
const sh_user_placeholder = "images/splashScreen/user.png";
const sh_settings = "images/splashScreen/settings.png";
const sh_ic_heart = "images/splashScreen/ic_heart.svg";
const sh_ic_home = "images/splashScreen/ic_home.svg";
const sh_user = "images/splashScreen/ic_user.svg";
const sh_ic_cart = "images/splashScreen/ic_cart.svg";
const sh_radar = "images/splashScreen/radar.gif";

const t1_home = "images/homeSdm/t1_home.svg";
const t1_notification = "images/homeSdm/t1_notification.svg";
const t1_settings = "images/homeSdm/t1_settings.svg";
const t1_user = "images/homeSdm/t1_user.svg";

const ic_search = "images/homeSdm/ic_search.png";

// add cuti
const qibus_gif_bell = "images/homeSdm/addCuti/qibus_gif_bell.gif";