import 'package:flutter/material.dart';
import 'dart:ui';

const sh_colorPrimary = Color(0xFFf17015);
const sh_colorPrimaryDark = Color(0xFFf17015);
const sh_colorAccent = Color(0xFFf17015);

const sh_textColorPrimary = Color(0xFF212121);
const sh_textColorSecondary = Color(0xFF757575);
const sh_white = Color(0xFFffffff);
const sh_semi_white = Color(0xFFF8F7F7);
const sh_black = Color(0xFF000000);
const sh_view_color = Color(0xFFeaeaea);
const sh_transparent = Color(0xFF00000000);
const db8_viewColor = Color(0xFFEEEEF1);

const sh_background_color = Color(0xFFFCFDFD);
const sh_editText_background = Color(0xFFF1F1F1);
const sh_tomato = Color(0xFFFF6347);
const sh_dots_color = Color(0xFFA5A5A5);
const sh_editText_background_active = Color(0xFFFDE9DA);
const sh_cat_1 = Color(0xFFFA4352);
const sh_cat_2 = Color(0xFF34B5C8);
const sh_cat_3 = Color(0xFFFED76D);
const sh_cat_4 = Color(0xFF0C5A93);
const sh_cat_5 = Color(0xFF3CA69B);
const sh_sidebar_background = Color(0xFFEFEEEF);
const sh_favourite_background = Color(0xFFFFEBEC);
const sh_favourite_unselected_background = Color(0xFFECECEC);
const sh_yellow = Color(0xFFFEBA39);
const sh_light_gray = Color(0xFFECECEC);
const sh_green = Color(0xFF66953A);
const sh_red = Color(0xFFF61929);
const sh_light_green = Color(0xFF81C049);
const sh_more_infoHeading_background = Color(0xFFDDDDDD);
const sh_more_infoValue_background = Color(0xFFF3F3F3);
const sh_item_background = Color(0xFFEFF3F6);
const sh_bg_4star = Color(0xFF78c639);
const sh_light_grey = Color(0xFFFAFAFA);
const sh_checkbox_color = Color(0xFFDFDFDF);
const sh_itemText_background = Color(0xFFF8F8F8);
const sh_track_green = Color(0xFF64B931);
const sh_track_yellow = Color(0xFFECC134);
const sh_track_red = Color(0xFFF61929);
const sh_track_grey = Color(0xFFD3D3D3);
const sh_radiobuttonTint = Color(0xFF4353FA);
const sh_scratch_start_gradient = Color(0xFFe5e5e5);
const sh_scratch_end_gradient = Color(0xFFcccccc);
const sh_shadow_color = Color(0X95E9EBF0);

// const fhijau1 = Color.fromRGBO(28, 188, 155,1);
// const fhijau2 = Color.fromRGBO(47, 204, 113,1);
// const fhijau3 = Color.fromRGBO(21, 161, 132,1);
// const fhijau4 = Color.fromRGBO(37, 174, 96,1);
// const fbiru1 = Color.fromRGBO(53, 153, 218,1);
// const fbiru2 = Color.fromRGBO(42, 127, 185,1);
// const fungu = Color.fromRGBO(155, 91, 182,1);
// const fungu2 = Color.fromRGBO(144, 70, 174,1);
// const fkuning1 = Color.fromRGBO(241, 196, 14,1);
// const fkuning2 = Color.fromRGBO(244, 156, 18,1);
// const forange1 = Color.fromRGBO(211, 84, 3,1);
// const forange2 = Color.fromRGBO(211, 84, 3,1);
// const fmerah1 = Color.fromRGBO(234, 91, 74,1);
// const fmerah2 = Color.fromRGBO(192, 58, 43,1);
// const fabu1 = Color.fromRGBO(236, 240, 241,1);
// const fabu2 = Color.fromRGBO(189, 195, 199,1);
// const fabu3 = Color.fromRGBO(149, 165, 166,1);
// const fabu4 = Color.fromRGBO(126, 140, 141,1);

const t1_colorPrimary = Color(0XFFff8080);
const t1_colorPrimary_light = Color(0XFFFFEEEE);
const t1_colorPrimaryDark = Color(0XFFff8080);
const t1_colorAccent = Color(0XFFff8080);

const t1_textColorPrimary = Color(0XFF333333);
const t1_textColorSecondary = Color(0XFF747474);

const t1_app_background = Color(0XFFf8f8f8);
const t1_view_color = Color(0XFFDADADA);

const t1_sign_in_background = Color(0XFFDADADA);

const t1_white = Color(0XFFffffff);
const t1_icon_color = Color(0XFF747474);
const t1_selected_tab = Color(0XFFFCE9E9);
const t1_red = Color(0XFFF10202);
const t1_blue = Color(0XFF1D36C0);
const t1_edit_text_background = Color(0XFFE8E8E8);
const t1_shadow = Color(0X70E2E2E5);
var t1White = materialColor(0XFFFFFFFF);
var t1TextColorPrimary = materialColor(0XFF212121);
const shadow_color = Color(0X95E9EBF0);
const t1_color_primary_light = Color(0XFFFCE8E8);
const t1_bg_bottom_sheet = Color(0XFFFFF1F1);

const sdTextPrimaryColor = Color(0xFF000000);
const sdTextSecondaryColor = Color(0xFF757575);

const sdPrimaryColor = Color(0xFF3281FF);
const sdSecondaryColorRed = Color(0xFFFF5C5C);
const sdSecondaryColorYellow = Color(0xFFFFA635);
const sdSecondaryColorGreen = Color(0xFF5DE094);
const sdIconColor = Color(0xFFA6A7AA);
const sdShadowColor = Color(0x95E9EBF0);
const sdAppBackground = Color(0xFFf8f8f8);
const sdViewColor = Color(0xFFDADADA);

const qIBus_colorPrimary = Color(0xFFeb4b51);
const qIBus_colorPrimaryDark = Color(0xFFeb4b51);
const qIBus_colorPrimaryRipple = Color(0xFFF57074);
const qIBus_colorAccent = Color(0xFFeb4b51);

const qIBus_blue = Color(0xFF4C92C0);
const qIBus_red = Color(0xFFe51415);
const qIBus_white = Color(0xFFFFFFFF);
const qIBus_gray = Color(0xFFE2E2E2);
const qIBus_rating = Color(0xFFFFC107);

const qIBus_view_color = Color(0xFFDADADA);
const qIBus_app_background = Color(0xFFF8F7F7);
const qIBus_color_check = Color(0xFF139B18);
const qIBus_color_link_blue = Color(0xFF1887f9);

const qIBus_textHeader = Color(0xFF464545);
const qIBus_textChild = Color(0xFF747474);
const qIBus_light_grey = Color(0xFFB8B8B8);
const qIBus_dark_gray = Color(0xFF929292);
const qIBus_icon_color = Color(0xFF747474);

const qIBus_color_facebook = Color(0xFF2F3181);
const qIBus_color_google = Color(0xFFF13B19);

const qIBus_purple = Color(0xFFB88DDD);
const qIBus_green = Color(0xFFC2DB77);
const qIBus_orange = Color(0xFFF5D270);
const qIBus_darkBlue = Color(0xFF5FACC9);
const qIBus_darkPurple = Color(0xFFB285CC);
const qIBus_darkGreen = Color(0xFFB6DD6E);
const qIBus_pink = Color(0xFFf5bebe);
const qIBus_ShadowColor = Color(0X95E9EBF0);


Map<int, Color> color = {
  50: Color.fromRGBO(136, 14, 79, .1),
  100: Color.fromRGBO(136, 14, 79, .2),
  200: Color.fromRGBO(136, 14, 79, .3),
  300: Color.fromRGBO(136, 14, 79, .4),
  400: Color.fromRGBO(136, 14, 79, .5),
  500: Color.fromRGBO(136, 14, 79, .6),
  600: Color.fromRGBO(136, 14, 79, .7),
  700: Color.fromRGBO(136, 14, 79, .8),
  800: Color.fromRGBO(136, 14, 79, .9),
  900: Color.fromRGBO(136, 14, 79, 1),
};

MaterialColor materialColor(colorHax) {
  return MaterialColor(colorHax, color);
}

MaterialColor colorCustom = MaterialColor(0XFF5959fc, color);
