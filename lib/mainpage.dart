
import 'dart:io' show Platform;
import 'dart:typed_data';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:mobintranet/bloc/main_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter/services.dart';
import 'dart:convert';
import 'package:mobintranet/api/manageapi.dart';
import 'package:mobintranet/model/user.dart';
import 'package:mobintranet/bloc/blocdata.dart';
import 'package:mobintranet/page/main.dart';
import 'package:mobintranet/page/home1.dart';
import 'package:mobintranet/page/pengumuman.dart';
import 'package:mobintranet/page/pesan.dart';
import 'package:mobintranet/page/profil.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:package_info/package_info.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:imei_plugin/imei_plugin.dart';

  const PLAY_STORE_URL ='https://play.google.com/store/apps/details?id=lan.go.id.mobintranet';
  const APP_STORE_URL= '';

class MainPage extends StatefulWidget {

  final String token;
  final String kode;

  const MainPage({Key key, this.token, this.kode}) : super(key: key);
 
  @override
  _MainPageState createState() => _MainPageState();
}

//reader jwt
Map<String, dynamic> parseJwt(String token) {
  final parts = token.split('.');
  if (parts.length != 3) {
    throw Exception('invalid token');
  }

  final payload = _decodeBase64(parts[1]);
  final payloadMap = json.decode(payload);
  if (payloadMap is! Map<String, dynamic>) {
    throw Exception('invalid payload');
  }

  return payloadMap;
}

String _decodeBase64(String str) {
  String output = str.replaceAll('-', '+').replaceAll('_', '/');

  switch (output.length % 4) {
    case 0:
      break;
    case 2:
      output += '==';
      break;
    case 3:
      output += '=';
      break;
    default:
      throw Exception('Illegal base64url string!"');
  }

  return utf8.decode(base64Url.decode(output));
}
// end reader

class _MainPageState extends State<MainPage>  {
  
  String nama;
  String nip;
  String unitkerja;
  String jk;
  String grading;
  String wilayah;
  String username;
  String eselon;
  User userr;
  String role;
  String unit;
  String kode1;
  int _selectedindex = 0;
  //imei
  String _platformImei = 'Unknown';
  String uniqueId = "Unknown";

  Widget getpage(int index, var token,var kode){
    if(index==0){
      return Main(token: token, kode: kode,);
    }
    if(index==1){
      return PageSurat(token: token, kode: kode,); 
    }
    if(index==2){
      return Pengumuman(token: token, kode: kode,);
    }
    if(index==3){
      return Profil(token: token, kode: kode,);
    }
  }

  void _onTabItem(int index){
    setState(() {
      _selectedindex=index;
    });
  }

  DateTime currentBackTime;

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  
// Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformImei;
    String idunique;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      platformImei =
          await ImeiPlugin.getImei(shouldShowRequestPermissionRationale: false);
      List<String> multiImei = await ImeiPlugin.getImeiMulti();
      print(multiImei);
      idunique = await ImeiPlugin.getId();
    } on PlatformException {
      platformImei = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      blocdata.setImei(platformImei);
      _platformImei = platformImei;
      uniqueId = idunique;
    });
  }

  initState() {

    initPlatformState();
      try {
        versionCheck(context);
      } catch (e) {
        print(e);
      }

    super.initState();
    //_ambilUser();
    blocdata = BlocData();

    print('token '+ widget.token);
    blocdata.settoken(widget.token);
    blocdata.seteselon(widget.kode);
    var jwt = parseJwt(widget.token);
    var rol= jwt['authorities'][jwt['authorities'].length-1];
    role=rol;
    blocdata.setrole(rol);
    print("${role}");

    

    //localnotif
    var android = new AndroidInitializationSettings('app_icon');
    var ios = new IOSInitializationSettings();
    var initializationSettings = new InitializationSettings(android: android,iOS: ios);
    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
    flutterLocalNotificationsPlugin.initialize(initializationSettings,onSelectNotification: onSelectNotification);
    //endlocalnotif
 
     _scheduleNotifPagi();
     _scheduleNotifSore();
  }

  Future<void> _cancelNotif(int id) async{
    await flutterLocalNotificationsPlugin.cancel(0);
  }

  Future<void> _cancelAllNotif() async{
    await flutterLocalNotificationsPlugin.cancelAll();
  }

  Future<void> onSelectNotification(String payload) async{
    print('cek payload : $payload');
    showDialog(context: context,
      builder: (_)=> new AlertDialog(
        title: const Text("#MasihDirumah"),
        content: new Text("Nanti Sore jangan Lupa Absen juga ya Bapak, Ibu.  Jangan Kemana mana dulu ya , Semangat Bekerja Semoga sehat selalu :)"),
      )
    );
  }

  Future<void> _scheduleNotifPagi() async {
    var time = Time(7, 00, 00);
    var androidPlatformChannelSpecifics =
        AndroidNotificationDetails('repeatDailyAtTime channel id',
            'repeatDailyAtTime channel name', 'repeatDailyAtTime description',
            importance: Importance.max,
            priority: Priority.high);
    var iOSPlatformChannelSpecifics =
      new IOSNotificationDetails(sound: "slow_spring_board.aiff");
    var platformChannelSpecifics = NotificationDetails(
        android: androidPlatformChannelSpecifics,iOS: iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.showDailyAtTime(
        1,
        'Sudah Jam 7 Pagi lho, Waktunya Absen Masuk Kawan :)',
        '#dirumahsaja',
        time,
        platformChannelSpecifics);
  }

  Future<void> _scheduleNotifSore() async {
    var time = Time(16, 0, 0);
    var androidPlatformChannelSpecifics =
        AndroidNotificationDetails('repeatDailyAtTime channel id',
            'repeatDailyAtTime channel name', 'repeatDailyAtTime description',
            importance: Importance.max,
            priority: Priority.high);
    var iOSPlatformChannelSpecifics =
      new IOSNotificationDetails(sound: "slow_spring_board.aiff");
    var platformChannelSpecifics = NotificationDetails(
        android: androidPlatformChannelSpecifics,iOS: iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.showDailyAtTime(
        2,
        'Sudah Jam 4 Sore, Jangan Lupa Absen Pulang Ya ;)',
        '#dirumahsaja',
        time,
        platformChannelSpecifics);
  }

  Future _showNotificationWithSound() async {
  var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
      'your channel id', 'your channel name', 'your channel description',
      // sound: "slow_spring_board",
      importance: Importance.max,
      priority: Priority.high);
  var iOSPlatformChannelSpecifics =
      new IOSNotificationDetails(sound: "slow_spring_board.aiff");
  var platformChannelSpecifics = new NotificationDetails(
      android:androidPlatformChannelSpecifics,iOS: iOSPlatformChannelSpecifics);
  await flutterLocalNotificationsPlugin.show(
    0,
    'Sudah Jam 7 Pagi lho, Waktunya Absen Masuk Kawan :)',
        '#dirumahsaja',
    platformChannelSpecifics,
    payload: 'Custom_Sound',
  );
}

  
  @override
  Widget build(BuildContext context) {
    Size media = MediaQuery.of(context).size;
    return new Scaffold(
    
      
      body: WillPopScope(
        child: getpage(_selectedindex, widget.token, widget.kode) ,
        onWillPop: onWillPop,
      ),
      // bottomNavigationBar: BottomNavigationBar(
      //   items: <BottomNavigationBarItem>[
      //     BottomNavigationBarItem(
      //       icon: Icon(Icons.home),
      //       title: Text("Beranda")
      //     ),
      //     BottomNavigationBarItem(
      //       icon: Icon(Icons.mail_outline),
      //       title: Text("Persuratan")
      //     ),
      //     BottomNavigationBarItem(
      //       icon: Icon(Icons.notifications),
      //       title: Text("Pengumuman")
      //     ),
      //     BottomNavigationBarItem(
      //       icon: Icon(Icons.account_circle),
      //       title: Text("Profil")
      //     )
      //   ],
      //   type: BottomNavigationBarType.fixed,
      //   currentIndex: _selectedindex,
      //   onTap: _onTabItem,
      // ),
  );
  }

versionCheck(context) async {
  //Get Current installed version of app
  final PackageInfo info = await PackageInfo.fromPlatform();
  double currentVersion = double.parse(info.version.trim().replaceAll(".", ""));
  //Get Latest version info from firebase config
  
  // final RemoteConfig remoteConfig = await RemoteConfig.instance;

  // try {
  //   print("sini try");
  //   // Using default duration to force fetching from remote server.  
  //   await remoteConfig.fetch(expiration: const Duration(seconds: 0));
  //   await remoteConfig.activateFetched();  
    
  //   print(currentVersion);
  //   print(remoteConfig.getString('force_update_current_version'));
    
  //   remoteConfig.getString('force_update_current_version');
  //   print(remoteConfig.getString('force_update_current_version'));
  //   double newVersion = double.parse(remoteConfig
  //       .getString('force_update_current_version')
  //       .trim()
  //       .replaceAll(".", ""));
  //   if (newVersion > currentVersion) {
  //     if(Platform.isAndroid){
  //       _showVersionDialog(context,PLAY_STORE_URL);
  //     }
  //     if(Platform.isIOS){
  //       _showVersionDialog(context,APP_STORE_URL);
  //     }
      
  //   }
  // } on FetchThrottledException catch (exception) {
    
  //   // Fetch throttled.
  //   print(exception);
  // } catch (exception) {
  //   print('Unable to fetch remote config. Cached or default values will be '
  //       'used');
  //   print(exception);
  // }
}

_showVersionDialog(context,url) async {
 
     
  await showDialog<String>(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      String title = "Update Terbaru Tersedia";
      String message =
          "Terdapat perubahan Terbaru Aplikasi Intranetlan. Lakukan update untuk mendapatkan fitur yang lebih lengkap.";
      String btnLabel = "Update Sekarang";
      String btnLabelCancel = "Nanti";
      return  new AlertDialog(
              title: Text(title),
              content: Text(message),
              actions: <Widget>[
                FlatButton(
                  child: Text(btnLabel),
                  onPressed: () => _launchURL(url),
                ),
                FlatButton(
                  child: Text(btnLabelCancel),
                  onPressed: () => Navigator.pop(context),
                ),
              ],
            );
    },
  );
}

_launchURL(String url) async {
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

Future<bool> onWillPop(){
    DateTime now = DateTime.now();
    if(currentBackTime == null|| now.difference(currentBackTime)> Duration(seconds: 2)){
      currentBackTime = now;
      Fluttertoast.showToast(msg: "Apakah anda yakin akan tutup aplikasi ?");
      return Future.value(false);
    }
    return Future.value(true);
  }
  
}
