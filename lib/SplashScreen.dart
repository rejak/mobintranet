import 'dart:async';
import 'dart:convert';

import 'package:mobintranet/Animation/fade_animation.dart';
import 'package:mobintranet/bloc/blocdata.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobintranet/login.dart';
import 'package:mobintranet/util/Db8Widget.dart';
import 'package:mobintranet/util/DbColors.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:mobintranet/mainpage.dart';
import 'package:mobintranet/api/manageapi.dart';
import 'package:mobintranet/util/colors.dart';
import 'package:mobintranet/util/constant.dart';
import 'package:mobintranet/util/extension.dart';
import 'package:mobintranet/util/images.dart';

class ShSplashScreen extends StatefulWidget {
  static String tag = '/ShophopSplash';
  final bool statusLink;

  const ShSplashScreen({this.statusLink});
  @override
  ShSplashScreenState createState() => ShSplashScreenState();
}

class ShSplashScreenState extends State<ShSplashScreen> {
  String token;
  String kode;
  bool status;
  @override
  void initState() {
    super.initState();
    startTime();
    cekstatuslogin().then((val) {
      print("siniiii");
      print(val);
      if (val) {
        API.cektoken(token).then((valu) {
          
          if (valu) {
            setState(() {
              status = val;
              print("status ok");
            });
          } else {
            setState(() {
              status = false;
              print("status expired");
            });
          }
        });
      } else {
        setState(() {
          status = false;
          print("status gagal");
        });
      }
    });
  }

  Future<bool> cekstatuslogin() async {
    final prefs = await SharedPreferences.getInstance();
    bool loginStatus = prefs.getBool('login') ?? false;
    print(prefs.getBool('login'));
    print("cek prefer");
    String tokens = prefs.getString('isitoken') ?? "tidak ada isii";
    String code = prefs.getString('user');
    print(tokens);
    if (loginStatus) {
      blocdata = BlocData();
      API.getUser(tokens).then((res) {
        var isi = jsonDecode(res.body);
        print(isi['nip']);
        API.getStswork(isi['nip']).then((res) {
          print(res.body);
          var i = jsonDecode(res.body);
          var data = i[0];
          print(data['STATUS_HADIR']);
          blocdata.setstatuswork(data['STATUS_HADIR']);
        });
      });

      API.getlaporan(tokens).then((res) {
        DateTime masuk;
        DateTime keluar;
        if (res.absenMasuk != null) {
          masuk = DateTime.parse(res.absenMasuk).toLocal();
          blocdata.setjammasuk("${masuk.hour}:${masuk.minute}:${masuk.second}");
        } else {
          blocdata.setjammasuk("-");
        }

        if (res.absenPulang != null) {
          keluar = DateTime.parse(res.absenPulang).toLocal();
          blocdata
              .setjamkeluar("${keluar.hour}:${keluar.minute}:${keluar.second}");
        } else {
          blocdata.setjamkeluar("-");
        }
        print("masukkk");
        print(blocdata.jammasuk);
      });
    }
    setState(() {
      token = tokens;
      kode = code;
    });
    print('login status: ${loginStatus}');
    return loginStatus;
  }

  // @override
  // void initState() {
  //   super.initState();
  //   startTime();
  // }

  startTime() async {
    var _duration = Duration(seconds: 3);
    return Timer(_duration, navigationPage);
  }

  void navigationPage() {
    Navigator.pop(context);
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => status != true
            ? Login()
            : MainPage(
                token: token,
                kode: kode,
              ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    changeStatusColor(Colors.black);
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Container(
        width: width + width * 0.4,
        child: Stack(
          children: <Widget>[
            Image.asset(splash_bg,
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                fit: BoxFit.cover),
            Positioned(
              top: -width * 0.2,
              left: -width * 0.2,
              child: Container(
                width: width * 0.65,
                height: width * 0.65,
                decoration: BoxDecoration(
                    shape: BoxShape.circle, color: fbiru1.withOpacity(0.7)),
              ),
            ),
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  FadeAnimation(
                    0.5,
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        text("Me..",
                            textColor: fbiru2,
                            fontSize: 25.0,
                            fontFamily: fontBold),
                      ],
                    ),
                  ),
                  FadeAnimation(
                    0.8,
                    Image.asset(
                      ic_app_icon,
                      width: 150,
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.bottomRight,
              child: Stack(
                children: <Widget>[
                  Positioned(
                    bottom: -width * 0.2,
                    right: -width * 0.2,
                    child: Container(
                      width: width * 0.65,
                      height: width * 0.65,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: fbiru1.withOpacity(0.7)),
                    ),
                  ),
                ],
              ),
            ),
            Align(
                alignment: Alignment.bottomRight,
                child: Image.asset(splash_img,
                    width: width * 0.5, height: width * 0.5))
          ],
        ),
      ),
    );
  }
}
