import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mobintranet/Animation/TransisionPage.dart';
import 'package:mobintranet/Animation/fade_animation.dart';
import 'package:mobintranet/bloc/bloc_login/bloc_login_bloc.dart';
import 'package:mobintranet/bloc/bloc_login/bloc_login_bloc.dart';
import 'package:mobintranet/util/DbColors.dart';
import 'package:mobintranet/util/DbExtension.dart';
import 'package:mobintranet/util/colors.dart';
import 'package:mobintranet/util/constant.dart';
import 'package:flutter/widgets.dart';
import 'package:mobintranet/util/ShString.dart';
import 'package:mobintranet/widget/WLogin.dart';
import 'package:mobintranet/api/requestLoginAPI.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:io';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:easy_localization/easy_localization.dart';
import 'Animation/buttonLoginAnimation.dart';
import 'package:flutter/animation.dart';
import 'package:flutter/scheduler.dart';

import 'model/loginModel.dart';

const URL = "intranet.lan.go.id/oauth";

class Login extends StatefulWidget {
  static String tag = '/Login';

  @override
  LoginState createState() => LoginState();
}

class LoginState extends State<Login> with TickerProviderStateMixin {
  final TextEditingController _usernameC = TextEditingController();
  final TextEditingController _passC = TextEditingController();
  String showPass = "Show Password";
  String _selamat = "";
  bool statuslogin = false;
  String token;
  bool _secure = true;
  final _loginBlocBloc = BlocLoginBloc();

  AnimationController animationControllerButton;
  AnimationController rippleController;
  AnimationController jamController;
  Animation<double> jamAnimation;
  Animation<double> rippleAnimation;

  @override
  void initState() {
    super.initState();
    animationControllerButton =
        AnimationController(duration: Duration(seconds: 3), vsync: this);
    rippleController =
        AnimationController(vsync: this, duration: Duration(seconds: 1));
    jamController =
        AnimationController(vsync: this, duration: Duration(seconds: 1));
    rippleAnimation =
        Tween<double>(begin: 250.0, end: 260.0).animate(rippleController)
          ..addStatusListener((status) {
            if (status == AnimationStatus.completed) {
              rippleController.reverse();
            } else if (status == AnimationStatus.dismissed) {
              rippleController.forward();
            }
          });
    jamAnimation = Tween<double>(begin: 0.0, end: 5.0).animate(jamController)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          jamController.reverse();
        } else if (status == AnimationStatus.dismissed) {
          jamController.forward();
        }
      });
    jamController.forward();
    rippleController.forward();
  }

  Future<Null> _playAnimation(BuildContext context, BlocLoginBloc bloc) async {
    await animationControllerButton.forward();
    print("cek login");
    return bloc.add(LoginEvent(
        con: context,
        username: _usernameC.text,
        password: _passC.text,
        status: statuslogin));
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    animationControllerButton.dispose();
  }

  void _toggle() {
    setState(() {
      if (_secure) {
        _secure = false;
        showPass = "Hide Password";
      } else {
        _secure = true;
        showPass = "Show Password";
      }
    });
  }

  cancelButton() {
    print("cancel button");
    setState(() {
      statuslogin = false;
      animationControllerButton.reset();
    });
  }

  var emailCont = TextEditingController();
  var passwordCont = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    changeStatusColor(fbiru2);
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        color: Colors.white,
        height: height,
        child: Stack(
          alignment: AlignmentDirectional.bottomCenter,
          children: <Widget>[
            Positioned(
              top: 0,
              child: Image(
                image: AssetImage("assets/login/login.png"),
                height: width * 0.6,
                fit: BoxFit.fill,
                width: MediaQuery.of(context).size.width,
              ),
            ),
            new Stack(
              children: [
                AnimatedBuilder(
                  animation: jamAnimation,
                  builder: (BuildContext context, Widget child) {
                    return Positioned(
                      right: -75,
                      top: -150,
                      width: (width * 0.5) + jamAnimation.value,
                      height: (height * 0.5) + jamAnimation.value,
                      child: Container(
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image:
                                    AssetImage("assets/login/bubble-1.png"))),
                      ),
                    );
                  },
                ),
                // AnimatedBuilder(
                //   animation: jamAnimation,
                //   builder: (BuildContext context, Widget child) {
                //     return Positioned(
                //       right: -70,
                //       top: -130,
                //       width: (width * 0.4) + jamAnimation.value,
                //       height: (height * 0.4) + jamAnimation.value,
                //       child: Container(
                //         decoration: BoxDecoration(
                //             image: DecorationImage(
                //                 image: AssetImage("assets/login/clock.png"))),
                //       ),
                //     );
                //   },
                // ),
                Positioned(
                  left: -65,
                  bottom: -170,
                  width: width * 0.8,
                  height: height * 0.5,
                  child: Container(
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage("assets/login/bubble-2.png"))),
                  ),
                ),
                Positioned(
                  left: 160,
                  bottom: -80,
                  width: width * 0.1,
                  height: height * 0.5,
                  child: FadeAnimation(
                    2,
                    Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage("assets/login/awan1.png"))),
                    ),
                  ),
                ),
                Positioned(
                  left: -65,
                  bottom: -180,
                  width: width * 0.9,
                  height: height * 0.5,
                  child: Container(
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage("assets/login/gedung1.png"))),
                  ),
                ),
                Positioned(
                  left: -45,
                  bottom: -70,
                  width: width * 0.2,
                  height: height * 0.5,
                  child: FadeAnimation(
                    1.5,
                    Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage("assets/login/matahari.png"))),
                    ),
                  ),
                ),
                Positioned(
                  left: 40,
                  bottom: -60,
                  width: width * 0.12,
                  height: height * 0.5,
                  child: FadeAnimation(
                    1.5,
                    Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage("assets/login/awan1.png"))),
                    ),
                  ),
                ),
                AnimatedBuilder(
                  animation: jamAnimation,
                  builder: (BuildContext context, Widget child) {
                    return Positioned(
                      right: -25,
                      top: 10+jamAnimation.value,
                      width: width * 0.9,
                      height: height * 0.5,
                      child: FadeAnimation(
                        2,
                        Container(
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image:
                                      AssetImage("assets/login/vektor2.png"))),
                        ),
                      ),
                    );
                  },
                ),
                FadeAnimation(
                  1,
                  Container(
                    margin: EdgeInsets.only(top: height * 0.13),
                    child: Column(
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Intranet LAN",
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: textSizeXLarge,
                                fontFamily: fontMedium,
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
            // Positioned(
            //   top: height - (width + width * 0.05),
            //   // child: CachedNetworkImage(
            //   //   imageUrl: ic_app_background,
            //   //   height: width + width * 0.05,
            //   //   width: width,
            //   //   fit: BoxFit.fill,
            //   // ),
            // ),
            SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.only(
                    bottom: 250.0, left: 24.0, right: 24.0, top: 24.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    // Image.asset(
                    //   ic_app_icon,
                    //   width: width * 0.22,
                    // ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        // text("Intranet",
                        //     textColor: sh_textColorPrimary,
                        //     fontSize: spacing_xlarge,
                        //     fontFamily: fontBold),
                      ],
                    ),
                    SizedBox(
                      height: width * 0.42,
                    ),
                    TextFormField(
                      keyboardType: TextInputType.emailAddress,
                      autofocus: false,
                      controller: _usernameC,
                      textCapitalization: TextCapitalization.words,
                      style: TextStyle(
                          color: sh_textColorPrimary,
                          fontFamily: fontRegular,
                          fontSize: textSizeMedium),
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: sh_editText_background,
                          focusColor: sh_editText_background_active,
                          hintText: "Username",
                          hintStyle: TextStyle(
                              color: sh_textColorSecondary,
                              fontFamily: fontRegular,
                              fontSize: textSizeMedium),
                          contentPadding:
                              EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(32.0),
                              borderSide:
                                  BorderSide(color: fbiru1, width: 0.5)),
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(32.0),
                              borderSide: BorderSide(
                                  color: Colors.transparent,
                                  style: BorderStyle.none,
                                  width: 0))),
                    ),
                    SizedBox(
                      height: spacing_standard_new,
                    ),
                    TextFormField(
                      controller: _passC,
                      obscureText: _secure,
                      keyboardType: TextInputType.text,
                      autofocus: false,
                      style: TextStyle(
                          color: sh_textColorPrimary,
                          fontFamily: fontRegular,
                          fontSize: textSizeMedium),
                      textCapitalization: TextCapitalization.words,
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: sh_editText_background,
                          focusColor: sh_editText_background_active,
                          hintStyle: TextStyle(
                              color: sh_textColorSecondary,
                              fontFamily: fontRegular,
                              fontSize: textSizeMedium),
                          hintText: "Password",
                          contentPadding:
                              EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(32.0),
                              borderSide:
                                  BorderSide(color: fbiru1, width: 0.5)),
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(32.0),
                              borderSide: BorderSide(
                                  color: Colors.transparent,
                                  style: BorderStyle.none,
                                  width: 0))),
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          FlatButton(
                            onPressed: _toggle,
                            child: new Row(
                              children: [
                                Text(
                                  showPass,
                                  style: TextStyle(color: Colors.grey),
                                ),
                                SizedBox(width: width * 0.02),
                                Icon(
                                  Icons.remove_red_eye,
                                  color: Colors.grey,
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: spacing_xlarge,
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  bottom: 200.0, left: 24.0, right: 24.0, top: 24.0),
              child: BlocListener<BlocLoginBloc, BlocLoginState>(
                cubit: _loginBlocBloc,
                listener: (context, state) {
                  if (state is LoginSukses) {
                    print("suksesss login");

                    print(statuslogin);
                    setpref(state.token, state.username);
                    print(state.jenisKelamin);
                    print(state.username);
                    // return PageTransit(jenisKelamin: state.jenisKelamin,username: state.username,);
                  }
                  if (state is LoginError) {
                    setState(() {
                      statuslogin = false;
                      animationControllerButton.reset();
                    });
                  }
                },
                child: Container(),
              ),
            ),
            (statuslogin == false)
                ? buttonLogin(context, _loginBlocBloc, width)
                : InkWell(
                    onTap: () {
                      print("cek animated");
                      setState(() {
                        statuslogin = false;
                        animationControllerButton.reset();
                      });
                    },
                    child: BlocBuilder<BlocLoginBloc, BlocLoginState>(
                      cubit: _loginBlocBloc,
                      builder: (context, state) {
                        // if (state is LoginError) {
                        //   print("cek erro build");
                        //   statuslogin = false;
                        //   animationControllerButton.reset();
                        //   return buttonLogin(context, _loginBlocBloc, width);
                        //   // return PageTransit(
                        //   //   buttonController: animationControllerButton.view,
                        //   // );
                        // }
                        if (state is LoginSukses) {
                          return PageTransit(
                            buttonController: animationControllerButton.view,
                            token: state.token,
                            kode: state.username,
                            jenisKelamin: state.jenisKelamin,username: state.myName
                          );
                        }
                        return AnimationButtonLogin(
                            buttonController: animationControllerButton.view,
                            clickCallback: cancelButton,
                            width: width);
                      },
                    )
                    // child: AnimationButtonLogin(
                    //     buttonController: animationControllerButton.view,
                    //     clickCallback: cancelButton, width: width),
                    )
          ],
        ),
      ),
    );
  }

  Widget buttonLogin(BuildContext context, BlocLoginBloc bloc, double width) {
    print(width);
    return AnimatedBuilder(
      animation: rippleAnimation,
      builder: (context, child) => Padding(
        padding: EdgeInsets.only(bottom: width / 2),
        child: Container(
          width: rippleAnimation.value,
          height: rippleAnimation.value / 5,
          child: MaterialButton(
            padding: EdgeInsets.all(spacing_standard),
            child: text(tr('login.button_login'),
                fontSize: textSizeNormal,
                fontFamily: fontMedium,
                textColor: sh_white),
            textColor: sh_white,
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(40.0)),
            color: fbiru2,
            onPressed: () {
              setState(() {
                statuslogin = true;
              });
              _playAnimation(context, bloc);
            },
          ),
          // child: Padding(
          //   padding: EdgeInsets.only(bottom: width / 2),
          //   child: Container(
          //     width: (width * 2) / 3,
          //     height: 50,
          //     // height: double.infinity,
          //     child: MaterialButton(
          //       padding: EdgeInsets.all(spacing_standard),
          //       child: text(tr('login.button_login'),
          //           fontSize: textSizeNormal,
          //           fontFamily: fontMedium,
          //           textColor: sh_white),
          //       textColor: sh_white,
          //       shape: RoundedRectangleBorder(
          //           borderRadius: new BorderRadius.circular(40.0)),
          //       color: fbiru1,
          //       onPressed: () {
          //         setState(() {
          //           statuslogin = true;
          //         });
          //         _playAnimation(context, bloc);
          //       },
          //     ),
          //   ),
          // )
        ),
      ),
    );
  }

  void setpref(tok, usrnm) async {
    print("cek token di setpref");
    print(tok);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print("isi tokennn: ${tok}");
    statuslogin = true;
    await prefs.setString('isitoken', tok.toString());
    print("sini woyyyyyyyyy");
    await prefs.setString('user', usrnm.toString());
    await prefs.setBool('login', statuslogin);
    print("cek memory");
    print(prefs.getBool('login'));
  }
}
