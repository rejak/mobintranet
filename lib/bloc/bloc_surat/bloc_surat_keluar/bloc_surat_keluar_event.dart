part of 'bloc_surat_keluar_bloc.dart';

@immutable
abstract class BlocSuratKeluarEvent {}

class PostSuratKeluarEvent extends BlocSuratKeluarEvent {
  File filePdf;
  final Surat dataPost;

  PostSuratKeluarEvent({@required this.dataPost,this.filePdf});
}

class GetSuratKeluarEvent extends BlocSuratKeluarEvent {

}

class PostReplyMailEvent extends BlocSuratKeluarEvent {
  File filepdf;
  final Surat dataPost;

  PostReplyMailEvent({this.filepdf,@required this.dataPost});
}

class PostDisposisiMail extends BlocSuratKeluarEvent{
  final Surat datapost;
  PostDisposisiMail({this.datapost});
}