import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:mobintranet/api/apiSurat.dart';
import 'package:mobintranet/api/manageapi.dart';
import 'package:mobintranet/model/Surat.dart';
import 'package:mobintranet/model/user.dart';
import 'package:mobintranet/page/surat/postDisposisi.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'bloc_surat_keluar_event.dart';
part 'bloc_surat_keluar_state.dart';


class BlocSuratKeluarBloc extends Bloc<BlocSuratKeluarEvent, BlocSuratKeluarState> {
  BlocSuratKeluarBloc() : super(BlocSuratKeluarInitial());

  @override
  Stream<BlocSuratKeluarState> mapEventToState(
    BlocSuratKeluarEvent event,
  ) async* {
    final prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('isitoken');
    //if post mail
    if(event is PostSuratKeluarEvent){
      print("masuk bloc awal");
      yield* _postSuratKeluar(event.filePdf,event.dataPost,token);
    }
    // if get mail outbox
    if(event is GetSuratKeluarEvent){
      print("masuk bloc awal");
      yield* _getSuratKeluar(token);
    }
    // if replymail
    if(event is PostReplyMailEvent){
      print("entry data");
      yield* _replyMail(token, event.dataPost, event.filepdf);
    }
    // if post disposisi mail
    if(event is PostDisposisiMail){
      yield* _disposisiMail(token, event.datapost);
    }
  }
}

Stream<BlocSuratKeluarState> _postSuratKeluar(filePdf,dataPost,tokens) async* {
  
  yield PostSuratWaiting();
  try {
    dynamic data = await ApiSurat.postSurat(filePdf, dataPost, tokens);
    var hasilFinal = Surat.fromJson(data);
    print(data);
    yield PostSuratSuccess(listSurat: [hasilFinal]);
  } catch (ex) {
    print("gagal");
    print(ex.toString()) ; 
    yield PostSuratFailed(errorMessage: ex.message.toString());
  }
}

Stream<BlocSuratKeluarState> _getSuratKeluar(tokens) async* {
  
  yield GetSuratWaiting();
  try {
    List<Surat> data = await API.getoutbox(tokens);
    yield GetSuratSuccess(listSurat: data);
  } catch (ex) {
    print("gagal");
    print(ex.toString()) ; 
    yield GetSuratFailed(errorMessage: ex.message.toString());
  }
}

Stream<BlocSuratKeluarState> _replyMail(token, data, file)async*{
  yield PostReplyMailWaiting();
  try {
    Surat isi = data;
    print(isi.catatan);
    print(file);
    bool result = await ApiSurat.replyMail(token, data, file);
    print(result);
    print("success reply mail");
    yield PostReplyMailSuccess();
  } catch (e) {
    yield PostReplyMailWaitingFailed(errorMessage: e.message.toString());
  }
}

Stream<BlocSuratKeluarState> _disposisiMail(token,Surat data)async*{
  yield PostDisposisiMailWaiting();
  try {
    print("masuk disposisi mail");
    print(data.catatanDisposisi);
    print(data.namaTujuan);
    print(data.sifatSurat);
    print(data.tanggalSurat);
    print(data.disposisi);
    bool isi =  await ApiSurat.postDisposisiMail(token, data);
    print(isi);
    print("cek bool");
    if(isi == true){
      yield PostDisposisiMailSuccess();
    } else {
      yield PostDisposisiMailFailed(errorMessage: "Mengirim Disposisi Gagal");
    }
  } catch (e) {
    yield PostDisposisiMailFailed(errorMessage: e.message.toString());
  }
}