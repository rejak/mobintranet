part of 'bloc_surat_keluar_bloc.dart';

@immutable
abstract class BlocSuratKeluarState {}

class BlocSuratKeluarInitial extends BlocSuratKeluarState {}

// post surat
class PostSuratSuccess extends BlocSuratKeluarState {
  List<Surat> listSurat;

  PostSuratSuccess({this.listSurat});
}

class PostSuratFailed extends BlocSuratKeluarState{
  final String errorMessage;
  PostSuratFailed({this.errorMessage});
}

class PostSuratWaiting extends BlocSuratKeluarState {
  
}

// get surat keluar
class GetSuratSuccess extends BlocSuratKeluarState {
  List<Surat> listSurat;

  GetSuratSuccess({this.listSurat});
}

class GetSuratFailed extends BlocSuratKeluarState{
  final String errorMessage;
  GetSuratFailed({this.errorMessage});
}

class GetSuratWaiting extends BlocSuratKeluarState {
  
}

// post reply mail
class PostReplyMailSuccess extends BlocSuratKeluarState{
  Surat valueReplyMail;
  PostReplyMailSuccess({this.valueReplyMail});
}
class PostReplyMailWaiting extends BlocSuratKeluarState{
  
}
class PostReplyMailWaitingFailed extends BlocSuratKeluarState{
  String errorMessage;
  PostReplyMailWaitingFailed({this.errorMessage});
}

// post disposisi mail
class PostDisposisiMailSuccess extends BlocSuratKeluarState{
  
}

class PostDisposisiMailWaiting extends BlocSuratKeluarState{

}

class PostDisposisiMailFailed extends BlocSuratKeluarState{
  final String errorMessage;
  PostDisposisiMailFailed({this.errorMessage});
}