part of 'bloc_surat_masuk_bloc.dart';

@immutable
abstract class BlocSuratMasukState {}

class BlocSuratMasukInitial extends BlocSuratMasukState {}

class GetSuratMasukSuccess extends BlocSuratMasukState{
 final List<Surat> listData;

 GetSuratMasukSuccess({@required this.listData});
}

class GetSuratMasukFailed extends BlocSuratMasukState{
  final String errorMessage;
  GetSuratMasukFailed({this.errorMessage});
  
}

class GetSuratMasukWaiting extends BlocSuratMasukState{

}