import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:mobintranet/api/manageapi.dart';
import 'package:mobintranet/model/Surat.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'bloc_surat_masuk_event.dart';
part 'bloc_surat_masuk_state.dart';

class BlocSuratMasukBloc extends Bloc<BlocSuratMasukEvent, BlocSuratMasukState> {
  BlocSuratMasukBloc() : super(BlocSuratMasukInitial());

  @override
  Stream<BlocSuratMasukState> mapEventToState(
    BlocSuratMasukEvent event,
  ) async* {
    final prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('isitoken');
    if(event is GetSuratMasukEvent){
      yield* _getSuratMasuk(token);
    }
  }
}

Stream<BlocSuratMasukState> _getSuratMasuk(token) async* {
  
  yield GetSuratMasukWaiting();
  try {
    List<Surat> data = await API.getinbox(token);
    yield GetSuratMasukSuccess(listData: data);
  } catch (ex) {
    yield GetSuratMasukFailed(errorMessage: ex.message.toString());
  }
}