part of 'bloc_detailsurat_bloc.dart';

@immutable
abstract class BlocDetailsuratEvent {}

class GetDetailSuratEvent extends BlocDetailsuratEvent {
  final int idSurat;
  GetDetailSuratEvent({@required this.idSurat});
}

class HistorySuratBloc extends BlocDetailsuratEvent{
  final int idSurat;
  // final int rcv;

  HistorySuratBloc({@required this.idSurat});
}