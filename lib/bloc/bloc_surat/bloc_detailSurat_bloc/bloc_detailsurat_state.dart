part of 'bloc_detailsurat_bloc.dart';

@immutable
abstract class BlocDetailsuratState {}

class BlocDetailsuratInitial extends BlocDetailsuratState {}

class GetDetailSuratSuccess extends BlocDetailsuratState{
 final Surat listData;

 GetDetailSuratSuccess({this.listData});
}

class GetDetailSuratFailed extends BlocDetailsuratState{
  final String errorMessage;
  GetDetailSuratFailed({this.errorMessage});
  
}

class GetDetailSuratWaiting extends BlocDetailsuratState{

}

class HistorySuccess extends BlocDetailsuratState{
 final List<Surat> dataHistory;
 

 HistorySuccess({this.dataHistory});
}

class HistoryFailed extends BlocDetailsuratState{
  final String errorMessage;

  HistoryFailed({this.errorMessage});
  
}

class HistoryWaiting extends BlocDetailsuratState{

  HistoryWaiting();
}