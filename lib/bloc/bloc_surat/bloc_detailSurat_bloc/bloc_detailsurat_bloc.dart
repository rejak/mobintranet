import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:mobintranet/api/apiSurat.dart';
import 'package:mobintranet/model/Surat.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'bloc_detailsurat_event.dart';
part 'bloc_detailsurat_state.dart';

class BlocDetailsuratBloc extends Bloc<BlocDetailsuratEvent, BlocDetailsuratState> {
  BlocDetailsuratBloc() : super(BlocDetailsuratInitial());

  @override
  Stream<BlocDetailsuratState> mapEventToState(
    BlocDetailsuratEvent event,
  ) async* {
    final prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('isitoken');
    if(event is GetDetailSuratEvent){
      yield* _getDetailSurat(event.idSurat, token);
    }
    if(event is HistorySuratBloc){
      yield* _getHistorySurat(token, event.idSurat);
    }
  }
}

Stream<BlocDetailsuratState> _getDetailSurat(idSurat,tokens) async* {
  
  yield GetDetailSuratWaiting();
  try {
    print("masuk bloc akhir");
    dynamic data = await ApiSurat.getDetailSurat(idSurat, tokens);
    var jsonData = Surat.fromJson(data); 
    yield GetDetailSuratSuccess(listData: jsonData);
  } catch (ex) {
    print("gagal");
    print(ex.toString()); 
    yield GetDetailSuratFailed(errorMessage: ex.message.toString());
  }
}

Stream<BlocDetailsuratState> _getHistorySurat(tokens, idSurat) async* {
  yield HistoryWaiting();
  try {
    print("masuk bloc akhir");
      final data = await ApiSurat.getHistorySurat(tokens, idSurat);
      print(data);
      final list = data.map<Surat>((json)=>Surat.fromJson(json)).toList();
      print("isi data");
    yield HistorySuccess(dataHistory: list);
  } catch (ex) {
    print("gagal");
    print(ex.toString()) ; 
    yield HistoryFailed(errorMessage: ex.message.toString());
  }
}