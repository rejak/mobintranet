part of 'blocabsen_bloc.dart';

@immutable
abstract class BlocabsenState {}

class BlocabsenInitial extends BlocabsenState {}

class PostAbsenSukses extends BlocabsenState{
  AbsenMwfh data;
  PostAbsenSukses({this.data});
}

class PostAbsenWaiting extends BlocabsenState{}

class PostAbsenError extends BlocabsenState{
  String errorMessage;

  PostAbsenError({this.errorMessage});
}

class GetLocationSukses extends BlocabsenState{
  String hasil;
  GetLocationSukses({this.hasil});
}

class GetLocationWaiting extends BlocabsenState{}

class GetLocationError extends BlocabsenState{
  String errorMessage;
  GetLocationError({this.errorMessage});
}

class GetInLocationSukses extends BlocabsenState{
  String hasil;
  GetInLocationSukses({this.hasil});
}

class GetInLocationWaiting extends BlocabsenState{}

class GetInLocationError extends BlocabsenState{
  String errorMessage;
  GetInLocationError({this.errorMessage});
}

class GetSiangLocationSukses extends BlocabsenState{
  String hasil;
  GetSiangLocationSukses({this.hasil});
}

class GetSiangLocationWaiting extends BlocabsenState{}

class GetSiangLocationError extends BlocabsenState{
  String errorMessage;
  GetSiangLocationError({this.errorMessage});
}

class SaveLaporanSukses extends BlocabsenState{
  String hasil;
  SaveLaporanSukses({this.hasil});
}

class SaveLaporanError extends BlocabsenState{
  String errorMessage;
  SaveLaporanError({this.errorMessage});
}

class SaveLaporanWaiting extends BlocabsenState{}