part of 'blocabsen_bloc.dart';

@immutable
abstract class BlocabsenEvent {}

class PostAbsenEvent extends BlocabsenEvent{}

class GetLocationEvent extends BlocabsenEvent{
  double longitude;
  double latitude;
  String tempat;
  GetLocationEvent({@required this.longitude, @required this.latitude, @required this.tempat});
}

class GetInLocationEvent extends BlocabsenEvent{
  double longitude;
  double latitude;
  String tempat;
  GetInLocationEvent({@required this.longitude, @required this.latitude, @required this.tempat});
}

class GetInLocationSiangEvent extends BlocabsenEvent{
  double longitude;
  double latitude;
  String tempat;
  GetInLocationSiangEvent({@required this.longitude, @required this.latitude, @required this.tempat});
}

class SaveLaporanEvent extends BlocabsenEvent{
  String kesehatan;
  String kegiatan;
  String output;
  String progress;

  SaveLaporanEvent({@required this.kegiatan, @required this.kesehatan, @required this.output, @required this.progress});
}