import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:mobintranet/api/manageapi.dart';
import 'package:mobintranet/model/absenwfh.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'blocabsen_event.dart';
part 'blocabsen_state.dart';

class BlocabsenBloc extends Bloc<BlocabsenEvent, BlocabsenState> {
  BlocabsenBloc() : super(BlocabsenInitial());

  @override
  Stream<BlocabsenState> mapEventToState(
    BlocabsenEvent event,
  ) async* {
    final prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('isitoken');
    if(event is GetLocationEvent){
      yield* _outLocation(token, event.longitude, event.latitude, event.tempat);
    }
    if(event is GetInLocationEvent){
      yield* _inLocation(token, event.longitude, event.latitude, event.tempat);
    }
    if(event is PostAbsenEvent){
      yield* _getAbsen(token);
    }
    if(event is SaveLaporanEvent){
      yield* _saveLaporan(token, event.kegiatan, event.kesehatan, event.output, event.progress);
    }
    if(event is GetInLocationSiangEvent){
      yield* _siangLocation(token,event.longitude, event.latitude, event.tempat);
    }
  }
}

Stream<BlocabsenState> _saveLaporan(token, kegiatan, kesehatan, output, progress)async* {
  yield SaveLaporanWaiting();
  try {
    print("bloc lapporan");
    String data = await API.simpanlaporan(token, kesehatan, kegiatan, output, progress);
    yield SaveLaporanSukses(hasil: data);
  } catch (e) {
    yield SaveLaporanError(errorMessage: e.message.toString());
  }
}

Stream<BlocabsenState> _getAbsen(token)async*{
  yield PostAbsenWaiting();
  try {
    print("blocabsen");
    AbsenMwfh data = await API.getlaporan(token);
    yield PostAbsenSukses(data: data);
  } catch (e) {
    yield PostAbsenError(errorMessage: e.message.toString());
  }
}

Stream<BlocabsenState> _outLocation(token, long, lat, tempat) async*{
  yield GetLocationWaiting();
  try {
    print("bloclokasi");
    String data = await API.cekoutgps(token, long, lat, tempat);
    yield GetLocationSukses(hasil:data);
  } catch (e) {
    yield GetLocationError(errorMessage: e.message.toString());
  }
}

Stream<BlocabsenState> _inLocation(token, long, lat, tempat) async*{
  yield GetInLocationWaiting();
  try {
    print("blocinlokasi");
    String data = await API.cekingps(token, long, lat, tempat);
    yield GetInLocationSukses(hasil:data);
  } catch (e) {
    yield GetInLocationError(errorMessage: e.message.toString());
  }
}

Stream<BlocabsenState> _siangLocation(token, long, lat, tempat) async*{
  yield GetInLocationWaiting();
  try {
    print("blocinlokasi");
    String data = await API.ceksiang(token, long, lat, tempat);
    yield GetInLocationSukses(hasil:data);
  } catch (e) {
    yield GetInLocationError(errorMessage: e.message.toString());
  }
}