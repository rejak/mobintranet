import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'bloc_coba_event.dart';
part 'bloc_coba_state.dart';

class BlocCobaBloc extends Bloc<BlocCobaEvent, BlocCobaState> {
  BlocCobaBloc() : super(BlocCobaInitial());

  @override
  Stream<BlocCobaState> mapEventToState(
    BlocCobaEvent event,
  ) async* {
    if(event is BlocCobaEvent){
      yield* _cekCoba();
    }
  }
}

Stream<BlocCobaState> _cekCoba()async*{
  yield StateWaiitng();
  try {
    print("sinimasuk");
    yield StateSuccess();
  } catch (e) {
    yield StateFailed();
  }
}