part of 'bloc_daftar_pegawai_bloc.dart';

@immutable
class BlocDaftarPegawaiState {}

class BlocDaftarPegawaiInitial extends BlocDaftarPegawaiState {}

class GetDaftarPegawaiSukses extends BlocDaftarPegawaiState{
  List<Pegawai> listData;
  List<Pegawai> listDataCari;
  List<dynamic> listDataNonModel;
  bool statusCari;
  GetDaftarPegawaiSukses({@required this.listData, this.statusCari, this.listDataCari, this.listDataNonModel});
}

class GetDaftarPegawaiGagal extends BlocDaftarPegawaiState{
  final String errorMessage;
  GetDaftarPegawaiGagal({
    this.errorMessage,
  });
}

class GetDaftarPegawaiWaiting extends BlocDaftarPegawaiState{}