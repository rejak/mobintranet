part of 'bloc_daftar_pegawai_bloc.dart';

@immutable
abstract class BlocDaftarPegawaiEvent {}

class GetDataPegawaiEvent extends BlocDaftarPegawaiEvent{
  
}

class SearchPegawaiEvent extends BlocDaftarPegawaiEvent{
  String text;
  List<Pegawai> dataList;
  bool cari;
  SearchPegawaiEvent({this.text, this.dataList, this.cari});
}