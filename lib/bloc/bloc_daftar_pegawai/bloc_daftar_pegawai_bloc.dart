import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:mobintranet/api/manageapi.dart';
import 'package:mobintranet/model/loginModel.dart';
import 'package:mobintranet/model/pegawai.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'bloc_daftar_pegawai_event.dart';
part 'bloc_daftar_pegawai_state.dart';

class BlocDaftarPegawaiBloc extends Bloc<BlocDaftarPegawaiEvent, BlocDaftarPegawaiState> {
  BlocDaftarPegawaiBloc() : super(BlocDaftarPegawaiInitial());

  @override
  Stream<BlocDaftarPegawaiState> mapEventToState(
    BlocDaftarPegawaiEvent event,
  ) async* {
    final prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('isitoken');
    String textDefault = "";
    print("dataaaaaaaa");
    print(state);
    if(event is GetDataPegawaiEvent){
      yield* _getDataPegawai(token, textDefault);
    }
    if(event is SearchPegawaiEvent){
      yield* _searchDataPegawai(event.text, event.dataList, event.cari, token);
    }
  }
}
Stream<BlocDaftarPegawaiState> _searchDataPegawai(textSearch, data, cari, token) async* {
  
  yield GetDaftarPegawaiWaiting();
  try {
    print("bloc cari");
    print(cari);
    List<Pegawai> daftarcari=new List();
    if(cari!=null){
      if(cari == true){
        print("ini true");
        for(int i =0; i< data.length;i++){
        Pegawai datas = data[i];
        if(datas.nama.toLowerCase().contains(textSearch.toLowerCase())){
          daftarcari.add(datas);
        }
      }
      }
    }
    yield GetDaftarPegawaiSukses(listData: data, listDataCari: daftarcari,statusCari: true);
  } catch (ex) {
    print("gagal");
    print(ex.toString()) ; 
    yield GetDaftarPegawaiGagal(errorMessage: ex.message.toString());
  }
}

Stream<BlocDaftarPegawaiState> _getDataPegawai(token, textSearch) async* {
  
  yield GetDaftarPegawaiWaiting();
  try {
    List<Pegawai> list=[];
    List<dynamic> listD=[];
    List<dynamic> data = await API.getallpegawai(token);
    print(data);
    list= data.map<Pegawai>((json) => Pegawai.fromJson(json)).toList();
    // list= list.map<dynamic>((json) => Pegawai.fromJson(json)).toList();
    // var isi = data.map<Pegawai>((e) => Pegawai(
    //   nama: e["iid"],
    // ));
    // print(isi);
    yield GetDaftarPegawaiSukses(listDataNonModel:data,listData: list, listDataCari: [],statusCari: false);
  } catch (ex) {
    print("gagal");
    print(ex.toString()) ; 
    yield GetDaftarPegawaiGagal(errorMessage: ex.message.toString());
  }
}
