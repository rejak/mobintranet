import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';
import 'package:mobintranet/api/requestLoginAPI.dart';
import 'package:mobintranet/model/loginModel.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'bloc_login_event.dart';
part 'bloc_login_state.dart';

class BlocLoginBloc extends Bloc<BlocLoginEvent, BlocLoginState> {
  BlocLoginBloc() : super(BlocLoginInitial());

  @override
  Stream<BlocLoginState> mapEventToState(
    BlocLoginEvent event,
  ) async* {
    if(event is LoginEvent){
      yield* _loginAction(event.con,event.username,event.password,event.status);
    }
  }
}

Stream<BlocLoginState> _loginAction(context, username,password, status) async* {
  
  yield LoginWaiting();
  try {
    LoginModel data = await requestLoginAPI(context, username, password, status);
    final prefs = await SharedPreferences.getInstance();
    var jk = prefs.getString('jenisKelamin');
    var myName = prefs.getString('myName');
    print("cek bloc");
    yield LoginSukses(token: data.token.toString(), username: data.userName.toString(), jenisKelamin: jk.toString(), myName: myName);
  } catch (ex) {
    print("gagal2");
    print(ex.toString()) ; 
    yield LoginError(errorMessage: ex.message.toString());
  }
}