part of 'bloc_login_bloc.dart';

@immutable
abstract class BlocLoginEvent {}

class LoginEvent extends BlocLoginEvent {
  String username;
  String password;
  bool status;
  BuildContext con;
  LoginEvent({@required this.username, @required this.password, @required this.status, this.con});
}
