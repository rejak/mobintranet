part of 'bloc_login_bloc.dart';

@immutable
abstract class BlocLoginState {}

class BlocLoginInitial extends BlocLoginState {}

class LoginSukses extends BlocLoginState {
  final String token;
  final String username;
  final String jenisKelamin;
  final String myName;
  LoginSukses({@required this.token, @required this.username, @required this.jenisKelamin, @required this.myName});
}
class LoginError extends BlocLoginState{
  final String errorMessage;
  LoginError({
    this.errorMessage,
  });
}
class LoginWaiting extends BlocLoginState {}