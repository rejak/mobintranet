import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:mobintranet/api/manageapi.dart';
import 'package:mobintranet/model/produk_hukum.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'bloc_produk_event.dart';
part 'bloc_produk_state.dart';

class BlocProdukBloc extends Bloc<BlocProdukEvent, BlocProdukState> {
  BlocProdukBloc() : super(BlocProdukInitial());

  @override
  Stream<BlocProdukState> mapEventToState(
    BlocProdukEvent event,
  ) async* {
    final prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('isitoken');
    if(event is GetProdukEvent){
      yield* _getProduk(token);
    }
  }
}

Stream<BlocProdukState> _getProduk(token) async* {
  yield GetProdukWaiting();
  try {
    List<ProdukHukum> data = await API.getProdukHukum(token);
    yield GetProdukSukses(listData: data);
  } catch (e) {
    yield GetProdukGagal(errorMessage: e.message.toString());
  }
}