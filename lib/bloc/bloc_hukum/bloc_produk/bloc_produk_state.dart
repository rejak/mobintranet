part of 'bloc_produk_bloc.dart';

@immutable
abstract class BlocProdukState {}

class BlocProdukInitial extends BlocProdukState {}

class GetProdukSukses extends BlocProdukState{
  List<ProdukHukum> listData;

  GetProdukSukses({@required this.listData});
}

class GetProdukGagal extends BlocProdukState{
  String errorMessage;
  GetProdukGagal({this.errorMessage});
}

class GetProdukWaiting extends BlocProdukState{}