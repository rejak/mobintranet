part of 'bloc_pengajuan_bloc.dart';

@immutable
abstract class BlocPengajuanState {}

class BlocPengajuanInitial extends BlocPengajuanState {}

class GetPengajuanSukses extends BlocPengajuanState{
  List<Pengajuan_hukum> listData;

  GetPengajuanSukses({@required this.listData});
}

class GetPengajuanWaiting extends BlocPengajuanState{}

class GetPengajuanGagal extends BlocPengajuanState{
  String errorMessage;
  GetPengajuanGagal({this.errorMessage});
}