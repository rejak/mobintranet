import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:mobintranet/api/manageapi.dart';
import 'package:mobintranet/model/pengajuan_hukum.dart';
import 'package:mobintranet/page/hukum/pengajuan.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'bloc_pengajuan_event.dart';
part 'bloc_pengajuan_state.dart';

class BlocPengajuanBloc extends Bloc<BlocPengajuanEvent, BlocPengajuanState> {
  BlocPengajuanBloc() : super(BlocPengajuanInitial());

  @override
  Stream<BlocPengajuanState> mapEventToState(
    BlocPengajuanEvent event,
  ) async* {
    final prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('isitoken');
    if(event is GetPengajuanEvent){
      yield* _getPengajuan(token);
    }
  }
}

Stream<BlocPengajuanState> _getPengajuan(token) async* {
  yield GetPengajuanWaiting();
  try {
    List<Pengajuan_hukum> data = await API.getpengajuanHukum(token);
    yield GetPengajuanSukses(listData: data);
  } catch (e) {
    yield GetPengajuanGagal(errorMessage: e.message.toString());
  }
}
