import 'package:flutter/gestures.dart';
import 'package:mobintranet/bloc/blocdata.dart';
import 'package:mobintranet/bloc/bloc_setting.dart';
import 'package:mobintranet/bloc/main_bloc.dart';
import 'package:mobintranet/model/user.dart';
import 'package:mobintranet/util/DbImages.dart';

class BlocData extends BloCSetting{
  var Datastate;
  String judul= "main bloc ok";
  String tokenapi="";
  String eselon="";
  String eselon1="";
  String kode="";
  String username="";
  String role="";
  String statuswork="";
  String jammasuk="";
  String jamkeluar="";
  String imei="";
  List<User> user=[];

  void settoken(String token){
    rebuildWidgets(
      setStates: (){
        tokenapi=token;
      },
      states: [Datastate],
    );
    mainBloc?.tokenapi=token;
  }

  void setusername(String usrnm){
    rebuildWidgets(
      setStates: (){
        username=usrnm;
      },
      states: [Datastate],
    );
    mainBloc?.username=usrnm;
  }

  void seteselon(String eselon){
    rebuildWidgets(
      setStates: (){
        eselon=eselon;
      },
      states: [Datastate],
    );
    mainBloc?.eselon=eselon;
    print(eselon);
    print(mainBloc?.eselon);
  }

  void setuser(List usr){
    rebuildWidgets(
      setStates: (){
        user=usr;
      },
      states: [Datastate],
    );
    mainBloc?.user=usr;
  }

  void setrole(String role){
    rebuildWidgets(
      setStates: (){
        role=role;
      },
      states: [Datastate],
    );
    mainBloc?.role=role;
  }

  void setkode(String kode){
    rebuildWidgets(
      setStates: (){
        kode=kode;
      },
      states: [Datastate],
    );
    mainBloc?.kode=kode;
  }

  void seteselon1(String eselon){
    rebuildWidgets(
      setStates: (){
        eselon1=eselon;
      },
      states: [Datastate],
    );
    mainBloc?.eselon1=eselon;
  }

  void setstatuswork(String work){
    rebuildWidgets(
      setStates: (){
        statuswork=work;
      },
      states: [Datastate],
    );
    mainBloc?.statuswork=work;
  }

  void setjammasuk(String jam){
    rebuildWidgets(
      setStates: (){
        jammasuk=jam;
      },
      states: [Datastate],
    );
    mainBloc?.jammasuk=jam;
  }

  void setjamkeluar(String jam){
    rebuildWidgets(
      setStates: (){
        jamkeluar=jam;
      },
      states: [Datastate],
    );
    mainBloc?.jamkeluar=jam;
  }

  void setImei(String imei){
    rebuildWidgets(
      setStates: (){
        imei=imei;
      },
      states: [Datastate],
    );
    mainBloc?.imei=imei;
  }


}

BlocData blocdata;