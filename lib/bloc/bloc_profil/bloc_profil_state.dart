part of 'bloc_profil_bloc.dart';

@immutable
class BlocProfilState {}

class BlocProfilInitial extends BlocProfilState {}

class GetProfilSukses extends BlocProfilState{
  final User dataList;

  GetProfilSukses({this.dataList});
}

class GetProfilFailedState extends BlocProfilState{
final String errorMessage;
GetProfilFailedState({this.errorMessage});
}

class GetProfilWaiting extends BlocProfilState {
  
}