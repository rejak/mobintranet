import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:mobintranet/api/manageapi.dart';
import 'package:mobintranet/model/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'bloc_profil_event.dart';
part 'bloc_profil_state.dart';

class BlocProfilBloc extends Bloc<BlocProfilEvent, BlocProfilState> {
  BlocProfilBloc() : super(BlocProfilInitial());

  @override
  Stream<BlocProfilState> mapEventToState(
    BlocProfilEvent event,
  ) async* {
    final prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('isitoken');
    if(event is GetProfilEvent){
      yield* _getDataProfil(token);
    }
  }
}

Stream<BlocProfilState> _getDataProfil(token) async* {
  
  yield GetProfilWaiting();
  try {
    User data = await API.getUser2Coba(token);
    yield GetProfilSukses(dataList: data);
  } catch (ex) {
    yield GetProfilFailedState(errorMessage: ex.message.toString());
  }
}