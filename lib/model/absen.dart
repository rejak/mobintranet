
import 'package:mobintranet/page/absen/absen.dart';

class Absen{
  String nip;
  String tglmasuk;
  String jammasuk;
  String jamkeluar;

  Absen({
    this.nip,
    this.jamkeluar,
    this.jammasuk,
    this.tglmasuk
  });

  factory Absen.fromjson(Map<String,dynamic> json){
   return Absen(
    nip : json['nip'],
    tglmasuk:json['tgl_masuk'],
    jammasuk:json['jam_masuk'],
    jamkeluar:json['jam_keluar']
   );
  }
    
}