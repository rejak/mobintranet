class DB8Scene {
  var name;
  var img;
  var color;
  var url;

  DB8Scene(this.img, this.name,this.color,this.url);
}

class DB8Rooms {
  var name;
  var img;
  var device;

  DB8Rooms(this.name, this.img, this.device);
}

class MenuSurat{
  var icon;
  var title;

  MenuSurat(this.icon, this.title);
}