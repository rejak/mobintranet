class UserModel {
  int id;
  String nama;
  String nip;
  String pendidikan;
  String golonganRuang;
  String tmtGolongan;
  String jabatan;
  String tmtJabatan;
  String unitKerja;
  String tmtUnit;
  String perEselonDua;
  String jenisKelamin;
  String grading;
  String eselon;
  String wilayah;
  String kode;
  String unit;
  String namaUnit;
  String eselonSatu;
  String eselonDua;
  String username;
  String photo;

  UserModel({this.eselon,
    this.eselonDua,
    this.eselonSatu,
    this.golonganRuang,
    this.grading,
    this.id,
    this.jabatan,
    this.jenisKelamin,
    this.kode,
    this.nama,
    this.namaUnit,
    this.nip,
    this.pendidikan,
    this.perEselonDua,
    this.photo,
    this.tmtGolongan,
    this.tmtJabatan,
    this.tmtUnit,
    this.unit,
    this.unitKerja,
    this.username,
    this.wilayah});

  factory UserModel.fromJson(Map<String, dynamic> json) {
    if (json == null) return null;
    return UserModel(
      eselon:json['eselon'],
      eselonDua:json['eselonDua'],
      eselonSatu:json['eselonSatu'],
      golonganRuang:json['golonganRuang'],
      grading:json['grading'],
      id:json['id'],
      jabatan:json['jabatan'],
      jenisKelamin:json['jenisKelamin'],
      kode:json['kode'],
      nama:json['nama'],
      namaUnit:json['namaUnit'],
      nip:json['nip'],
      pendidikan:json['pendidikan'],
      perEselonDua:json['perEselonDua'],
      photo:json['photo'],
      tmtGolongan:json['tmtGolongan'],
      tmtJabatan:json['tmtJabatan'],
      tmtUnit:json['tmtUnit'],
      unit:json['unit'],
      unitKerja:json['unitKerja'],
      username:json['username'],
      wilayah:json['wilayah']
    );
  }

  static List<UserModel> fromJsonList(List list) {
    if (list == null) return null;
    return list.map<UserModel>((item) => UserModel.fromJson(item)).toList();
  }

  ///this method will prevent the override of toString
  String userAsString() {
    return '#${this.id} ${this.nama}';
  }

  ///this method will prevent the override of toString
  // bool userFilterByCreationDate(String filter) {
  //   return this?.createdAt?.toString()?.contains(filter);
  // }

  ///custom comparing function to check if two users are equal
  bool isEqual(UserModel model) {
    return this?.id == model?.id;
  }

  @override
  String toString() => nama;
}