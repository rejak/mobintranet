
import 'dart:ffi';

class AbsenMwfh{
  String statusAbsen;
  String nip;
  String nama;
  String eselonDua;
  String tanggal;
  String absenMasuk;
  String absenPulang;
  String kondisi;
  String kegiatan;
  String output;
  int progres;
  String unit;
  String lokasi;
  double long;
  double lat;
  int status;
  String keterangan;
  double longitudePulang;
  double latitudePulang;
  String lokasiPulang;

  AbsenMwfh({
    this.statusAbsen,
    this.nip,
    this.nama,
  this.eselonDua,
  this.tanggal,
  this.absenMasuk,
  this.absenPulang,
  this.kondisi,
  this.kegiatan,
  this.output,
  this.progres,
  this.unit,
  this.lokasi,
  this.long,
  this.lat,
  this.status,
  this.keterangan,
  this.longitudePulang,
  this.latitudePulang,
  this.lokasiPulang

  });

   Map<String, dynamic> toJson() =>
    {
      'statusAbsen':statusAbsen,
      'nip':nip,
      'nama':nama,
      'eselonDua':eselonDua,
      'tanggal':tanggal,
      'absenMasuk':absenMasuk,
      'absenPulang':absenPulang,
      'kondisi':kondisi,
      'kegiatan':kegiatan,
      'output':output,
      'progres':progres,
      'unit':unit,
      'lokasi':lokasi,
      'long':long,
      'lat':lat,
      'status':status,
      'keterangan':keterangan,
      'longitudePulang':longitudePulang,
      'latitudePulang':latitudePulang,
      'lokasiPulang':lokasiPulang
    };

  factory AbsenMwfh.fromjson(Map<String,dynamic> json){
   return AbsenMwfh(
  statusAbsen: json['statusAbsen'],
  nama:json['nama'],
  eselonDua:json['eselonDua'],
  tanggal:json['tanggal'],
  absenMasuk:json['absenMasuk'],
  absenPulang:json['absenPulang'],
  kondisi:json['kondisi'],
  kegiatan:json['kegiatan'],
  output:json['output'],
  progres:json['progres'],
  unit:json['unit'],
  lokasi:json['lokasi'],
  long:json['long'],
  lat:json['lat'],
  status:json['status'],
  keterangan:json['keterangan'],
  longitudePulang:json['longitudePulang'],
  latitudePulang: json['latitudePulang'],
  lokasiPulang: json['lokasiPulang']
   );
  }
}