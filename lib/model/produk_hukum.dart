class ProdukHukum {
  int id;
  String nomor;
  String tanggalPenetapan;
  String namaPeraturan;
  String mencabut;
  String dicabut;
  String diubah;
  String keberlakuan;
  String deskripsiSingkat;
  String permasalahan;
  String keterangan;

 ProdukHukum({
   this.deskripsiSingkat,
   this.dicabut,
   this.diubah,
   this.id,
   this.keberlakuan,
   this.keterangan,
   this.mencabut,
   this.namaPeraturan,
   this.nomor,
   this.permasalahan,
   this.tanggalPenetapan
 });

 factory ProdukHukum.fromjson(Map<String,dynamic> json){
   return ProdukHukum(
   deskripsiSingkat:json['deskripsiSingkat'],
   dicabut:json['dicabut'],
   diubah:json['diubah'],
   id:json['id'],
   keberlakuan:json['keberlakuan'],
   keterangan:json['keterangan'],
   mencabut:json['mencabut'],
   namaPeraturan:json['namaPeraturan'],
   nomor:json['nomor'],
   permasalahan:json['permasalahan'],
   tanggalPenetapan:json['tanggalPenetapan']
   );
 }
}