class Pengajuan_hukum{

  int id;
  String judulPeraturan;
  String jenis;
  String unitTerkait;
  String targetSelesai;
  String jadwalPembahasan;
  String deskripsi;
  String path;
  String path2;
  String path3;
  String idPengusul;
  String namaPengusul;
  String tanggalPengajuan;
  String tanggalPersetujuan;
  String tanggalJawaban;
  String tanggalKirim;
  int aktif;
  String status;
  String idPengajuan;
  String komentar;

  Pengajuan_hukum({
    this.aktif,
    this.deskripsi,
    this.tanggalJawaban,
    this.id,
    this.idPengajuan,
    this.idPengusul,
    this.jadwalPembahasan,
    this.jenis,
    this.judulPeraturan,
    this.komentar,
    this.namaPengusul,
    this.path,
    this.path2,
    this.path3,
    this.status,
    this.tanggalKirim,
    this.tanggalPengajuan,
    this.tanggalPersetujuan,
    this.targetSelesai,
    this.unitTerkait,
  });

  factory Pengajuan_hukum.fromjson(Map<String,dynamic> json){
    return Pengajuan_hukum(
    aktif:json['aktif'],
    deskripsi:json['deskripsi'],
    tanggalJawaban:json['tanggalJawaban'],
    id:json['id'],
    idPengajuan:json['idPengajuan'],
    idPengusul:json['idPengusul'],
    jadwalPembahasan:json['jadwalPembahasan'],
    jenis:json['jenis'],
    judulPeraturan:json['judulPeraturan'],
    komentar:json['komentar'],
    namaPengusul:json['namaPengusul'],
    path:json['path'],
    path2:json['path2'],
    path3:json['path3'],
    status:json['status'],
    tanggalKirim:json['tanggalKirim'],
    tanggalPengajuan:json['tanggalPengajuan'],
    tanggalPersetujuan:json['tanggalPersetujuan'],
    targetSelesai:json['targetSelesai'],
    unitTerkait:json['unitTerkait'],
    );
  }
}