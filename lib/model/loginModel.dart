class LoginModel{
  final String userName;
  final String token;
  final String email;
  final String pk;

  LoginModel(this.userName, this.token, this.email, this.pk);

  LoginModel.fromJson(Map<String,dynamic> json):
  userName=json['userName'],
  token=json['access_token'],
  email=json['email'],
  pk=json['pk'];

  Map<String,dynamic> toJson()=>{
    'username':userName,
    'token':token,
    'email':email,
    'pk':pk
  };
}