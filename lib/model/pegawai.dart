class Pegawai {
  int id;
  String nama;
  String nip;
  String pendidikan;
  String golonganRuang;
  String tmtGolongan;
  String jabatan;
  String tmtJabatan;
  String unitKerja;
  String tmtUnit;
  String perEselonDua;
  String jenisKelamin;
  String grading;
  String eselon;
  String wilayah;
  String kode;
  String unit;
  String namaUnit;
  String eselonSatu;
  String eselonDua;
  String username;
  String photo;

  Pegawai({
    this.eselon,
    this.eselonDua,
    this.eselonSatu,
    this.golonganRuang,
    this.grading,
    this.id,
    this.jabatan,
    this.jenisKelamin,
    this.kode,
    this.nama,
    this.namaUnit,
    this.nip,
    this.pendidikan,
    this.perEselonDua,
    this.photo,
    this.tmtGolongan,
    this.tmtJabatan,
    this.tmtUnit,
    this.unit,
    this.unitKerja,
    this.username,
    this.wilayah
  });

  factory Pegawai.fromJson(Map<String,dynamic> json){
    return Pegawai(
      eselon:json['eselon'],
      eselonDua:json['eselonDua'],
      eselonSatu:json['eselonSatu'],
      golonganRuang:json['golonganRuang'],
      grading:json['grading'],
      id:json['id'],
      jabatan:json['jabatan'],
      jenisKelamin:json['jenisKelamin'],
      kode:json['kode'],
      nama:json['nama'],
      namaUnit:json['namaUnit'],
      nip:json['nip'],
      pendidikan:json['pendidikan'],
      perEselonDua:json['perEselonDua'],
      photo:json['photo'],
      tmtGolongan:json['tmtGolongan'],
      tmtJabatan:json['tmtJabatan'],
      tmtUnit:json['tmtUnit'],
      unit:json['unit'],
      unitKerja:json['unitKerja'],
      username:json['username'],
      wilayah:json['wilayah']
    );
  }

  Map<String,dynamic> toJson() =>
  {
    "eselon":eselon,
    "eselonDua":eselonDua,
    "eselonSatu":eselonSatu,
    "golonganRuang":golonganRuang,
    "grading":grading,
    "id":id,
    "jabatan":jabatan,
    "jenisKelamin":jenisKelamin,
    "kode":kode,
    "nama":nama,
    "namaUnit":namaUnit,
    "nip":nip,
    "pendidikan":pendidikan,
    "perEselonDua":perEselonDua,
    "photo":photo,
    "tmtGolongan":tmtGolongan,
    "tmtJabatan":tmtJabatan,
    "tmtUnit":tmtUnit,
    "unit":unit,
    "unitKerja":unitKerja,
    "username":username,
    "wilayah":wilayah
  };

}