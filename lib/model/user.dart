class User{
  String penerima;
  String nama;
  String nip;
  String unitKerja;
  String jk;
  String grading;
  String wilayah;
  String username;
  String kode;
  String eselon;
  String unit;
  String jabatan;
  String golonganRuang;
  String tmtGolongan;
  String tmtJabatan;
  String tmtUnit;
  String perEselonDua;
  


  User(this.penerima,this.perEselonDua,this.golonganRuang,this.tmtGolongan,this.tmtJabatan,this.jabatan,this.tmtUnit,this.unit,this.eselon,this.nama, this.nip, this.unitKerja, this.jk, this.grading, this.wilayah, this.username,this.kode);

  void setnama(String nama){
    this.nama=nama;
  }
  String getnama(){
    return nama;
  }

  User.fromJson(Map<String,dynamic> json) :
  penerima=json['penerima'],
  nama=json['nama'],
  nip=json['nip'],
  unitKerja=json['unitKerja'],
  jk=json['jenisKelamin'],
  grading=json['grading'],
  wilayah=json['wilayah'],
  kode=json['kode'],
  username=json['username'],
  eselon=json['eselon'],
  unit=json['unit'],
  jabatan=json['jabatan'],
  perEselonDua=json['perEselonDua'],
  golonganRuang = json['golonganRuang'],
  tmtGolongan = json['tmtGolongan'],
  tmtJabatan = json['tmtJabatan'],
  tmtUnit = json['tmtUnit'];

  Map<String,dynamic> toJson()=>{
    'penerima':penerima,
    'nama':nama,
    'nip':nip,
    'unitKerja':unitKerja,
    'jk':jk,
    'grading':grading,
    'wilayah':wilayah,
    'username':username,
    'kode':kode,
    'eselon':eselon,
    'unit':unit,
    'jabatan':jabatan,
    'perEselonDua':perEselonDua,
    'golonganRuang':golonganRuang,
    'tmtGolongan':tmtGolongan,
    'tmtJabatan':tmtJabatan,
    'tmtUnit':tmtUnit,
  };
}