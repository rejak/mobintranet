import 'package:mobintranet/model/contohModel.dart';

class Surat{
  int idSurat;
  String jenisSurat;
  String klasifikasiSurat;
  String asalSurat;
  String derajatSurat;
  String rakSurat;
  String tanggal;
  String tangalAkhirDisposisi;
  String nomorAgenda;
  String nomorSurat;
  String tanggalSurat;
  String pengirim;
  String namaPengirim;
  String instansiAsal;
  String tujuan;
  String namaTujuan;
  String lampiran;
  String perihal;
  String catatan;
  String ringkasanSurat;
  String path;
  String status;
  String catatanDisposisi;
  String disposisi;
  String dibuatOleh;
  String statusApp;
  String jawabanDisposisi;
  String pathJawaban;
  String disposisiTanggal;
  String pathDisplay;
  String untuk;
  String sekretarisSatu;
  String sekretarisDua;
  String email;
  int aktif;
  int tahun;
  String lokasi;
  String tanggalMulai;
  String tanggalSelesai;
  String kodeEselonSatu;
  String kodeEselonDua;
  String sifatSurat;
  List<dynamic> penerimaRequest;

  Surat({
    this.penerimaRequest,
    this.sifatSurat,
    this.aktif,
    this.asalSurat,
    this.catatan,
    this.catatanDisposisi,
    this.derajatSurat,
    this.dibuatOleh,
    this.disposisi,
    this.disposisiTanggal,
    this.email,
    this.idSurat,
    this.instansiAsal,
    this.jawabanDisposisi,
    this.jenisSurat,
    this.klasifikasiSurat,
    this.kodeEselonDua,
    this.kodeEselonSatu,
    this.lampiran,
    this.lokasi,
    this.namaPengirim,
    this.namaTujuan,
    this.nomorAgenda,
    this.nomorSurat,
    this.path,
    this.pathDisplay,
    this.pathJawaban,
    this.pengirim,
    this.perihal,
    this.rakSurat,
    this.ringkasanSurat,
    this.sekretarisDua,
    this.sekretarisSatu,
    this.status,
    this.statusApp,
    this.tahun,
    this.tangalAkhirDisposisi,
    this.tanggal,
    this.tanggalMulai,
    this.tanggalSelesai,
    this.tanggalSurat,
    this.tujuan,
    this.untuk,

  });

  factory Surat.fromJson(Map<String,dynamic> json){
    return Surat(
      penerimaRequest: json['penerimaRequest'],
      aktif:json['aktif'],
      asalSurat:json['asalSurat'],
      catatan:json['catatan'],
      catatanDisposisi:json['catatanDisposisi'],
      derajatSurat:json['derajatSurat'],
      dibuatOleh:json['dibuatOleh'],
      disposisi:json['disposisi'],
      disposisiTanggal:json['disposisiTanggal'],
      email:json['email'],
      idSurat:json['idSurat'],
      instansiAsal:json['instansiAsal'],
      jawabanDisposisi:json['jawabanDisposisi'],
      jenisSurat:json['jenisSurat'],
      klasifikasiSurat:json['klasifikasiSurat'],
      kodeEselonDua:json['kodeEselonDua'],
      kodeEselonSatu:json['kodeEselonSatu'],
      lampiran:json['lampiran'],
      lokasi:json['lokasi'],
      namaPengirim:json['namaPengirim'],
      namaTujuan:json['namaTujuan'],
      nomorAgenda:json['nomorAgenda'],
      nomorSurat:json['nomorSurat'],
      path:json['path'],
      pathDisplay:json['pathDisplay'],
      pathJawaban:json['pathJawaban'],
      pengirim:json['pengirim'],
      perihal:json['perihal'],
      rakSurat:json['rakSurat'],
      ringkasanSurat:json['ringkasanSurat'],
      sekretarisDua:json['sekretarisDua'],
      sekretarisSatu:json['sekretarisSatu'],
      status:json['status'],
      statusApp:json['statusApp'],
      tahun:json['tahun'],
      tangalAkhirDisposisi:json['tangalAkhirDisposisi'],
      tanggal:json['tanggal'],
      tanggalMulai:json['tanggalMulai'],
      tanggalSelesai:json['tanggalSelesai'],
      tanggalSurat:json['tanggalSurat'],
      tujuan:json['tujuan'],
      untuk:json['untuk'],
      sifatSurat:json['sifatSurat']
    );
  }

  
}

       