import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mobintranet/bloc/bloc_profil/bloc_profil_bloc.dart';
import 'package:mobintranet/model/user.dart';
import 'package:mobintranet/page/surat/ResponDisposisi.dart';
import 'package:mobintranet/page/surat/disposisi.dart';
import 'package:mobintranet/page/surat/kotakkeluar.dart';
import 'package:mobintranet/page/surat/kotakmasuk.dart';
import 'package:mobintranet/page/surat/tambahsurat.dart';
import 'package:mobintranet/util/DbColors.dart';
import 'package:mobintranet/util/DbImages.dart';
import 'package:mobintranet/util/Style.dart';
import 'package:mobintranet/widget/pesan/configuration.dart';
import 'package:mobintranet/widget/pesan/drawer.dart';

import '../Loader.dart';
import 'errorPage/error_page.dart';

class PageSurat extends StatefulWidget {
  final String token;
  final String kode;
  final int indexPage;

  const PageSurat({Key key, this.token, this.kode, this.indexPage}) : super(key: key);
  @override
  _PageSuratState createState() => _PageSuratState();
}

class _PageSuratState extends State<PageSurat> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final bloc = BlocProfilBloc();
  double xOffset = 0;
  double yOffset = 0;
  double scaleFactor = 1;

  bool isDrawerOpen = false;
  int indexlayout = 0;
  String judul = "Kotak Masuk";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    bloc..add(GetProfilEvent());
  }

  @override
  Widget build(BuildContext context) {
    changeStatusColor(Colors.white);
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      floatingActionButton: FloatingActionButton(
              backgroundColor: Colors.transparent,
              onPressed: () {
                // Navigator.of(context).pop(true);
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => TambahSurat()));
              },
              child: Image.asset(social_fab_msg)
              ),
          body: Stack(
            children: [
              // Drawer2(),
              BlocBuilder<BlocProfilBloc, BlocProfilState>(
                cubit: bloc,
                builder: (context, state) {
                  print(state);
                  if(state is GetProfilSukses){
                    return Drawer2(currentPage: widget.indexPage,);
                  } if(state is GetProfilWaiting){
                    return Container(child: Loader());
                  } if(state is GetProfilFailedState){
                    return ErrorPage();
                  }
                  return Container(child: Loader());
                },
              ),
              (widget.indexPage == 0) ?KotakMasuk():(widget.indexPage == 1)?KotakKeluar():(widget.indexPage == 2)?ResponDisposisi():(widget.indexPage == 3)?Disposisi():null
              // PageAwal()
            ],
          ),
    );
  }

  Widget PageAwal(){
    return Scaffold(
            // key: _scaffoldKey,
            floatingActionButton: FloatingActionButton(
              backgroundColor: fbiru1,
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => TambahSurat()));
              },
              child: Icon(
                Icons.add,
              ),
            ),
            
            body:KotakMasuk()
    );
            
  }

  // Widget drawer() {
  //   return Drawer(
  //     child: ListView(
  //       padding: EdgeInsets.zero,
  //       children: <Widget>[
  //         Container(
  //           height: 120,
  //           child: DrawerHeader(
  //             child: Text(
  //               "Persuratan",
  //               style: TextStyle(fontSize: 22, color: Colors.white),
  //             ),
  //             decoration: BoxDecoration(
  //               color: fbiru1,
  //             ),
  //           ),
  //         ),
  //         ListTile(
  //           leading: Icon(Icons.inbox),
  //           title: Text("Kotak Masuk"),
  //           onTap: () {
  //             setState(() {
  //               judul = "Kotak Masuk";
  //               indexlayout = 0;
  //             });
  //             Navigator.pop(context);
  //           },
  //         ),
  //         ListTile(
  //           leading: Icon(Icons.send),
  //           title: Text("Kotak Keluar"),
  //           onTap: () {
  //             setState(() {
  //               judul = "Kotak Keluar";
  //               indexlayout = 1;
  //             });
  //             Navigator.pop(context);
  //           },
  //         ),
  //         ListTile(
  //           leading: Icon(Icons.move_to_inbox),
  //           title: Text("Disposisi Masuk"),
  //           onTap: () {
  //             setState(() {
  //               judul = "Disposisi Masuk";
  //               indexlayout = 2;
  //             });
  //             Navigator.pop(context);
  //           },
  //         ),
  //         ListTile(
  //           leading: Icon(Icons.add_circle_outline),
  //           title: Text("Respon Disposisi"),
  //           onTap: () {
  //             setState(() {
  //               judul = "Respon Disposisi";
  //               indexlayout = 3;
  //             });
  //             Navigator.pop(context);
  //           },
  //         ),
  //       ],
  //     ),
  //   );
  // }

  // Widget drawer2() {
  //   return Container(
  //     color: Colors.greenAccent[400],
  //     padding: EdgeInsets.only(top: 50, bottom: 70, left: 10),
  //     child: Column(
  //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //       children: [
  //         Row(
  //           children: [
  //             CircleAvatar(),
  //             SizedBox(
  //               width: 10,
  //             ),
  //             Column(
  //               crossAxisAlignment: CrossAxisAlignment.start,
  //               children: [
  //                 Text(
  //                   'Miroslava Savitskaya',
  //                   style: TextStyle(
  //                       color: Colors.white, fontWeight: FontWeight.bold),
  //                 ),
  //                 Text('Active Status',
  //                     style: TextStyle(
  //                         color: Colors.white, fontWeight: FontWeight.bold))
  //               ],
  //             )
  //           ],
  //         ),
  //         Column(
  //           children: drawerItems
  //               .map((element) => Padding(
  //                     padding: const EdgeInsets.all(8.0),
  //                     child: Row(
  //                       children: [
  //                         Icon(
  //                           element['icon'],
  //                           color: Colors.white,
  //                           size: 30,
  //                         ),
  //                         SizedBox(
  //                           width: 10,
  //                         ),
  //                         Text(element['title'],
  //                             style: TextStyle(
  //                                 color: Colors.white,
  //                                 fontWeight: FontWeight.bold,
  //                                 fontSize: 20))
  //                       ],
  //                     ),
  //                   ))
  //               .toList(),
  //         ),
  //         Row(
  //           children: [
  //             Icon(
  //               Icons.settings,
  //               color: Colors.white,
  //             ),
  //             SizedBox(
  //               width: 10,
  //             ),
  //             Text(
  //               'Settings',
  //               style:
  //                   TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
  //             ),
  //             SizedBox(
  //               width: 10,
  //             ),
  //             Container(
  //               width: 2,
  //               height: 20,
  //               color: Colors.white,
  //             ),
  //             SizedBox(
  //               width: 10,
  //             ),
  //             Text(
  //               'Log out',
  //               style:
  //                   TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
  //             )
  //           ],
  //         )
  //       ],
  //     ),
  //   );
  // }
}
