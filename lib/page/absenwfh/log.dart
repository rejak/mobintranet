
import 'package:flutter/material.dart';
import 'package:mobintranet/Loader.dart';
import 'package:mobintranet/api/manageapi.dart';
import 'package:mobintranet/bloc/main_bloc.dart';
import 'package:mobintranet/model/absen.dart';
import 'package:mobintranet/model/absenwfh.dart';
import 'package:mobintranet/util/DbColors.dart';
import 'package:mobintranet/widget/datetime.dart';

class LogAbsenwfh extends StatefulWidget {
  @override
  _LogAbsenwfhState createState() => _LogAbsenwfhState();
}

DateTime date = DateTime.now();


class _LogAbsenwfhState extends State<LogAbsenwfh> {
  
  List<AbsenMwfh> list;

  bool ada =true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    API.getalllaporan(mainBloc.tokenapi).then((res){
       list=res;
       print('${list[0]}');
    }).catchError((err)=>print(err));
  }

   

  @override
  Widget build(BuildContext context) {
    Size media = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: fbiru1,
        title: Text("Riwayat absen WFH"),
      ),
      body: FutureBuilder(
      future: API.getalllaporan(mainBloc.tokenapi),
      builder: (context,res){
        return list !=null ?
        listlog(list.reversed.toList(),media)
        : Loader();
      }
    )
    );
  }
}

Widget listlog (List<AbsenMwfh> absen,Size media){

  return Container(
      child: Column(
        children: <Widget>[
          Expanded(
            child: ListView.builder(
              itemCount: absen.length,
              itemBuilder: (context,index){
                var jammasuk=DateTime.parse('0000-00-00 00:00:00+00:00');
                var jampulang=DateTime.parse('0000-00-00 00:00:00+00:00');
                var tanggal=DateTime.parse('0000-00-00 00:00:00+00:00');
                if(absen[index].absenMasuk!=null){jammasuk = DateTime.parse(absen[index].absenMasuk).toLocal();}
                if(absen[index].absenPulang!=null){jampulang = DateTime.parse(absen[index].absenPulang).toLocal();}
                if(absen[index].tanggal!=null){tanggal = DateTime.parse(absen[index].tanggal).toLocal();}

                String bulan;
                if(tanggal.month==1){bulan="Januari";}
                if(tanggal.month==2){bulan="Febuari";}
                if(tanggal.month==3){bulan="Maret";}
                if(tanggal.month==4){bulan="April";}
                if(tanggal.month==5){bulan="Mei";}
                if(tanggal.month==6){bulan="Juni";}
                if(tanggal.month==7){bulan="Juli";}
                if(tanggal.month==8){bulan="Agustus";}
                if(tanggal.month==9){bulan="September";}
                if(tanggal.month==10){bulan="Oktober";}
                if(tanggal.month==11){bulan="November";}
                if(tanggal.month==12){bulan="Desember";}

                return Container(
                  padding: EdgeInsets.symmetric(vertical: 1.0,horizontal: 1.0),
                  // height: 120,
                  width: double.maxFinite,
                  child: GestureDetector(
                      onTap: (){
                        },
                      child: Card(
                      elevation: 0,
                      child: Container(
                        padding: EdgeInsets.symmetric(vertical: 13,horizontal: 1),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text("${tanggal.day} ${bulan} ${tanggal.year}",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),),
                                  Row(
                                    children: <Widget>[
                                      Text("Masuk : ${jammasuk.hour}:${jammasuk.minute}:${jammasuk.second}  ",style: TextStyle(fontSize: 15),),
                                      Text("| Pulang : ${jampulang.hour}:${jampulang.minute}:${jampulang.second} ",style: TextStyle(fontSize: 15),),
                                    ],
                                  ),
                                  //Text("Lokasi Masuk: ( ${absen[index].lokasi} )",style: TextStyle(fontSize: 15),),
                                  //Text("Lokasi Pulang: ( ${absen[index].lokasiPulang} )",style: TextStyle(fontSize: 15)),  
                                  Text("Kondisi : ${absen[index].kondisi}",style: TextStyle(fontSize: 15))
                                ],
                              ), 
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
}