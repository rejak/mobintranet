import 'package:camera/camera.dart';
import 'package:flutter/material.dart';

class Kamera extends StatefulWidget {
  final CameraDescription camera;
  Kamera({@required this.camera});
  
  @override
  _KameraState createState() => _KameraState();
}

class _KameraState extends State<Kamera> {
  CameraController controller;
  List cameras;
  int selectedCameraIdx;
  String imagePath;

  @override
  void initState() {
    super.initState();
    // 1
    availableCameras().then((availableCameras) {
      
      cameras = availableCameras;
      if (cameras.length > 0) {
        setState(() {
          // 2
          selectedCameraIdx = 0;
        });

        _initCameraController(cameras[selectedCameraIdx]).then((void v) {});
      }else{
        print("No camera available");
      }
    }).catchError((err) {
      // 3
      print('Error: $err.code\nError Message: $err.message');
    });
  }

  Future _initCameraController(CameraDescription cameraDescription) async {
  if (controller != null) {
    await controller.dispose();
  }

  // 3
  controller = CameraController(cameraDescription, ResolutionPreset.high);

  // If the controller is updated then update the UI.
  // 4
  controller.addListener(() {
    // 5
    if (mounted) {
      setState(() {});
    }

    if (controller.value.hasError) {
      print('Camera error ${controller.value.errorDescription}');
    }
  });

  // 6
  try {
    await controller.initialize();
  } on CameraException catch (e) {
    // _showCameraException(e);
  }

  if (mounted) {
    setState(() {});
  }
}

Widget _cameraPreviewWidget() {
  if (controller == null || !controller.value.isInitialized) {
    return const Text(
      'Loading',
      style: TextStyle(
        color: Colors.white,
        fontSize: 20.0,
        fontWeight: FontWeight.w900,
      ),
    );
  }

  return AspectRatio(
      aspectRatio: controller.value.aspectRatio,
      child: CameraPreview(controller),
    );
}
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("camera"),
      ),
      body: Container(
        child: Column(
          children: [
            Expanded(
              flex: 7,
              child: Container(
                width: MediaQuery.of(context).size.width,
                child: _cameraPreviewWidget(),
              )
            ),
            Expanded(
              flex: 1,
              child: Row(
                children: [
                  Expanded(
                  flex: 2,
                  child: Center(
                   child: InkWell(
                     onTap: () => setState(() => {}),
                     child: Container(
                       padding: const EdgeInsets.all(10.0),
                       decoration: BoxDecoration(
                         shape: BoxShape.circle,
                         color: Colors.grey,
                       ),
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.center,
                         children: [
                           Icon(
                            Icons.crop_rotate,
                            color: Colors.white,
                            size: 36.0,
                          ),
                         ],
                       ),
                     ),
                   ), 
                  )
                ),
                Expanded(
                  flex: 2,
                  child: Center(
                   child: InkWell(
                     onTap: () => setState(() => {}),
                     child: Container(
                       padding: const EdgeInsets.all(10.0),
                       decoration: BoxDecoration(
                         shape: BoxShape.circle,
                         color: Colors.grey,
                       ),
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.center,
                         children: [
                           Icon(
                            Icons.camera,
                            color: Colors.white,
                            size: 36.0,
                          ),
                         ],
                       ),
                     ),
                   ), 
                  )
                ),
                Expanded(
                  flex: 2,
                  child: Container()
                ),
                ],
              )),
          ],
        ),
      ),
    );
  }
}