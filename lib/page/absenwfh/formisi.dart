import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:mobintranet/api/manageapi.dart';
import 'package:mobintranet/bloc/bloc_absen/blocabsen_bloc.dart';
import 'package:mobintranet/bloc/blocdata.dart';
import 'package:mobintranet/bloc/main_bloc.dart';
import 'package:mobintranet/mainpage.dart';
import 'package:mobintranet/page/absenwfh/absenwfh.dart';
import 'package:mobintranet/page/home1.dart';
import 'package:mobintranet/widget/datetime.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:trust_location/trust_location.dart';

class Formisi extends StatefulWidget {
  @override
  _FormisiState createState() => _FormisiState();
}

Position pos ;


class _FormisiState extends State<Formisi>  {
  
final bloc = BlocabsenBloc();
String tempat;
String namahari;
List<Placemark> loc;
String kesehatan="Sehat";
TextEditingController kondisi = TextEditingController();
TextEditingController kegiatan = TextEditingController();
TextEditingController progres = TextEditingController();
TextEditingController output = TextEditingController();
bool _loading = true;


Future<Position> posisi()  {
  log("here");
    return  Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high); 
  }
  //end permission
Future<List<Placemark>> lokasi(double long,double lat){
  return Geolocator().placemarkFromCoordinates(lat, long);
}

_onBasicAlertPressed(context) {
     var alertStyle = AlertStyle(
      animationType: AnimationType.fromTop,
      isCloseButton: false,
      isOverlayTapDismiss: false,
      descStyle: TextStyle(fontWeight: FontWeight.bold),
      animationDuration: Duration(milliseconds: 400),
      alertBorder: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(0.0),
        side: BorderSide(
          color: Colors.grey,
        ),
      ),
      titleStyle: TextStyle(
        color: Colors.green,
      ),
      constraints: BoxConstraints.expand(width: 300)
    );

    // Alert dialog using custom alert style
    Alert(
      context: context,
      style: alertStyle,
      type: AlertType.info,
      title: "Berhasil Absen Pulang",
      desc: "Jangan kemana mana, selalu #dirumahsaja yaa",
      
    ).show();
  }
  
  @override
  void initState() {
    setState(() {
      namahari=Tanggal().hariini();
    });
    posisi().then((res){
      setState(() {
        pos=res;
      });
      lokasi(res.longitude,res.latitude).then((res){
        setState(() {
          loc=res;
          loc.forEach((f)=>print(f.country));
          tempat=loc.toString();
          tempat=loc[0].name+','+loc[0].subLocality+','+loc[0].locality+','+loc[0].administrativeArea;
          _loading= false;
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Fom Absensi "),
      ),
      body: SingleChildScrollView(
        // child: _loading? Column(
        //   mainAxisAlignment: MainAxisAlignment.center,
        //   crossAxisAlignment: CrossAxisAlignment.center,
        //   children: <Widget>[
        //     Center(child: CircularProgressIndicator())
        //   ],
        // )
      child:Card(
          child:Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
            children:<Widget>[
              Text("Kondisi Kesehatan"),
              new DropdownButton<String>(
                value: kesehatan,
                items: <String>['Sehat','Kurang Sehat','Sakit'].map((String value){
                  return new DropdownMenuItem<String>(
                    value: value,
                    child: new Text(value),);
                }).toList(), 
                onChanged: (String newvalue){
                  setState(() {
                    kesehatan = newvalue;
                  });
                }),
              // TextFormField(
              //   controller: kondisi,
              //   decoration: InputDecoration(
              //     hintText: 'Kondisi Kesehatan',
              //     labelText: 'Kondisi Kesehatan'
              //   ),
              // ),
              TextFormField(
                controller: kegiatan,
                decoration: InputDecoration(
                  hintText: 'Kegiatan',
                  labelText: 'Kegiatan'
                ),
              ),
              TextFormField(
                controller: progres,
                decoration: InputDecoration(
                  hintText: 'Diisi angka 1-100',
                  labelText: 'Progres Kegiatan (%)'
                ),
              ),
              TextFormField(
                controller: output,
                decoration: InputDecoration(
                  hintText: 'Output Kegiatan',
                  labelText: 'Output Kegiatan'
                ),
              ),
              SizedBox(height:10),
              // Text("Lokasi Anda :",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold)),
              // Text(" $tempat",style: TextStyle(fontSize: 16)),
              SizedBox(height:10),
              BlocListener<BlocabsenBloc, BlocabsenState>(
                cubit: bloc,
                listener: (context, state) {
                  if(state is PostAbsenSukses){
                    print("absen");
                    TrustLocation.stop();
                          DateTime keluar;
                          if(state.data.absenPulang!=null){
                            keluar=DateTime.parse(state.data.absenPulang).toLocal();
                          blocdata.setjamkeluar("${keluar.hour}:${keluar.minute}:${keluar.second}"); }
                          else{
                            blocdata.setjamkeluar("-");
                          }
                          _onBasicAlertPressed(context);
                      Timer(const Duration(milliseconds: 3000), () {
                        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (contex)=>MainPage(token: mainBloc.tokenapi)), (Route<dynamic> route) => false);
                      });
                  }
                },
                child: BlocBuilder<BlocabsenBloc, BlocabsenState>(
                cubit: bloc,
                builder: (context, state) {
                  if(state is SaveLaporanSukses){
                    print("save");
                    bloc..add(GetLocationEvent(longitude: pos.longitude, latitude: pos.latitude, tempat: tempat));
                  }
                  if(state is GetLocationSukses){
                    print("lokasi");
                    bloc..add(PostAbsenEvent());
                  }
                  return Container();
                },
              ),
              ),
              FlatButton(
                color: Colors.green,
                textColor: Colors.white,
                disabledColor: Colors.grey,
                disabledTextColor: Colors.black,
                padding: EdgeInsets.all(8.0),
                splashColor: Colors.blueAccent,
                onPressed: () {
                  bloc..add(SaveLaporanEvent(kesehatan: kesehatan,kegiatan: kegiatan.text,output: output.text,progress: progres.text));
                  print(kondisi.text);
                  // API.simpanlaporan(mainBloc.tokenapi, kesehatan, kegiatan.text, output.text, progres.text).then((res){
                    
                  //   API.cekoutgps(mainBloc.tokenapi, pos.longitude, pos.latitude, tempat).then((res){
                  //     API.getlaporan(mainBloc.tokenapi).then((res){

                  //         TrustLocation.stop();
                  //         DateTime keluar;
                  //         if(res.absenPulang!=null){
                  //           keluar=DateTime.parse(res.absenPulang).toLocal();
                  //         blocdata.setjamkeluar("${keluar.hour}:${keluar.minute}:${keluar.second}"); }
                  //         else{
                  //           blocdata.setjamkeluar("-");
                  //         }
                  //     });
                  //     _onBasicAlertPressed(context);
                  //     Timer(const Duration(milliseconds: 3000), () {
                  //       Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (contex)=>MainPage(token: mainBloc.tokenapi)), (Route<dynamic> route) => false);
                  //     });
                  //   }).catchError((err){print(err);});
                      
                  // }).catchError((err){print(err);});
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.timer),
                    Text(
                      "Kirim Absen",
                      style: TextStyle(fontSize: 20.0),
                    ),
                  ],
                ), 
              ),
            ]
        ),
          ), 
        )
      ),
      
    );
  }
}