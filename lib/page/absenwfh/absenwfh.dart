import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:mobintranet/Loader.dart';
import 'package:mobintranet/api/manageapi.dart';
import 'package:mobintranet/bloc/main_bloc.dart';
import 'package:mobintranet/model/absenwfh.dart';
import 'package:mobintranet/page/absen/log.dart';
import 'package:mobintranet/page/absenwfh/formisi.dart';
import 'package:mobintranet/page/absenwfh/log.dart';
import 'package:mobintranet/util/DbColors.dart';
import 'package:rflutter_alert/rflutter_alert.dart';


class Absenwfh extends StatefulWidget {

  final String tanggal;


  const Absenwfh({Key key, this.tanggal}) : super(key: key);

  @override
  _AbsenState createState() => _AbsenState();
}

class _AbsenState extends State<Absenwfh> {

    int tombolpulang=0;
    String tempat;
    List<Placemark> loc;
    Position pos ;
    AbsenMwfh dataabsen;
    DateTime jammasuk;
    DateTime jampulang;
    String lokasimasuk;
    String lokasipulang;
    bool _loading = true;
    String nama,nip,jabatan,unit,gol,bidang,tmtgol,tmtjab,tmtunit;

   _onBasicAlertPressed(context) {
     var alertStyle = AlertStyle(
      animationType: AnimationType.fromTop,
      isCloseButton: false,
      isOverlayTapDismiss: false,
      descStyle: TextStyle(fontWeight: FontWeight.bold),
      animationDuration: Duration(milliseconds: 400),
      alertBorder: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(0.0),
        side: BorderSide(
          color: Colors.grey,
        ),
      ),
      titleStyle: TextStyle(
        color: Colors.green,
      ),
      constraints: BoxConstraints.expand(width: 300)
    );

    // Alert dialog using custom alert style
    Alert(
      context: context,
      style: alertStyle,
      type: AlertType.info,
      title: "Berhasil Absen Masuk",
      desc: "Jangan Lupa untuk absen pulang nanti sore",
      buttons: [
        DialogButton(
          child: Text(
            "Kembali",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          color: Color.fromRGBO(0, 179, 134, 1.0),
          radius: BorderRadius.circular(0.0),
        ),
      ],
    ).show();
  }

  Future<Position> posisi()  {
    return  Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high); 
  }
  //end permission
Future<List<Placemark>> lokasi(double long,double lat){
  return Geolocator().placemarkFromCoordinates(lat, long);
}

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    API.getlaporan(mainBloc.tokenapi).then((res){
      setState(() {
        
      
        dataabsen=res;

        if(res.absenMasuk!=null){jammasuk= DateTime.parse(res.absenMasuk).toLocal();}
        if(res.absenPulang!=null){jampulang=DateTime.parse(res.absenPulang).toLocal();}
        if(res.lokasi!=null){lokasimasuk=res.lokasi;}
        if(res.lokasiPulang!=null){lokasipulang=res.lokasiPulang;}
        
        if(res.status==1){
          tombolpulang=2;
        }else{
            tombolpulang=1;
        }
      });
      print(res.absenMasuk);
      
    }).catchError((err){print(err);});

  
      API.getUser(mainBloc.tokenapi).then((res){
        // print(res.body);
        var isi= jsonDecode(res.body);
        print(isi);
        setState(() {
          nama=isi['nama'];
          nip=isi['nip'];
          jabatan=isi['jabatan'];
          unit=isi['perEselonDua'];
          gol=isi['golonganRuang'];
          bidang=isi['unitKerja'];
          tmtgol=isi['tmtGolongan'];
          tmtjab=isi['tmtJabatan'];
          tmtunit=isi['tmtUnit'];
          _loading = false;
        });
        
        print(isi);
      });

    posisi().then((res){
      setState(() {
        pos=res;
      });
      lokasi(res.longitude,res.latitude).then((res){
        setState(() {
          loc=res;
          loc.forEach((f)=>print(f.country));
          tempat=loc.toString();
          tempat=loc[0].name+','+loc[0].subLocality+','+loc[0].locality+','+loc[0].administrativeArea;
          _loading = false;
        });
      });
    });

  }

  @override
  Widget build(BuildContext context) {
   return Scaffold(
      appBar: AppBar(
        backgroundColor: fbiru1,
        leading: InkWell(
          onTap: (){
            Navigator.pop(context);
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(Icons.arrow_back),
            ],
          ),
        ),
        title: Text("Absensi"),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 20,top: 10),
            child: InkWell(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=> LogAbsenwfh()));
              },
              child: Row(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Icon(Icons.timer),
                      Text("Logg ",style: TextStyle(fontSize: 10),),
                    ],
                  ),
                  
                ],
              ),
            ),
          ),
        ],
      ),
      body: _loading? Loader():  SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              width: double.infinity,
              child: Card(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.person_outline, size: 40,color: fbiru1,),
                    Text(widget.tanggal,style: TextStyle(fontWeight: FontWeight.normal, fontSize: 20),),
                    Text("Lokasi Sekarang: ",style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),),
                    Text("$tempat ",style: TextStyle(fontWeight: FontWeight.normal, fontSize: 15),),
                    Text("($pos)",style: TextStyle(fontWeight: FontWeight.normal, fontSize: 15),),
                    SizedBox(height:10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      
                      children:<Widget>[
                          (tombolpulang!=2)?
                          (tombolpulang!=0 )?
                        FlatButton(
                          color: forange1,
                          textColor: Colors.white,
                          disabledColor: Colors.grey,
                          disabledTextColor: Colors.black,
                          padding: EdgeInsets.all(8.0),
                          splashColor: Colors.blueAccent,
                          onPressed: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context)=> Formisi()));
                          },
                          child: Row(
                            children: <Widget>[
                              Icon(Icons.timer),
                              SizedBox(width:5),
                              Text(
                                "Absen Pulang",
                                style: TextStyle(fontSize: 20.0),
                              ),
                            ],
                          ), 
                        )
                         :
                        FlatButton(
                          color: forange1,
                          textColor: Colors.white,
                          disabledColor: Colors.grey,
                          disabledTextColor: Colors.black,
                          padding: EdgeInsets.all(8.0),
                          splashColor: Colors.blueAccent,
                          onPressed: () {
                            print("tes absen masuk");
                           API.cekingps(mainBloc.tokenapi, pos.longitude, pos.latitude, tempat).then((res){
                               _onBasicAlertPressed(context);
                              API.getlaporan(mainBloc.tokenapi).then((res){
                                setState(() {
                                  dataabsen=res;
                                  
                                    if(res.absenMasuk!=null){jammasuk= DateTime.parse(res.absenMasuk).toLocal();}
                                    if(res.absenPulang!=null){jampulang=DateTime.parse(res.absenPulang).toLocal();}
                                    if(res.lokasi!=null){lokasimasuk=res.lokasi;}
                                    if(res.lokasiPulang!=null){lokasipulang=res.lokasiPulang;}

                                  if(res.status==1){
                                    tombolpulang=2;
                                  }else{
                                      tombolpulang=1;
                                  }
                                });
                                print(res.absenMasuk);
                                
                              }).catchError((err){print(err);});
                              setState(() {
                                tombolpulang=1;
                              });
                           }).catchError((err){print(err);});
                            // API.initlaporan(mainBloc.tokenapi).then((res){
                            //   print(res.absenMasuk);
                            //   // API.cekingps(mainBloc.tokenapi, pos.longitude, pos.latitude, tempat).catchError((err){print(err);});
                            //   _onBasicAlertPressed(context);
                            //   API.getlaporan(mainBloc.tokenapi).then((res){
                            //     setState(() {
                            //       dataabsen=res;
                                  
                            //         if(res.absenMasuk!=null){jammasuk= DateTime.parse(res.absenMasuk).toLocal();}
                            //         if(res.absenPulang!=null){jampulang=DateTime.parse(res.absenPulang).toLocal();}
                            //         if(res.lokasi!=null){lokasimasuk=res.lokasi;}
                            //         if(res.lokasiPulang!=null){lokasipulang=res.lokasiPulang;}

                            //       if(res.status==1){
                            //         tombolpulang=2;
                            //       }else{
                            //           tombolpulang=1;
                            //       }
                            //     });
                            //     print(res.absenMasuk);
                                
                            //   }).catchError((err){print(err);});
                            //   setState(() {
                            //     tombolpulang=1;
                            //   });
                              
                            // });
                          },
                          child: Row(
                            children: <Widget>[
                              Icon(Icons.timer),
                              SizedBox(width:5),
                              Text(
                                "Absen Masuk",
                                style: TextStyle(fontSize: 20.0),
                              ),
                            ],
                          ), 
                        )
                         :Container(),
                        
                        
                      ]
                    ),
                        SizedBox(height:10),
                  ],
                ), 
              ),
            ),
            Container(
              width: double.infinity,
              height: 100,
              child: Card(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text("Jam mulai kerja",style: TextStyle(fontSize: 16),),
                        (jammasuk!=null)?
                        Text("${jammasuk.hour}:${jammasuk.minute}:${jammasuk.second} ",style: TextStyle(fontSize: 25)):Text("-",style: TextStyle(fontSize: 25))
                      ],
                    ),
                    VerticalDivider(color: Colors.grey[250],thickness: 1),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text("Jam Pulang kerja",style: TextStyle(fontSize: 16)),
                        (jampulang!=null)?
                        Text("${jampulang.hour}:${jampulang.minute}:${jampulang.second} ",style: TextStyle(fontSize: 25)):Text("-",style: TextStyle(fontSize: 25))
                      ],
                    ),
                  ],
                ),
              ),
            ),

            Container(
              width: double.infinity,
              child: Column(
                children: <Widget>[
                  Card(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Flexible(child: 
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text("Lokasi mulai kerja",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
                            (lokasimasuk!=null)?
                            Text("${lokasimasuk}",style: TextStyle(fontSize: 16)):Text("-"),
                                
                          ],
                        ),),
                      ],
                    ),
                  ),
                  Card(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Flexible(child: 
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text("Lokasi Pulang kerja",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
                            (lokasipulang!=null)?
                            Text("${lokasipulang}",style: TextStyle(fontSize: 16)):Text("-"),
                          ],
                        ),),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            
          ],
        ),
      ),
    );
  }
}