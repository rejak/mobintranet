// import 'package:flutter/cupertino.dart';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:mobintranet/Loader.dart';
import 'package:mobintranet/api/manageapi.dart';
import 'package:mobintranet/bloc/bloc_absen/blocabsen_bloc.dart';
import 'package:mobintranet/bloc/blocdata.dart';
import 'package:mobintranet/bloc/main_bloc.dart';
import 'package:mobintranet/mainpage.dart';
import 'package:mobintranet/model/absenwfh.dart';
import 'package:mobintranet/page/absenwfh/absenwfh.dart';
import 'package:mobintranet/page/absenwfh/formisi.dart';
import 'package:mobintranet/page/absenwfh/log.dart';
import 'package:mobintranet/util/DbColors.dart';
import 'package:mobintranet/util/SDStyle.dart';
import 'package:mobintranet/util/colors.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:trust_location/trust_location.dart';
// import 'package:mobintranet/util/DbExtension.dart';
// import 'package:mobintranet/util/SDStyle.dart';
// import 'package:mobintranet/util/colors.dart';


class Nabsen extends StatefulWidget {
  final String tanggal;

  const Nabsen({Key key, this.tanggal}) : super(key: key);
  @override
  _NabsenState createState() => _NabsenState();
}

class _NabsenState extends State<Nabsen> {
    final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
    final bloc = BlocabsenBloc();
    int tombolpulang=0;
    String tempat;
    List<Placemark> loc;
    Position pos ;
    AbsenMwfh dataabsen;
    DateTime jammasuk;
    DateTime jampulang;
    String lokasimasuk;
    String lokasipulang;
    bool _loading = true;
    String nama,nip,jabatan,unit,gol,bidang,tmtgol,tmtjab,tmtunit;
    //mock loc
    String _latitude;
    String _longitude;
    bool _isMockLocation;
    

   _onBasicAlertPressed(context) {
     var alertStyle = AlertStyle(
      animationType: AnimationType.fromTop,
      isCloseButton: false,
      isOverlayTapDismiss: false,
      descStyle: TextStyle(fontWeight: FontWeight.bold),
      animationDuration: Duration(milliseconds: 400),
      alertBorder: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(0.0),
        side: BorderSide(
          color: Colors.grey,
        ),
      ),
      titleStyle: TextStyle(
        color: Colors.green,
      ),
      constraints: BoxConstraints.expand(width: 300)
    );

    // Alert dialog using custom alert style
    Alert(
      context: context,
      style: alertStyle,
      type: AlertType.info,
      title: "Berhasil Absen Masuk",
      desc: "Jangan Lupa untuk absen pulang nanti sore",
      buttons: [
        DialogButton(
          child: Text(
            "Kembali",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          color: Color.fromRGBO(0, 179, 134, 1.0),
          radius: BorderRadius.circular(0.0),
        ),
      ],
    ).show();
  }
  Future<Position> posisi()  {
    return  Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high); 
  }
  //end permission
  Future<List<Placemark>> lokasi(double long,double lat){
    return Geolocator().placemarkFromCoordinates(lat, long);
  }

  /// get location method, use a try/catch PlatformException.
  Future<void> getLocation() async {
    try {
      TrustLocation.onChange.listen((values) => setState(() {
            _latitude = values.latitude;
            _longitude = values.longitude;
            _isMockLocation = values.isMockLocation;
            
          }));
    } on PlatformException catch (e) {
      print('PlatformException $e');
    }
    if (!mounted) return;
    
  }

  void showsnackbar(String pesan){
    scaffoldKey.currentState.showSnackBar(
       new SnackBar(
          backgroundColor: Colors.red,
          content: new Text('Fake Gps Terdeteksi,nonaktifkan untuk dapat melakukan absen !!!')
       )
);
  }

  @override
  void dispose() {
    // AnimationController.di;
    super.dispose();
  }

  

  @override
  void initState() {
    super.initState();
    TrustLocation.start(5);
    getLocation().then((value) {
      if(!mounted){
        return;
      }
    });
    API.getlaporan(mainBloc.tokenapi).then((res){
      setState(() {
        dataabsen=res;

        if(res.absenMasuk!=null){jammasuk= DateTime.parse(res.absenMasuk).toLocal();}
        if(res.absenPulang!=null){jampulang=DateTime.parse(res.absenPulang).toLocal();}
        if(res.lokasi!=null){lokasimasuk=res.lokasi;}
        if(res.lokasiPulang!=null){lokasipulang=res.lokasiPulang;}
        
        if(res.status==1){
          tombolpulang=2;
        }else{
            tombolpulang=1;
        }
      });
      print(res.absenMasuk);
      
    }).catchError((err){print(err);});

    API.getUser(mainBloc.tokenapi).then((res){
        // print(res.body);
        var isi= jsonDecode(res.body);
        print(isi);
        setState(() {
          nama=isi['nama'];
          nip=isi['nip'];
          jabatan=isi['jabatan'];
          unit=isi['perEselonDua'];
          gol=isi['golonganRuang'];
          bidang=isi['unitKerja'];
          tmtgol=isi['tmtGolongan'];
          tmtjab=isi['tmtJabatan'];
          tmtunit=isi['tmtUnit'];
          // _loading = false;
        });
       
        print(isi);
      });

    posisi().then((res){
      setState(() {
        pos=res;
        print(pos);
      });
      
      lokasi(res.longitude,res.latitude).then((res){
        setState(() {
          loc=res;
          // loc.forEach((f)=>print(f.country));
          tempat=loc.toString();
          tempat=loc[0].name+','+loc[0].subLocality+','+loc[0].locality+','+loc[0].administrativeArea;
           _loading = false;
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    // changeStatusColor(fbiru1);
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        backgroundColor: fbiru1,
        leading: InkWell(
          onTap: (){
            Navigator.pop(context);
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(Icons.arrow_back),
            ],
          ),
        ),
        title: Text(widget.tanggal),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 20,top: 10),
            child: InkWell(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=> LogAbsenwfh()));
              },
              child: Row(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Icon(Icons.timer),
                      Text("Log ",style: TextStyle(fontSize: 10),),
                    ],
                  ),
                  
                ],
              ),
            ),
          ),
        ],
      ),
      body: _loading? Loader(): SingleChildScrollView(
      child: Container(
        child: Stack(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(5),
              height: 280,
              width: size.width,
              // padding: EdgeInsets.only(top: 5),
              color: fbiru1,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    child: Text(
                      nama,
                      style: boldTextStyle(textColor: Colors.white),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 5),
                    child: Text(
                      nip,
                      style: secondaryTextStyle(
                        size: 14,
                        textColor: Colors.white.withOpacity(0.7),
                      ),
                    ),
                  ),
                  Container(
                    child: Text(
                      jabatan+" di "+unit,
                      textAlign: TextAlign.center,
                      style: secondaryTextStyle(
                        size: 12,
                        textColor: Colors.white.withOpacity(0.7),
                      ),
                    ),
                  ),
                  SizedBox(height: 10,),
                  
                  Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      
                      children:<Widget>[(tombolpulang!=1 && tombolpulang!=0 )?
                        FlatButton(
                          color: fkuning1,
                          textColor: Colors.white,
                          disabledColor: Colors.grey,
                          disabledTextColor: Colors.white,
                          padding: EdgeInsets.all(8.0),
                          splashColor: Colors.blueAccent,
                          onPressed:(Platform.isAndroid)?(_isMockLocation==null)?null:_isMockLocation?(){showsnackbar("pesan");}: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context)=> Formisi()));}
                            :() {Navigator.push(context, MaterialPageRoute(builder: (context)=> Formisi()));},
                          child: Row(
                            children: <Widget>[
                              Icon(Icons.timer),
                              SizedBox(width:5),
                              Text(
                                "Absen Pulang (${mainBloc.statuswork})",
                                style: TextStyle(fontSize: 20.0),
                              ),
                            ],
                          ), 
                        ):(tombolpulang != 1)?
                        BlocListener<BlocabsenBloc, BlocabsenState>(
                           cubit: bloc,
                           listener: (context, state) {
                             if(state is PostAbsenSukses){
                               TrustLocation.stop();
                                dataabsen=state.data;
                                  if(state.data.absenMasuk!=null){
                                    jammasuk= DateTime.parse(state.data.absenMasuk).toLocal();
                                     blocdata.setjammasuk("${jammasuk.hour}:${jammasuk.minute}:${jammasuk.second}"); }
                                  else{
                                    blocdata.setjammasuk("-");
                                  }
                                  if(state.data.lokasi!=null){lokasimasuk=state.data.lokasi;}
                                  if(state.data.lokasiPulang!=null){lokasipulang=state.data.lokasiPulang;}

                                if(state.data.status==1){
                                  tombolpulang=2;
                                }else{
                                    tombolpulang=1;
                                }
                                Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (contex)=>MainPage(token: mainBloc.tokenapi)), (Route<dynamic> route) => false);
                             }
                             if(state is PostAbsenWaiting){
                               return Container(child: Loader());
                             }
                             if(state is PostAbsenError){
                               tombolpulang=1;
                               return Container(child: Text("Error"),);
                             }
                             if(state is GetInLocationSukses){
                               _onBasicAlertPressed(context);
                               bloc..add(PostAbsenEvent());
                             }
                             if(state is GetInLocationError){
                               return Container(child: Text("Error"),);
                             }
                             if(state is GetInLocationWaiting){
                               return Container(child: Loader(),);
                             }
                           },
                           child: BlocBuilder<BlocabsenBloc, BlocabsenState>(
                           cubit: bloc,
                           builder: (context, state) {
                             return buildFlatButtonsiang(context, bloc);
                           },
                         ),
                         )    
                        :
                         BlocListener<BlocabsenBloc, BlocabsenState>(
                           cubit: bloc,
                           listener: (context, state) {
                             if(state is PostAbsenSukses){
                               TrustLocation.stop();
                                dataabsen=state.data;
                                  if(state.data.absenMasuk!=null){
                                    jammasuk= DateTime.parse(state.data.absenMasuk).toLocal();
                                     blocdata.setjammasuk("${jammasuk.hour}:${jammasuk.minute}:${jammasuk.second}"); }
                                  else{
                                    blocdata.setjammasuk("-");
                                  }
                                  if(state.data.lokasi!=null){lokasimasuk=state.data.lokasi;}
                                  if(state.data.lokasiPulang!=null){lokasipulang=state.data.lokasiPulang;}

                                if(state.data.status==1){
                                  tombolpulang=2;
                                }else{
                                    tombolpulang=1;
                                }
                                Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (contex)=>MainPage(token: mainBloc.tokenapi)), (Route<dynamic> route) => false);
                             }
                             if(state is PostAbsenWaiting){
                               return Container(child: Loader());
                             }
                             if(state is PostAbsenError){
                               tombolpulang=1;
                               return Container(child: Text("Error"),);
                             }
                             if(state is GetInLocationSukses){
                               _onBasicAlertPressed(context);
                               bloc..add(PostAbsenEvent());
                             }
                             if(state is GetInLocationError){
                               return Container(child: Text("Error"),);
                             }
                             if(state is GetInLocationWaiting){
                               return Container(child: Loader(),);
                             }
                           },
                           child: BlocBuilder<BlocabsenBloc, BlocabsenState>(
                           cubit: bloc,
                           builder: (context, state) {
                             return buildFlatButton(context, bloc);
                           },
                         ),
                         )              
                      ]
                    ),
                  
                //  (Platform.isAndroid)?Text('Running on: ${mainBloc.imei}'):Container(),

                  SizedBox(height: 15,),
                    Container(
                    padding: EdgeInsets.all(5),
                    decoration: boxDecoration(
                        radius: 8,
                        backGroundColor: Colors.white,
                        spreadRadius: 2,
                        blurRadius: 10,
                      ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.only(left: 15),
                          child: Icon(Icons.location_on,color: Colors.red)),
                        Expanded(
                          child: Container(
                            child: 
                              (tempat!=null)?
                              Text(tempat,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.black
                              ),):Text("-",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.black
                              ),),
                          ),
                        ),
                        
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 16, right: 16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Container(
                      margin: EdgeInsets.only(
                        top: 280.00 - 50,
                      ),
                      padding: EdgeInsets.all(20),
                      decoration: boxDecoration(
                        radius: 8,
                        backGroundColor: Colors.white,
                        spreadRadius: 2,
                        blurRadius: 10,
                      ),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'Jam mulai kerja :',
                            style: boldTextStyle(
                                textColor: Colors.black, size: 11),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          
                          (jammasuk!=null)?
                          Text(
                            '${jammasuk.hour}:${jammasuk.minute}:${jammasuk.second} ',
                            style: boldTextStyle(
                              textColor:
                                  fhijau1,
                              size: 26,
                            ),
                          ):Text(
                            '-',
                            style: boldTextStyle(
                              textColor:
                                  fhijau1,
                              size: 26,
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          // Text(
                          //   'Chemist',
                          //   style: secondaryTextStyle(
                          //       textColor: Colors.grey.withOpacity(0.7),
                          //       size: 14),
                          // ),
                        ],
                      ),
                    ),
                  ),
                   SizedBox(
                    width: 15,
                  ),
                  
                  Expanded(
                    flex: 1,
                    child: Container(
                      margin: EdgeInsets.only(
                        top: 280.00 - 50,
                      ),
                      padding: EdgeInsets.all(20),
                      decoration: boxDecoration(
                          radius: 8,
                          backGroundColor: Colors.white,
                          spreadRadius: 2,
                          blurRadius: 10),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'Jam siang',
                            style: boldTextStyle(
                                textColor: Colors.black, size: 11),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          (jampulang!=null)?
                          Text(
                            '${jampulang.hour}:${jampulang.minute}:${jampulang.second} ',
                            style: boldTextStyle(
                              textColor:
                                  fkuning1,
                              size: 26,
                            ),
                          ):Text(
                            '-',
                            style: boldTextStyle(
                              textColor:
                                  fkuning1,
                              size: 26,
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          // Text(
                          //   'Maths',
                          //   style: secondaryTextStyle(
                          //       textColor: Colors.grey.withOpacity(0.7),
                          //       size: 14),
                          // ),
                        ],
                      ),
                    ),
                  ),
                
                  SizedBox(
                    width: 15,
                  ),

                  Expanded(
                    flex: 1,
                    child: Container(
                      margin: EdgeInsets.only(
                        top: 280.00 - 50,
                      ),
                      padding: EdgeInsets.all(20),
                      decoration: boxDecoration(
                          radius: 8,
                          backGroundColor: Colors.white,
                          spreadRadius: 2,
                          blurRadius: 10),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'Jam pulang kerja',
                            style: boldTextStyle(
                                textColor: Colors.black, size: 11),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          (jampulang!=null)?
                          Text(
                            '${jampulang.hour}:${jampulang.minute}:${jampulang.second} ',
                            style: boldTextStyle(
                              textColor:
                                  fkuning1,
                              size: 26,
                            ),
                          ):Text(
                            '-',
                            style: boldTextStyle(
                              textColor:
                                  fkuning1,
                              size: 26,
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          // Text(
                          //   'Maths',
                          //   style: secondaryTextStyle(
                          //       textColor: Colors.grey.withOpacity(0.7),
                          //       size: 14),
                          // ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Center(
              child: Container(
                margin: EdgeInsets.only(
                    top: 280.00 + 100, bottom: 25, left: 16, right: 16),
                padding: EdgeInsets.only(
                  top: 15,
                  left: 15,
                  right: 15,
                  bottom: 15,
                ),
                decoration: boxDecorations(
                  showShadow: true,
                ),
                child: Column(
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Text(
                            'Lokasi Mulai Kerja :',
                            style: boldTextStyle(
                                textColor: Colors.black, size: 14),
                          ),
                        SizedBox(height: 10,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 15),
                              child: Icon(Icons.location_on,color: Colors.red)),
                            Expanded(
                              child: Container(
                                child: 
                                (lokasimasuk!=null)?
                                Text(lokasimasuk,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Colors.black
                                  ),):Text("-",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Colors.black
                                  ),),
                              ),
                            ),
                            
                          ],
                        ),
                        SizedBox(height: 5,),
                        Divider(color: fabu3,),
                        SizedBox(height: 5,),
                        Text(
                            'Lokasi Absen Siang :',
                            style: boldTextStyle(
                                textColor: Colors.black, size: 14),
                          ),
                        SizedBox(height: 10,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 15),
                              child: Icon(Icons.location_on,color: Colors.red)),
                            Expanded(
                              child: Container(
                                child: 
                                (lokasipulang!=null)?
                                Text(lokasipulang,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Colors.black
                                  ),):Text("-",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Colors.black
                                  ),),
                              ),
                            ),
                            
                          ],
                        ),
                        Divider(color: fabu3,),
                        SizedBox(height: 5,),
                        Text(
                            'Lokasi Pulang Kerja :',
                            style: boldTextStyle(
                                textColor: Colors.black, size: 14),
                          ),
                        SizedBox(height: 10,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 15),
                              child: Icon(Icons.location_on,color: Colors.red)),
                            Expanded(
                              child: Container(
                                child: 
                                (lokasipulang!=null)?
                                Text(lokasipulang,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Colors.black
                                  ),):Text("-",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Colors.black
                                  ),),
                              ),
                            ),
                            
                          ],
                        ),
                      ],
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    ),
    );
  }

  FlatButton buildFlatButton(BuildContext context,BlocabsenBloc bloc) {
    return FlatButton(
                        color: fhijau1,
                        textColor: Colors.white,
                        disabledColor: Colors.grey,
                        disabledTextColor: Colors.black,
                        padding: EdgeInsets.all(8.0),
                        splashColor: Colors.blueAccent,
                        onPressed: (Platform.isAndroid)? (_isMockLocation==null)?null:_isMockLocation?(){showsnackbar("pesan");}: () {
                          print("tes absen masuk");
                          bloc..add(GetInLocationEvent(longitude: pos.longitude, latitude: pos.latitude, tempat: tempat));
                        //  API.cekingps(mainBloc.tokenapi, pos.longitude, pos.latitude, tempat).then((res){
                        //      _onBasicAlertPressed(context);
                        //     API.getlaporan(mainBloc.tokenapi).then((res){

                        //       TrustLocation.stop();
                        //       setState(() {
                        //         dataabsen=res;
                        //           if(res.absenMasuk!=null){
                        //             jammasuk= DateTime.parse(res.absenMasuk).toLocal();
                        //              blocdata.setjammasuk("${jammasuk.hour}:${jammasuk.minute}:${jammasuk.second}"); }
                        //           else{
                        //             blocdata.setjammasuk("-");
                        //           }
                        //           if(res.lokasi!=null){lokasimasuk=res.lokasi;}
                        //           if(res.lokasiPulang!=null){lokasipulang=res.lokasiPulang;}

                        //         if(res.status==1){
                        //           tombolpulang=2;
                        //         }else{
                        //             tombolpulang=1;
                        //         }
                        //       });
                        //       print(res.absenMasuk);
                              
                        //     }).catchError((err){print(err);});
                        //     setState(() {
                        //       tombolpulang=1;
                        //     });
                        //  }).catchError((err){print(err);});
                          
                        }:
                         () {
                          print("tes absen masuk");
                          bloc..add(GetInLocationEvent(longitude: pos.longitude, latitude: pos.latitude, tempat: tempat));
                        //  API.cekingps(mainBloc.tokenapi, pos.longitude, pos.latitude, tempat).then((res){
                        //      _onBasicAlertPressed(context);
                        //     API.getlaporan(mainBloc.tokenapi).then((res){

                        //       TrustLocation.stop();
                        //       setState(() {
                        //         dataabsen=res;
                        //           if(res.absenMasuk!=null){
                        //             jammasuk= DateTime.parse(res.absenMasuk).toLocal();
                        //              blocdata.setjammasuk("${jammasuk.hour}:${jammasuk.minute}:${jammasuk.second}"); }
                        //           else{
                        //             blocdata.setjammasuk("-");
                        //           }
                        //           if(res.lokasi!=null){lokasimasuk=res.lokasi;}
                        //           if(res.lokasiPulang!=null){lokasipulang=res.lokasiPulang;}

                        //         if(res.status==1){
                        //           tombolpulang=2;
                        //         }else{
                        //             tombolpulang=1;
                        //         }
                        //       });
                        //       print(res.absenMasuk);
                              
                        //     }).catchError((err){print(err);});
                        //     setState(() {
                        //       tombolpulang=1;
                        //     });
                        //  }).catchError((err){print(err);});
                          
                        },
                        child: Row(
                          children: <Widget>[
                            Icon(Icons.timer),
                            SizedBox(width:5),
                            Text(
                              "Absen Masuk (${mainBloc.statuswork})",
                              style: TextStyle(fontSize: 20.0),
                            ),
                          ],
                        ), 
                      );
  }

   FlatButton buildFlatButtonsiang(BuildContext context,BlocabsenBloc bloc) {
    return FlatButton(
                        color: fhijau1,
                        textColor: Colors.white,
                        disabledColor: Colors.grey,
                        disabledTextColor: Colors.black,
                        padding: EdgeInsets.all(8.0),
                        splashColor: Colors.blueAccent,
                        onPressed: (Platform.isAndroid)? (_isMockLocation==null)?null:_isMockLocation?(){showsnackbar("pesan");}: () {
                          print("tes absen siang");
                          bloc..add(GetInLocationEvent(longitude: pos.longitude, latitude: pos.latitude, tempat: tempat));
                        //  API.cekingps(mainBloc.tokenapi, pos.longitude, pos.latitude, tempat).then((res){
                        //      _onBasicAlertPressed(context);
                        //     API.getlaporan(mainBloc.tokenapi).then((res){

                        //       TrustLocation.stop();
                        //       setState(() {
                        //         dataabsen=res;
                        //           if(res.absenMasuk!=null){
                        //             jammasuk= DateTime.parse(res.absenMasuk).toLocal();
                        //              blocdata.setjammasuk("${jammasuk.hour}:${jammasuk.minute}:${jammasuk.second}"); }
                        //           else{
                        //             blocdata.setjammasuk("-");
                        //           }
                        //           if(res.lokasi!=null){lokasimasuk=res.lokasi;}
                        //           if(res.lokasiPulang!=null){lokasipulang=res.lokasiPulang;}

                        //         if(res.status==1){
                        //           tombolpulang=2;
                        //         }else{
                        //             tombolpulang=1;
                        //         }
                        //       });
                        //       print(res.absenMasuk);
                              
                        //     }).catchError((err){print(err);});
                        //     setState(() {
                        //       tombolpulang=1;
                        //     });
                        //  }).catchError((err){print(err);});
                          
                        }:
                         () {
                          print("tes absen siang");
                          bloc..add(GetInLocationEvent(longitude: pos.longitude, latitude: pos.latitude, tempat: tempat));
                        //  API.cekingps(mainBloc.tokenapi, pos.longitude, pos.latitude, tempat).then((res){
                        //      _onBasicAlertPressed(context);
                        //     API.getlaporan(mainBloc.tokenapi).then((res){

                        //       TrustLocation.stop();
                        //       setState(() {
                        //         dataabsen=res;
                        //           if(res.absenMasuk!=null){
                        //             jammasuk= DateTime.parse(res.absenMasuk).toLocal();
                        //              blocdata.setjammasuk("${jammasuk.hour}:${jammasuk.minute}:${jammasuk.second}"); }
                        //           else{
                        //             blocdata.setjammasuk("-");
                        //           }
                        //           if(res.lokasi!=null){lokasimasuk=res.lokasi;}
                        //           if(res.lokasiPulang!=null){lokasipulang=res.lokasiPulang;}

                        //         if(res.status==1){
                        //           tombolpulang=2;
                        //         }else{
                        //             tombolpulang=1;
                        //         }
                        //       });
                        //       print(res.absenMasuk);
                              
                        //     }).catchError((err){print(err);});
                        //     setState(() {
                        //       tombolpulang=1;
                        //     });
                        //  }).catchError((err){print(err);});
                          
                        },
                        child: Row(
                          children: <Widget>[
                            Icon(Icons.timer),
                            SizedBox(width:5),
                            Text(
                              "Absen Siang (${mainBloc.statuswork})",
                              style: TextStyle(fontSize: 20.0),
                            ),
                          ],
                        ), 
                      );
  }
}


