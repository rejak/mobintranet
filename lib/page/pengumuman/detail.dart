import 'package:flutter/material.dart';

class DetailNotif extends StatefulWidget {
  @override
  _DetailNotifState createState() => _DetailNotifState();
}

class _DetailNotifState extends State<DetailNotif> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
    body: NestedScrollView(
      headerSliverBuilder: (BuildContext context, bool innerscroll){
        return <Widget>[
          SliverAppBar(
            expandedHeight: 200.0,
            floating: false,
            pinned: true,
            flexibleSpace: FlexibleSpaceBar(
              centerTitle: true,
              title: Text("Pengumuman",style: TextStyle(color: Colors.white,fontSize: 16.0),),
              background: Image.network("http://lan.go.id/media/k2/items/cache/ffe1991fca173b5b449421f2bfcb87a0_XL.jpg",fit: BoxFit.cover,),
            ),
          ),
        ];
      },
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 10,horizontal: 20),
          child: Column(
            children: <Widget>[
              SizedBox(height: 20,),
              Text("LAN Sambut CPNS Lulusan PKN STAN",textAlign: TextAlign.center,style: TextStyle(fontSize: 25),),
              SizedBox(height: 5,),
              Text("22 Jan 2020 09.30",style: TextStyle(fontSize: 14 ),),
              SizedBox(height: 5,),
              Text("Jakarta - Lembaga Administrasi Negara (LAN) melaksanakan orientasi untuk Calon Pegawai Negeri Sipil (CPNS) LAN tahun 2020 di Ruang Corpu, Kantor LAN, Jakarta, Senin (20/1). Penerimaan CPNS lulusan Politeknik Keuangan Negara Sekolah Tinggi Akuntansi Negara (PKN STAN) merupakan tindak lanjut nota kesepahaman antara Kementerian Keuangan dan Lembaga Administrasi Negara (LAN) dalam Program Kemenkeu Leaders Factory beberapa waktu lalu. Dalam sambutannya Sekretaris Utama LAN, Dra. Reni Suzana, MPPM mengenalkan budaya kerja di LAN berdasarkan 4 nilai dasar yang dimiliki oleh LAN yaitu integritas, profesional, inovatif dan peduli.“Integritas meliputi kesesuaian antara perkataan dan perbuatan, memegang amanah. Profesional yang berarti bekerja secara bertanggungjawab dan sesuai dengan kompetensi. Inovatif, mampu berpikir kreatif dan beradaptasi dengan perubahan. Peduli, memiliki kepedulian akan sesama pegawai LAN.” tambah Reni.Sestama LAN berharap nantinya para CPNS ini mampu segera melebur dan beradaptasi dengan budaya kerja di LAN.“Anggap ini sebagai rumah baru kalian, karena kita ingin siapapun yang ada di dalamnya merasa nyaman. Perubahan itu terjadi dari waktu ke waktu, dan bukan LAN namanya kalau kaget dengan perubahan. Oleh karena itu, saya mengucapkan selamat datang di rumah perubahan” tutup Sestama.Dalam kesempatan itu hadir pula Kepala Biro SDM dan Umum, M. Yusuf Gunawan Idris, S.IP.,ME, Kepala Bagian Keuangan Dwi Ariany, S.Sos., M.Si. dan juga Kepala Bagian Sumber Daya Manusia, Awan Hari Murtiadi, S.E. (humas)",textAlign: TextAlign.justify,style: TextStyle(fontSize: 16),),
            ],
          ),
        ),
      ),
    ),
    );
  }
}