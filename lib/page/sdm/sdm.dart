import 'package:flutter/material.dart';
import 'package:mobintranet/page/sdm/daftarpegawai.dart';
import 'package:mobintranet/page/sdm/data.dart';
import 'package:mobintranet/util/DbColors.dart';

class SDM extends StatefulWidget {
  @override
  _SDMState createState() => _SDMState();
}



class _SDMState extends State<SDM> {
  int _select=0;
  final _page = [
    DaftarPegawai(),
    DataPegawai()
  ];

  void _ontap(index){
    setState(() {
      _select=index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // bottomNavigationBar: BottomNavigationBar(
      //   items: <BottomNavigationBarItem>[
      //     BottomNavigationBarItem(
      //       backgroundColor: fbiru1,
      //       icon: Icon(Icons.person),
      //       title: Text("Pegawai"),
      //     ),
      //     BottomNavigationBarItem(
      //       backgroundColor: fbiru1,
      //       icon: Icon(Icons.trending_up),
      //       title: Text("Data"),
      //     ),
      //   ],
      //   type: BottomNavigationBarType.fixed,
      //   currentIndex: _select,
      //   onTap: _ontap,
      // ),
      
      body: _page.elementAt(_select),
    );
  }
}