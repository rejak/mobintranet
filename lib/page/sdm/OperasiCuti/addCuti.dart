import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobintranet/model/addCutiModel.dart/addModel.dart';
import 'package:mobintranet/util/DbColors.dart';
import 'package:mobintranet/util/colors.dart';
import 'package:mobintranet/util/DbConstant.dart';
import 'package:mobintranet/util/images.dart';
import 'package:mobintranet/util/DbStrings.dart';
import 'package:mobintranet/widget/addCuti.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_rounded_date_picker/rounded_picker.dart';
import 'package:easy_localization/easy_localization.dart';
import 'dart:convert' as JSON;

class AddCuti extends StatefulWidget {
  static String tag = '/QIBusSetting';

  @override
  AddCutiState createState() => AddCutiState();
}

class AddCutiState extends State<AddCuti> {
  bool mEmailNotification = false;
  bool mContactNotification = false;
  String _selectedLocation = 'English';
  String _selectedLocation1 = 'India';
  List<String> language = <String>['English', 'Arabic', 'French'];
  List<String> country = <String>['India', 'United State', 'Canada'];
  List<ListDateCuti> listDate = List<ListDateCuti>();

  Widget pilihTanggalCuti(var heading, var subLabel, var value) {
    return Container(
      decoration: boxDecoration(showShadow: true),
      padding: EdgeInsets.fromLTRB(spacing_standard, spacing_standard,
          spacing_standard, spacing_standard_new),
      margin: EdgeInsets.only(
          left: spacing_standard_new,
          right: spacing_standard_new,
          bottom: spacing_standard_new),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
              padding: const EdgeInsets.fromLTRB(
                  spacing_standard, 0, spacing_standard, 0),
              child: text(tr('tab_service.configure_time_leave.Add_Leave.form.select_a_leave_date'), fontFamily: fontMedium)),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(
                    20, spacing_standard, 16, spacing_standard),
                child: text(
                    (listDate.length == 0)
                        ? "-"
                        : listDate[0].day.toString() +
                            "-" +
                            listDate[0].month.toString() +
                            "-" +
                            listDate[0].year.toString() +
                            " s/d " +
                            listDate[listDate.length - 1].day.toString() +
                            "-" +
                            listDate[listDate.length - 1].month.toString() +
                            "-" +
                            listDate[listDate.length - 1].year.toString(),
                    textColor: qIBus_textChild),
              ),
              InkWell(
                onTap: () {
                  return DatePickerContoh();
                },
                child: Icon(Icons.calendar_today),
              )
            ],
          )
        ],
      ),
    );
  }

  Widget formNip(var heading, var subLabel, List<String> a, String value) {
    return Container(
      decoration: boxDecoration(showShadow: true),
      padding: EdgeInsets.all(spacing_standard_new),
      margin: EdgeInsets.only(
          left: spacing_standard_new,
          right: spacing_standard_new,
          bottom: spacing_standard_new),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          // Padding(
          //     padding: const EdgeInsets.fromLTRB(
          //         spacing_standard, 0, spacing_standard, 0),
          //     child: text(heading, fontFamily: fontMedium)),
          Row(
            children: <Widget>[
              Flexible(
                  flex: 2,
                  child: TextFormField(
                    // controller: _nomorId,
                    keyboardType: TextInputType.multiline,
                    minLines: 1,
                    maxLines: 5,
                    decoration: InputDecoration(
                        hintText: tr('tab_service.configure_time_leave.Add_Leave.form.num_nip'),
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black)),
                        hintStyle: TextStyle(color: Colors.black),
                        labelText: tr('tab_service.configure_time_leave.Add_Leave.form.num_nip'),
                        labelStyle: TextStyle(color: Colors.black)),
                  )),
              // DropdownButtonHideUnderline(
              //   child: DropdownButton<String>(
              //     value: value,
              //     items: a.map((String value) {
              //       return new DropdownMenuItem<String>(
              //         value: value,
              //         child: text(value,
              //             fontSize: textSizeMedium, textColor: qIBus_textChild),
              //       );
              //     }).toList(),
              //     onChanged: (newValue) {
              //       setState(() {
              //         value = newValue;
              //       });
              //     },
              //   ),
              // )
            ],
          )
        ],
      ),
    );
  }

  Widget formStatusAlasanDanJenisIzin(
      var heading, var subLabel, List<String> a, String value) {
    return Container(
      decoration: boxDecoration(showShadow: true),
      padding: EdgeInsets.all(spacing_standard_new),
      margin: EdgeInsets.only(
          left: spacing_standard_new,
          right: spacing_standard_new,
          bottom: spacing_standard_new),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                  padding: const EdgeInsets.fromLTRB(
                      spacing_standard, 0, spacing_standard, 0),
                  child: text(tr('tab_service.configure_time_leave.Add_Leave.form.reason_of_leave'), fontFamily: fontMedium)),
              Padding(
                  padding: const EdgeInsets.fromLTRB(
                      spacing_standard, 0, spacing_standard, 0),
                  child: text(tr('tab_service.configure_time_leave.Add_Leave.form.types_of_leave'), fontFamily: fontMedium)),
            ],
          ),
          // Padding(
          //     padding: const EdgeInsets.fromLTRB(
          //         spacing_standard, 0, spacing_standard, 0),
          //     child: text(heading, fontFamily: fontMedium)),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(
                    spacing_standard, 0, spacing_standard, 0),
                child: DropdownButton<String>(
                    value: value,
                    items: a.map((String value) {
                      return new DropdownMenuItem<String>(
                        value: value,
                        child: text(value,
                            fontSize: textSizeMedium,
                            textColor: qIBus_textChild),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      setState(() {
                        value = newValue;
                      });
                    },
                  ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(
                    spacing_standard, 0, spacing_standard, 0),
                child: DropdownButtonHideUnderline(
                  child: DropdownButton<String>(
                    value: value,
                    items: a.map((String value) {
                      return new DropdownMenuItem<String>(
                        value: value,
                        child: text(value,
                            fontSize: textSizeMedium,
                            textColor: qIBus_textChild),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      setState(() {
                        value = newValue;
                      });
                    },
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  DatePickerContoh() async {
    DateTime newDateTime = await showRoundedDatePicker(
        listDateDisabled: [
          DateTime.now().subtract(Duration(days: 2)),
          DateTime.now().subtract(Duration(days: 4)),
          DateTime.now().subtract(Duration(days: 6)),
          DateTime.now().subtract(Duration(days: 8)),
          DateTime.now().subtract(Duration(days: 10)),
          DateTime.now().add(Duration(days: 2)),
          DateTime.now().add(Duration(days: 4)),
          DateTime.now().add(Duration(days: 6)),
          DateTime.now().add(Duration(days: 8)),
          DateTime.now().add(Duration(days: 10)),
        ],
        textActionButton: tr('tab_service.configure_time_leave.filter_by.button.clear'),
        onTapActionButton: () {
          setState(() {
            listDate = [];
          });
        },
        textPositiveButton: tr('tab_service.configure_time_leave.filter_by.button.ok'),
        textNegativeButton: tr('tab_service.configure_time_leave.filter_by.button.close'),
        context: context,
        background: Colors.white,
        theme: ThemeData.dark(),
        // theme: ThemeData(
        //   primaryColor: Colors.white,
        //   accentColor: Colors.green[800],
        //   dialogBackgroundColor: Colors.black,
        //   textTheme: TextTheme(
        //     headline: TextStyle(color: Colors.black),
        //     subhead: TextStyle(color: Colors.white),
        //     subtitle: TextStyle(color: Colors.white),
        //     // subtitle1: TextStyle(color: Colors.white),
        //     // subtitle2: TextStyle(color: Colors.white),
        //     body1: TextStyle(color: Colors.white),
        //     caption: TextStyle(color: Colors.white),
        //   ),
        //   disabledColor: Colors.blueGrey,
        //   accentTextTheme: TextTheme(
        //     body2: TextStyle(color: Colors.green[200]),
        //   ),
        // ),
        imageHeader: AssetImage("assets/sdm/header2.png"),
        description: "LAN RI",
        onTapDay: (DateTime dateTime, bool available) {
          print(dateTime.day);
          setState(() {
            var day = dateTime.day;
            var month = dateTime.month;
            var year = dateTime.year;
            listDate.add(ListDateCuti(day, month, year));
            print(listDate.length);
          });
          if (!available) {
            showDialog(
                context: context,
                builder: (c) => CupertinoAlertDialog(
                      title: Text("This date cannot be selected."),
                      actions: <Widget>[
                        CupertinoDialogAction(
                          child: Text("OK"),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        )
                      ],
                    ));
          }
          return available;
        },
        builderDay: (DateTime dateTime, bool isCurrentDay, bool isSelected,
            TextStyle defaultTextStyle) {
          // print(isCurrentDay);
          // print(dateTime.day);
          // print(listDate[0].day);
          for (var i = 0; i < listDate.length; i++) {
            if (dateTime.day == listDate[i].day) {
              return Container(
                decoration: BoxDecoration(
                    color: Colors.orange[600], shape: BoxShape.circle),
                child: Center(
                  child: Text(
                    dateTime.day.toString(),
                    style: defaultTextStyle,
                  ),
                ),
              );
            }
          }
          if (isSelected) {
            return Container(
              decoration: BoxDecoration(
                  color: Colors.orange[600], shape: BoxShape.circle),
              child: Center(
                child: Text(
                  dateTime.day.toString(),
                  style: defaultTextStyle,
                ),
              ),
            );
          }

          if (dateTime.day == 10) {
            return Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.pink[300], width: 4),
                  shape: BoxShape.circle),
              child: Center(
                child: Text(
                  dateTime.day.toString(),
                  style: defaultTextStyle,
                ),
              ),
            );
          }
          if (dateTime.day == 12) {
            return Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.pink[300], width: 4),
                  shape: BoxShape.circle),
              child: Center(
                child: Text(
                  dateTime.day.toString(),
                  style: defaultTextStyle,
                ),
              ),
            );
          }

          return Container(
            child: Center(
              child: Text(
                dateTime.day.toString(),
                style: defaultTextStyle,
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en', 'US'), // English
        const Locale('th', 'TH'), // Thai
      ],
      home: Scaffold(
          backgroundColor: qIBus_app_background,
          body: Column(
            children: <Widget>[
              TopBarAdd(true),
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        pilihTanggalCuti(
                            QIBus_title_email_notification_settings,
                            QIBus_txt_email_notification,
                            mEmailNotification),
                        formNip(QIBus_title_language_setting,
                            QIBus_lbl_language, language, _selectedLocation),
                        formStatusAlasanDanJenisIzin(
                            QIBus_title_language_setting,
                            QIBus_lbl_language,
                            language,
                            _selectedLocation)
                      ],
                    ),
                  ),
                ),
              )
            ],
          )),
    );
  }

  TopBarAdd(st) {
    return SafeArea(
      child: Stack(
        children: <Widget>[
          Container(color: fbiru2, height: 70),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.width * 0.15,
                // alignment: Alignment.topLeft,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    st
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              IconButton(
                                icon: Icon(
                                  Icons.arrow_back,
                                  color: qIBus_white,
                                ),
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                              ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(
                                    spacing_standard, 0, 0, 0),
                                child: text(tr('tab_service.configure_time_leave.Add_Leave.title'),
                                    textColor: qIBus_white,
                                    fontSize: textSizeNormal,
                                    fontFamily: fontBold),
                              ),
                            ],
                          )
                        : Padding(
                            padding: EdgeInsets.fromLTRB(
                                spacing_standard_new, spacing_standard, 0, 0),
                            child: text(tr('tab_service.configure_time_leave.Add_Leave.title'),
                                textColor: qIBus_white,
                                fontSize: textSizeNormal,
                                fontFamily: fontBold),
                          ),
                    // widget.isVisible
                    // ? GestureDetector(
                    //     onTap: () {
                    //       // launchScreen(context, QIBusNotification.tag);
                    //     },
                    //     child: Container(
                    //       margin: EdgeInsets.only(
                    //         right: spacing_standard_new,
                    //       ),
                    //       child: Image(
                    //         image: AssetImage(widget.icon),
                    //         height: 25,
                    //         width: 25,
                    //         color: qIBus_white,
                    //       ),
                    //     ))
                    // : GestureDetector(
                    //     onTap: () {
                    //       // launchScreen(context, QIBusNotification.tag);
                    //     },
                    //     child: Container(
                    //       margin: EdgeInsets.only(
                    //         top: spacing_standard,
                    //         right: spacing_standard_new,
                    //       ),
                    //       child: Image(
                    //         image: AssetImage(widget.icon),
                    //         height: 25,
                    //         width: 25,
                    //         color: qIBus_white,
                    //       ),
                    //     ))
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        IconButton(
                          icon: Icon(
                            Icons.send,
                            color: qIBus_white,
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                      ],
                    )
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.width * 0.05),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20)),
                    color: qIBus_app_background),
              ),
            ],
          )
        ],
      ),
    );
  }
}
