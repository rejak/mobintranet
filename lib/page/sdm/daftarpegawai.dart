import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mobintranet/Loader.dart';
import 'package:mobintranet/api/manageapi.dart';
import 'package:mobintranet/bloc/bloc_daftar_pegawai/bloc_daftar_pegawai_bloc.dart';
import 'package:mobintranet/bloc/main_bloc.dart';
import 'package:mobintranet/model/pegawai.dart';
import 'package:mobintranet/page/errorPage/error_page.dart';
import 'package:mobintranet/util/DbColors.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:easy_localization/easy_localization.dart';

class DaftarPegawai extends StatefulWidget {
  @override
  _DaftarPegawaiState createState() => _DaftarPegawaiState();
}

class _DaftarPegawaiState extends State<DaftarPegawai> {
  List<Pegawai> list;
  List<Pegawai> allpeg;
  List<Pegawai> listDataMaster;
  bool cari ;
  final cariController= TextEditingController();
  String _textcari ="";
  List<Pegawai> daftarcari=new List();
  final bloc = BlocDaftarPegawaiBloc();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    cari=false;
    bloc..add(GetDataPegawaiEvent());
    // API.getallpegawai(mainBloc.tokenapi).then((res){
    //   setState(() {
    //     list=res;
    //     allpeg=res;
    //   });
    // });
  }

  @override
  Widget build(BuildContext context) {
    return 
    Scaffold(
      appBar: AppBar(
        backgroundColor: fbiru1,
        title: cari?TextField(
          controller: cariController,
          autofocus: true,
          style: TextStyle(color: Colors.white ),
          decoration: InputDecoration(
            border: InputBorder.none,
            hintText: tr('tab_service.page_search_employee.search'),
            hintStyle: TextStyle(color: Colors.white)
          ),
          onChanged: _filterdata,
        ):Text("SDM"),
        actions: <Widget>[
          Row(
            children: <Widget>[
              cari?
              InkWell(
                onTap: (){
                  setState(() {
                    cari=false;
                  });
                },
                child: Icon(Icons.cancel)):
              InkWell(
                onTap: (){
                  setState(() {
                    cari=true;
                    cariController.clear();
                  });
                },
                child: Icon(Icons.search),
              ),
              
            ],
          ),
        ],
      ),
      body:BlocBuilder<BlocDaftarPegawaiBloc, BlocDaftarPegawaiState>(
        cubit: bloc,
        builder: (context, state) {
          if(state is GetDaftarPegawaiSukses){
            if(state.statusCari == true){
              list=state.listDataCari;
              allpeg=state.listDataCari;
            } else {
              list=state.listData;
              allpeg=state.listData;
              listDataMaster = state.listData;
            }
            return daftarcari.length!=0 || cariController.text.isNotEmpty?
            Container(
              color: Colors.white,
              child: list!=null?
                  Listpeg(list):Center(child: Loader(),)
            ):
            Container(
              color: Colors.white,
              child: list!=null?
                  Listpeg(allpeg):Center(child: Loader(),)
            );
          }
          if(state is GetDaftarPegawaiGagal){
            return ErrorPage();
          }
          if(state is GetDaftarPegawaiWaiting){
            return Container(child: Loader());
          }
          return Container(child: Loader());
        },
      )
      
    );
  }

  void _filterdata(String text){
    daftarcari.clear();
    if(cari!=null){
      bloc..add(SearchPegawaiEvent(text: text,cari: true, dataList: listDataMaster));
    }
    
  }

    Widget Listpeg(List<Pegawai> pegawai){
      return Container(
        child: ListView.builder(
          itemCount: pegawai.length,
          itemBuilder: (context,posisi){
            return Container(
              
              child: Column(
                children: <Widget>[
                  Card(
                    child: Container(
                      padding: EdgeInsets.all(5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            flex: 1,
                            child: Icon(Icons.account_circle,size: 30,color: fabu4,),
                          ),
                          SizedBox(width: 2,),
                          Expanded(
                            flex: 5,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(pegawai[posisi].nama,style: TextStyle(fontSize: 13,fontWeight: FontWeight.bold),),
                                Text(pegawai[posisi].jabatan,style: TextStyle(fontSize: 12),),
                                Text(pegawai[posisi].perEselonDua,style: TextStyle(fontSize: 12),),
                              ],
                            ),
                          ),
                          Expanded(
                            flex: 2,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                 Text(pegawai[posisi].nip,style: TextStyle(fontSize: 9),),
                                 Text(pegawai[posisi].golonganRuang,style: TextStyle(fontSize: 12),)
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      );
      
    }

}