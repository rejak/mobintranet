// import 'dart:html';

// import 'dart:html';

import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:mobintranet/page/sdm/OperasiCuti/addCuti.dart';
import 'package:mobintranet/util/DbColors.dart';
import 'package:mobintranet/util/DbDataGenerator.dart';
import 'package:mobintranet/util/models/KeloladCutiModels.dart';
import 'package:mobintranet/util/colors.dart';
import 'package:mobintranet/util/constant.dart';
import 'package:mobintranet/util/DbStrings.dart';
import 'package:mobintranet/widget/widgetKelolaCuti.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:easy_localization/easy_localization.dart';

import 'OperasiCuti/editCuti.dart';

// void main() => runApp(EasyLocalization(child: KelolaCuti()));
class KelolaCuti extends StatefulWidget {
  static var tag = "/T1Listing";

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return KelolaCutiState();
  }
}

class KelolaCutiState extends State<KelolaCuti> {
  List<T1Model> mListings;

  @override
  void initState() {
    super.initState();
    mListings = getListings();
    print(mListings[0]);
    // print(tr);
  }

  @override
  Widget build(BuildContext context) {
    print("sono");
    print(context);
    Size size = MediaQuery.of(context).size;
    print(size.width);
    // return MaterialApp(
      return Scaffold(
        body: Container(
          child: Column(
            children: <Widget>[
              TopBarAdd(true),
              Container(
                child: Column(
                  children: <Widget>[
                    Container(
                      height: size.height / 12,
                      width: size.width,
                      padding: EdgeInsets.only(
                          left: 16, right: 16, top: 16, bottom: 20),
                      margin: EdgeInsets.fromLTRB(16, 0, 16, 16),
                      decoration: boxDecoration2(radius: 16),
                      child: textField2(
                        title: tr('tab_service.configure_time_leave.search'),
                        image: Icons.search,
                        // textInputType: TextInputType.number,
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: ListView.builder(
                    scrollDirection: Axis.vertical,
                    itemCount: mListings.length,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return ListCard(mListings[index], index);
                    }),
              )
            ],
          ),
        ),
        floatingActionButton: RaisedButton(
          color: Colors.lightBlueAccent[100],
          onPressed: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => AddCuti()));
          },
          child: Container(
            width: 80,
            height: 20,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(
                    child: Text(
                  tr('tab_service.configure_time_leave.Add_Leave.title'),
                  style: TextStyle(color: Colors.black),
                )),
              ],
            ),
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(80.0),
          ),
        ),
      );
    // );
  }

  Widget textField2({
    String title,
    IconData image,
    TextInputType textInputType,
  }) {
    return TextField(
      keyboardType: textInputType,
      autocorrect: true,
      style: TextStyle(color: Colors.white, fontSize: 18),
      decoration: InputDecoration(
          hintText: title,
          enabledBorder: UnderlineInputBorder(
              borderRadius: BorderRadius.circular(10.0),
              borderSide: BorderSide(color: Colors.white)),
          focusedBorder:
              UnderlineInputBorder(borderSide: BorderSide(color: Colors.white)),
          hintStyle: TextStyle(color: Colors.white54, fontSize: 14),
          fillColor: Colors.white54,
          prefixIcon: Icon(image, color: Colors.white54),
          suffixIcon: InkWell(
              onTap: () {
                return _handleClickMe();
              },
              child: Icon(
                Icons.filter_list,
                color: Colors.white54,
              ))),
    );
  }

  Future<void> _handleClickMe() async {
    return showDialog<void>(
      context: context,
      // barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16),
          ),
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          child: dialogContent(context),
        );
      },
    );
  }

  dialogContent(BuildContext context) {
    return Container(
        decoration: new BoxDecoration(
          color: Colors.white,
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(16),
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              blurRadius: 10.0,
              offset: const Offset(0.0, 10.0),
            ),
          ],
        ),
        width: MediaQuery.of(context).size.width,
        child: Padding(
            padding: EdgeInsets.all(20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      tr('tab_service.configure_time_leave.filter_by.title'),
                      style: TextStyle(fontSize: 20),
                    )
                  ],
                ),
                SizedBox(height: 20),
                RadioListTile(
                  value: 0,
                  groupValue: 0,
                  activeColor: Colors.blueGrey,
                  title: Text(tr('tab_service.configure_time_leave.filter_by.choose.first_choose')),
                  onChanged: (val) {
                    print("1");
                  },
                ),
                RadioListTile(
                  value: 1,
                  groupValue: 0,
                  activeColor: Colors.blueGrey,
                  title: Text(tr('tab_service.configure_time_leave.filter_by.choose.seccond_choose')),
                  onChanged: (val) {
                    print("2");
                  },
                ),
                SizedBox(height:10),
                Padding(
                  padding: EdgeInsets.fromLTRB(20, 20, 20, 5),
                                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      InkWell(
                        onTap: (){
                          Navigator.of(context).pop(true);
                        },
                        child: Text(
                          tr('tab_service.configure_time_leave.filter_by.button.ok')
                        ),
                      ),
                      InkWell(
                        onTap: (){
                          Navigator.of(context).pop(true);
                        },
                        child: Text(
                          tr('tab_service.configure_time_leave.filter_by.button.clear')
                        ),
                      ),
                      InkWell(
                        onTap: (){
                          Navigator.of(context).pop(true);
                        },
                        child: Text(
                          tr('tab_service.configure_time_leave.filter_by.button.close')
                        ),
                      )
                    ],
                  ),
                )
              ],
            )));
  }

  ListCard(T1Model model, int pos) {
    return Padding(
        padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
        child: Card(
          shadowColor: Colors.black,
          elevation: 8,
          clipBehavior: Clip.antiAlias,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
          child: Container(
            decoration: boxDecoration(radius: 10, showShadow: true),
            child: Stack(
              children: <Widget>[
                new Positioned(
                  top: -65.0,
                  right: -20.0,
                  height: 440,
                  child: new Image(
                    image: new AssetImage('assets/sdm/bubble-3.png'),
                  ),
                ),
                new Positioned(
                  bottom: -20.0,
                  height: 140,
                  right: -40.0,
                  child: new Image(
                      image: new AssetImage('assets/sdm/kalender1.png')),
                ),
                Container(
                  padding: EdgeInsets.all(16),
                  child: Column(
                    children: <Widget>[
                      // Row(
                      //   children: <Widget>[
                      //     ClipRRect(
                      //       child: CachedNetworkImage(
                      //         imageUrl: model.img,
                      //         width: width / 5.5,
                      //         height: width / 6,
                      //       ),
                      //       borderRadius: BorderRadius.circular(12),
                      //     ),
                      //     Expanded(
                      //       child: Container(
                      //         padding: EdgeInsets.only(left: 16),
                      //         child: Column(
                      //           crossAxisAlignment: CrossAxisAlignment.start,
                      //           children: <Widget>[
                      //             Row(
                      //               mainAxisAlignment:
                      //                   MainAxisAlignment.spaceBetween,
                      //               children: <Widget>[
                      //                 text(model.name,
                      //                     textColor: t1TextColorPrimary,
                      //                     fontFamily: fontBold,
                      //                     fontSize: textSizeNormal,
                      //                     maxLine: 2),
                      //                 text(model.duration,
                      //                     fontSize: textSizeMedium),
                      //               ],
                      //             ),
                      //             text(model.designation,
                      //                 fontSize: textSizeLargeMedium,
                      //                 textColor: t1TextColorPrimary,
                      //                 fontFamily: fontMedium),
                      //           ],
                      //         ),
                      //       ),
                      //     )
                      //   ],
                      //   mainAxisAlignment: MainAxisAlignment.start,
                      // ),
                      // SizedBox(
                      //   height: 16,
                      // ),

                      // menampilkan tanggal cuti
                      Row(
                        children: [
                          SizedBox(
                            width: 100,
                            child: text(
                                tr(
                                    'tab_service.configure_time_leave.list_value.leave_date'),
                                fontSize: textSizeMedium,
                                maxLine: 2,
                                textColor: t1TextColorPrimary),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          text(":",
                              fontSize: textSizeMedium,
                              maxLine: 2,
                              textColor: t1TextColorPrimary),
                          SizedBox(
                            width: 10,
                          ),
                          Container(
                            child: Container(
                              child: Expanded(
                                child: text("6-12-2020" + " s/d " + "8-12-2020",
                                    fontSize: textSizeMedium,
                                    maxLine: 2,
                                    textColor: t1TextColorPrimary),
                              ),
                            ),
                          ),
                        ],
                      ),
                      // menampilkan alasan cuti
                      Row(
                        children: [
                          SizedBox(
                            width: 100,
                            child: text(tr(
                                    'tab_service.configure_time_leave.list_value.reason_of_leave'),
                                fontSize: textSizeMedium,
                                maxLine: 2,
                                textColor: t1TextColorPrimary),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          text(":",
                              fontSize: textSizeMedium,
                              maxLine: 2,
                              textColor: t1TextColorPrimary),
                          SizedBox(
                            width: 10,
                          ),
                          Container(
                            child: Expanded(
                              child: text("Pulang ke kampung halaman",
                                  fontSize: textSizeMedium,
                                  maxLine: 2,
                                  textColor: t1TextColorPrimary),
                            ),
                          )
                        ],
                      ),
                      // menampilkan status cuti
                      Row(
                        children: [
                          SizedBox(
                            width: 100,
                            child: text(tr(
                                    'tab_service.configure_time_leave.list_value.leave_status'),
                                fontSize: textSizeMedium,
                                maxLine: 2,
                                textColor: t1TextColorPrimary),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          text(":",
                              fontSize: textSizeMedium,
                              maxLine: 2,
                              textColor: t1TextColorPrimary),
                          SizedBox(
                            width: 10,
                          ),
                          text("Diizinkan",
                              fontSize: textSizeMedium,
                              maxLine: 2,
                              textColor: t1TextColorPrimary),
                        ],
                      ),
                      //button
                      Padding(
                        padding: EdgeInsets.fromLTRB(
                            spacing_standard_new, spacing_middle, 0, 0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            InkWell(
                              onTap: () {
                                return Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => EditCuti()));
                              },
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.edit,
                                    color: Colors.orange,
                                  ),
                                  Text(tr(
                                    'tab_service.configure_time_leave.list_value.button.edit'))
                                ],
                              ),
                            ),
                            SizedBox(width: 50),
                            InkWell(
                              onTap: () {},
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.delete_forever,
                                    color: Colors.red,
                                  ),
                                  Text(tr(
                                    'tab_service.configure_time_leave.list_value.button.delete'))
                                ],
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  width: 4,
                  height: 35,
                  margin: EdgeInsets.only(top: 16),
                  color: pos % 2 == 0 ? t1TextColorPrimary : t1_colorPrimary,
                )
              ],
            ),
          ),
        ));
  }

  TopBarAdd(st) {
    return SafeArea(
        child: Stack(
          children: <Widget>[
            Container(color: fbiru2, height: 70),
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.width * 0.15,
                  // alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      st
                          ? Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                IconButton(
                                  icon: Icon(
                                    Icons.arrow_back,
                                    color: qIBus_white,
                                  ),
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(
                                      spacing_standard, 0, 0, 0),
                                  child: text(
                                      tr(
                                          'tab_service.configure_time_leave.title'),
                                      textColor: qIBus_white,
                                      fontSize: textSizeNormal,
                                      fontFamily: fontBold),
                                ),
                              ],
                            )
                          : Padding(
                              padding: EdgeInsets.fromLTRB(
                                  spacing_standard_new, spacing_standard, 0, 0),
                              child: text("Kelola Cuti",
                                  textColor: qIBus_white,
                                  fontSize: textSizeNormal,
                                  fontFamily: fontBold),
                            ),
                      // widget.isVisible
                      // ? GestureDetector(
                      //     onTap: () {
                      //       // launchScreen(context, QIBusNotification.tag);
                      //     },
                      //     child: Container(
                      //       margin: EdgeInsets.only(
                      //         right: spacing_standard_new,
                      //       ),
                      //       child: Image(
                      //         image: AssetImage(widget.icon),
                      //         height: 25,
                      //         width: 25,
                      //         color: qIBus_white,
                      //       ),
                      //     ))
                      // : GestureDetector(
                      //     onTap: () {
                      //       // launchScreen(context, QIBusNotification.tag);
                      //     },
                      //     child: Container(
                      //       margin: EdgeInsets.only(
                      //         top: spacing_standard,
                      //         right: spacing_standard_new,
                      //       ),
                      //       child: Image(
                      //         image: AssetImage(widget.icon),
                      //         height: 25,
                      //         width: 25,
                      //         color: qIBus_white,
                      //       ),
                      //     ))
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(
                      top: MediaQuery.of(context).size.width * 0.05),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20),
                          topRight: Radius.circular(20)),
                      color: qIBus_app_background),
                ),
              ],
            )
          ],
        ),
      );
  }
}

// class T1ListItem extends StatelessWidget {
//   T1Model model;
//   int pos;
//   BuildContext con;
//   T1ListItem(T1Model model, int pos, BuildContext con) {
//     this.model = model;
//     this.pos = pos;
//     this.con = con;
//   }

//   @override
//   Widget build(BuildContext context) {
//     var data = EasyLocalizationProvider.of(con).data;
//     print(data);
//     print("sini");
//     print(con);
//     var width = MediaQuery.of(con).size.width;
//     return EasyLocalizationProvider(
//       data: data,
//       child: Scaffold(
//         body: Padding(
//             padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
//             child: Card(
//               shadowColor: Colors.black,
//               elevation: 8,
//               clipBehavior: Clip.antiAlias,
//               shape: RoundedRectangleBorder(
//                   borderRadius: BorderRadius.circular(24)),
//               child: Container(
//                 decoration: boxDecoration(radius: 10, showShadow: true),
//                 child: Stack(
//                   children: <Widget>[
//                     new Positioned(
//                       top: -65.0,
//                       right: -20.0,
//                       height: 440,
//                       child: new Image(
//                         image: new AssetImage('assets/sdm/bubble-3.png'),
//                       ),
//                     ),
//                     new Positioned(
//                       bottom: -20.0,
//                       height: 140,
//                       right: -40.0,
//                       child: new Image(
//                           image: new AssetImage('assets/sdm/kalender1.png')),
//                     ),
//                     Container(
//                       padding: EdgeInsets.all(16),
//                       child: Column(
//                         children: <Widget>[
//                           // Row(
//                           //   children: <Widget>[
//                           //     ClipRRect(
//                           //       child: CachedNetworkImage(
//                           //         imageUrl: model.img,
//                           //         width: width / 5.5,
//                           //         height: width / 6,
//                           //       ),
//                           //       borderRadius: BorderRadius.circular(12),
//                           //     ),
//                           //     Expanded(
//                           //       child: Container(
//                           //         padding: EdgeInsets.only(left: 16),
//                           //         child: Column(
//                           //           crossAxisAlignment: CrossAxisAlignment.start,
//                           //           children: <Widget>[
//                           //             Row(
//                           //               mainAxisAlignment:
//                           //                   MainAxisAlignment.spaceBetween,
//                           //               children: <Widget>[
//                           //                 text(model.name,
//                           //                     textColor: t1TextColorPrimary,
//                           //                     fontFamily: fontBold,
//                           //                     fontSize: textSizeNormal,
//                           //                     maxLine: 2),
//                           //                 text(model.duration,
//                           //                     fontSize: textSizeMedium),
//                           //               ],
//                           //             ),
//                           //             text(model.designation,
//                           //                 fontSize: textSizeLargeMedium,
//                           //                 textColor: t1TextColorPrimary,
//                           //                 fontFamily: fontMedium),
//                           //           ],
//                           //         ),
//                           //       ),
//                           //     )
//                           //   ],
//                           //   mainAxisAlignment: MainAxisAlignment.start,
//                           // ),
//                           // SizedBox(
//                           //   height: 16,
//                           // ),

//                           // menampilkan tanggal cuti
//                           Row(
//                             children: [
//                               SizedBox(
//                                 width: 100,
//                                 child: text(
//                                     AppLocalizations.of(con).tr(
//                                         'tab_service.configure_time_leave.list_value.leave_date'),
//                                     fontSize: textSizeMedium,
//                                     maxLine: 2,
//                                     textColor: t1TextColorPrimary),
//                               ),
//                               SizedBox(
//                                 width: 10,
//                               ),
//                               text(":",
//                                   fontSize: textSizeMedium,
//                                   maxLine: 2,
//                                   textColor: t1TextColorPrimary),
//                               SizedBox(
//                                 width: 10,
//                               ),
//                               Container(
//                                 child: Container(
//                                   child: Expanded(
//                                     child: text(
//                                         "6-12-2020" + " s/d " + "8-12-2020",
//                                         fontSize: textSizeMedium,
//                                         maxLine: 2,
//                                         textColor: t1TextColorPrimary),
//                                   ),
//                                 ),
//                               ),
//                             ],
//                           ),
//                           // menampilkan alasan cuti
//                           Row(
//                             children: [
//                               SizedBox(
//                                 width: 100,
//                                 child: text("Alasan Cuti",
//                                     fontSize: textSizeMedium,
//                                     maxLine: 2,
//                                     textColor: t1TextColorPrimary),
//                               ),
//                               SizedBox(
//                                 width: 10,
//                               ),
//                               text(":",
//                                   fontSize: textSizeMedium,
//                                   maxLine: 2,
//                                   textColor: t1TextColorPrimary),
//                               SizedBox(
//                                 width: 10,
//                               ),
//                               Container(
//                                 child: Expanded(
//                                   child: text("Pulang ke kampung halaman",
//                                       fontSize: textSizeMedium,
//                                       maxLine: 2,
//                                       textColor: t1TextColorPrimary),
//                                 ),
//                               )
//                             ],
//                           ),
//                           // menampilkan status cuti
//                           Row(
//                             children: [
//                               SizedBox(
//                                 width: 100,
//                                 child: text("Status Cuti",
//                                     fontSize: textSizeMedium,
//                                     maxLine: 2,
//                                     textColor: t1TextColorPrimary),
//                               ),
//                               SizedBox(
//                                 width: 10,
//                               ),
//                               text(":",
//                                   fontSize: textSizeMedium,
//                                   maxLine: 2,
//                                   textColor: t1TextColorPrimary),
//                               SizedBox(
//                                 width: 10,
//                               ),
//                               text("Diizinkan",
//                                   fontSize: textSizeMedium,
//                                   maxLine: 2,
//                                   textColor: t1TextColorPrimary),
//                             ],
//                           ),
//                           //button
//                           Padding(
//                             padding: EdgeInsets.fromLTRB(
//                                 spacing_standard_new, spacing_middle, 0, 0),
//                             child: Row(
//                               mainAxisAlignment: MainAxisAlignment.start,
//                               children: [
//                                 InkWell(
//                                   onTap: () {
//                                     return Navigator.push(
//                                         con,
//                                         MaterialPageRoute(
//                                             builder: (context) => EditCuti()));
//                                   },
//                                   child: Row(
//                                     children: [
//                                       Icon(
//                                         Icons.edit,
//                                         color: Colors.orange,
//                                       ),
//                                       Text("Edit")
//                                     ],
//                                   ),
//                                 ),
//                                 SizedBox(width: 50),
//                                 InkWell(
//                                   onTap: () {},
//                                   child: Row(
//                                     children: [
//                                       Icon(
//                                         Icons.delete_forever,
//                                         color: Colors.red,
//                                       ),
//                                       Text("Hapus")
//                                     ],
//                                   ),
//                                 )
//                               ],
//                             ),
//                           )
//                         ],
//                       ),
//                     ),
//                     Container(
//                       width: 4,
//                       height: 35,
//                       margin: EdgeInsets.only(top: 16),
//                       color:
//                           pos % 2 == 0 ? t1TextColorPrimary : t1_colorPrimary,
//                     )
//                   ],
//                 ),
//               ),
//             )),
//       ),
//     );
//   }
// }
