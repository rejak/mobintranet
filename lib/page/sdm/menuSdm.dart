import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mobintranet/page/sdm/kelolaCuti.dart';
import 'package:mobintranet/page/sdm/sdm.dart';
import 'package:mobintranet/util/colors.dart';
import 'package:mobintranet/util/DbColors.dart';
import 'package:mobintranet/util/images.dart';
import 'package:mobintranet/util/DbStrings.dart';
import 'package:mobintranet/widget/sdmMenu.dart';

class MenuSdm extends StatefulWidget {
  static var tag = "/T1BottomNavigation";

  @override
  MenuSdmState createState() => MenuSdmState();
}

class MenuSdmState extends State<MenuSdm> {
  var isSelected = 1;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  Widget tabItem(var pos, var icon) {
    return GestureDetector(
      onTap: () {
        setState(() {
          isSelected = pos;
        });
      },
      child: Container(
        width: 45,
        height: 45,
        alignment: Alignment.center,
        decoration: isSelected == pos ? BoxDecoration(shape: BoxShape.circle, color: Colors.lightBlue[100]) : BoxDecoration(),
        child: SvgPicture.asset(
          icon,
          width: 20,
          height: 20,
          color: isSelected == pos ? Colors.black : t1_textColorSecondary,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: bodyItem(context),
      bottomNavigationBar: Stack(
        alignment: Alignment.topCenter,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top: 5),
            height: 60,
            decoration: BoxDecoration(
              color: t1_white,
              boxShadow: [BoxShadow(color: shadow_color, blurRadius: 10, spreadRadius: 2, offset: Offset(0, 3.0))],
            ),
            child: Padding(
              padding: const EdgeInsets.only(left: 16.0, right: 16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  tabItem(1, t1_home),
                  tabItem(2, t1_user),
                  // tabItem(2, t1_notification),
                  // // Container(width: 45, height: 45),
                  // tabItem(3, t1_settings),
                  // tabItem(4, t1_user),
                ],
              ),
            ),
          ),
          // Container(
          //   child: FloatingActionButton(
          //     backgroundColor: t1_colorPrimary,
          //     onPressed: () {
          //       //
          //     },
          //     child: Icon(
          //       Icons.mic,
          //       color: t1_white,
          //     ),
          //   ),
          // )
        ],
      ),
    );
  }

  Widget bodyItem(BuildContext context) {
      if (isSelected == 1) {
        return KelolaCuti();
      } else if (isSelected == 2) {
        return SDM();
      } 
      // else if (isSelected == 3) {
      //   // return Agenda();
      // } else if (isSelected == 4) {
      //   // return T10Profile();
      // }
    }
}
