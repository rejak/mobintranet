// import 'dart:convert';
// import 'package:mobintranet/Loader.dart';
// import 'package:mobintranet/page/p_gantipassword.dart';
// import 'package:mobintranet/util/DbColors.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:flutter/material.dart';
// import 'package:mobintranet/api/manageapi.dart';
// import 'package:mobintranet/bloc/main_bloc.dart';

// class Profil extends StatefulWidget {
//   final String token;
//   final String kode;

//   const Profil({Key key, this.token, this.kode}) : super(key: key);
//   @override
//   _ProfilState createState() => _ProfilState();
// }

// class _ProfilState extends State<Profil> {
// bool _loading = true;
// String nama,nip,jabatan,unit,gol,bidang,tmtgol,tmtjab,tmtunit;
// void _ambiluser(){
//   API.getUser(mainBloc.tokenapi).then((res){
//     // print(res.body);
//     var isi= jsonDecode(res.body);
//     print(isi);
//     setState(() {
//       nama=isi['nama'];
//       nip=isi['nip'];
//       jabatan=isi['jabatan'];
//       unit=isi['perEselonDua'];
//       gol=isi['golonganRuang'];
//       bidang=isi['unitKerja'];
//       tmtgol=isi['tmtGolongan'];
//       tmtjab=isi['tmtJabatan'];
//       tmtunit=isi['tmtUnit'];
//       _loading = false;
//     });
    
//     print(isi);
//   });
// }

// void setpref()async {
//       final prefs = await SharedPreferences.getInstance();
//       bool statuslogin = false;
//       prefs.setString('isitoken', '');
//       prefs.setBool('login', statuslogin);
//     }

// @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
//     _ambiluser();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: fbiru1,
//         leading: InkWell(
//           onTap: (){Navigator.pop(context);},
//           child: Icon(Icons.arrow_back),
//         ),
//         title: Text("Akun Saya"),
//         elevation: 0,
//       ),
//       body: _loading? Loader():  SingleChildScrollView(
//         child: Column(
//           children: <Widget>[
//             Stack(
//               children: <Widget>[
//                 Container(
//                     height: 150,
//                     child: Container(),
//                     decoration: BoxDecoration(
//                       color: Colors.blue,
//                       borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10),bottomRight: Radius.circular(10))
//                     ),
//                   ),
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: <Widget>[
//                   Column(
//                     children: <Widget>[
//                       Card(
//                         child: Container(
//                           padding: EdgeInsets.all(15),
//                           width: MediaQuery.of(context).size.width- 10,
//                           child: Column(
//                             mainAxisAlignment: MainAxisAlignment.start,
//                             crossAxisAlignment: CrossAxisAlignment.start,
//                             children: <Widget>[
//                               Row(
//                                 children: <Widget>[
//                                   Expanded(
//                                     flex: 0,
//                                       child: Container(
//                                       width: 80,
//                                       height: 80,
//                                       child: Icon(Icons.people,color: Colors.grey,size: 50,),
//                                       decoration: BoxDecoration(
//                                         color: Colors.grey[200],
//                                         borderRadius: BorderRadius.all(Radius.circular(10)),
//                                       ),
//                                     ),
//                                   ),
//                                   SizedBox(width: 10,),
//                                   Expanded(
//                                     flex: 4,
//                                     child: Column(
//                                       mainAxisAlignment: MainAxisAlignment.start,
//                                       crossAxisAlignment: CrossAxisAlignment.start,
//                                       children: <Widget>[
//                                         Text("${nama}",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14),),
//                                         Text("${jabatan}",style: TextStyle(fontWeight: FontWeight.normal,fontSize: 12),),
//                                         Text("${unit}",style: TextStyle(fontWeight: FontWeight.normal,fontSize: 12),),
                                        
//                                       ],
//                                     ),
//                                   ),
                                  
//                                 ],
//                               ),
//                               SizedBox(height: 15,),
//                               Text("Informasi Profil",textAlign: TextAlign.left,style: TextStyle(fontWeight: FontWeight.bold),),
//                               SizedBox(height: 5,),
//                               Divider(color: Colors.grey[200],thickness: 2.0,),
//                               SizedBox(height: 5,),
//                               Container(
//                                 padding: EdgeInsets.all(15),
//                                 child: Column(
//                                   children: <Widget>[
//                                     Row(
//                                       children: <Widget>[
//                                         Expanded(
//                                           flex: 2,
//                                           child: Text("NIP"),
//                                         ),
//                                         Expanded(
//                                           flex: 4,
//                                           child: Text("${nip}"),
//                                         ),
//                                       ],
//                                     ),
//                                     SizedBox(height: 10,),
//                                     Row(
//                                       children: <Widget>[
//                                         Expanded(
//                                           flex: 2,
//                                           child: Text("Nama Lengkap"),
//                                         ),
//                                         Expanded(
//                                           flex: 4,
//                                           child: Text("${nama}"),
//                                         ),
//                                       ],
//                                     ),
//                                     SizedBox(height: 10,),
//                                     Row(
//                                       children: <Widget>[
//                                         Expanded(
//                                           flex: 2,
//                                           child: Text("Golongan"),
//                                         ),
//                                         Expanded(
//                                           flex: 4,
//                                           child: Text("${gol}"),
//                                         ),
//                                       ],
//                                     ),
//                                     SizedBox(height: 10,),
//                                     Row(
//                                       children: <Widget>[
//                                         Expanded(
//                                           flex: 2,
//                                           child: Text("Jabatan"),
//                                         ),
//                                         Expanded(
//                                           flex: 4,
//                                           child: Text("${jabatan}"),
//                                         ),
//                                       ],
//                                     ),
//                                     SizedBox(height: 10,),
//                                     Row(
//                                       children: <Widget>[
//                                         Expanded(
//                                           flex: 2,
//                                           child: Text("Bidang"),
//                                         ),
//                                         Expanded(
//                                           flex: 4,
//                                           child: Text("${bidang}"),
//                                         ),
//                                       ],
//                                     ),
//                                     SizedBox(height: 10,),
//                                     Row(
//                                       children: <Widget>[
//                                         Expanded(
//                                           flex: 2,
//                                           child: Text("Unit Kerja"),
//                                         ),
//                                         Expanded(
//                                           flex: 4,
//                                           child: Text("${unit}"),
//                                         ),
//                                       ],
//                                     ),
//                                     SizedBox(height: 20,),
//                                     Row(
//                                       children: <Widget>[
//                                         Expanded(
//                                           flex: 2,
//                                           child: Text("TMT Golongan"),
//                                         ),
//                                         Expanded(
//                                           flex: 4,
//                                           child: Text("${tmtgol}"),
//                                         ),
//                                       ],
//                                     ),
//                                     SizedBox(height: 10,),
//                                     Row(
//                                       children: <Widget>[
//                                         Expanded(
//                                           flex: 2,
//                                           child: Text("TMT Jabatan"),
//                                         ),
//                                         Expanded(
//                                           flex: 4,
//                                           child: Text("${tmtjab}"),
//                                         ),
//                                       ],
//                                     ),
//                                     SizedBox(height: 10,),
//                                     Row(
//                                       children: <Widget>[
//                                         Expanded(
//                                           flex: 2,
//                                           child: Text("TMT Unit"),
//                                         ),
//                                         Expanded(
//                                           flex: 4,
//                                           child: Text("${tmtunit}"),
//                                         ),
//                                       ],
//                                     ),
//                                     SizedBox(height: 20,),
                                    
//                                   ],
//                                 ),
//                               ),

//                             ],
//                           ),
//                         ),
//                       ),

//                       Row(
//                     children: <Widget>[
//                       RaisedButton(
//                         onPressed: (){
//                           Navigator.push(context, MaterialPageRoute(builder: (context)=> ChangePass()));
//                         },
//                         color: fbiru1,
//                         child: Row(
//                           children: <Widget>[
//                             Icon(Icons.lock,color: Colors.white,),
//                             SizedBox(width: 5,),
//                             Text("Ganti Password",style: TextStyle(color: Colors.white),),
//                           ],
//                         ), 
//                       ),
//                       SizedBox(width: 20,),
//                       RaisedButton(
//                         onPressed: (){
//                           setpref();
//                           Navigator.of(context).pushNamedAndRemoveUntil('/login', (Route<dynamic> route)=> false);
//                         },
//                         color: fmerah1,
//                         child: Row(
//                           children: <Widget>[
//                             Icon(Icons.vpn_key,color: Colors.white,),
//                             SizedBox(width: 5,),
//                             Text("Logout",style: TextStyle(color: Colors.white),),
//                           ],
//                         ), 
//                       ),
//                     ],
//                   ),

//                     ],
//                   ),
//                   ], 
//                 ),
                
//               ],
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }