import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mobintranet/bloc/bloc_surat/bloc_detailSurat_bloc/bloc_detailsurat_bloc.dart';
import 'package:mobintranet/model/Surat.dart';
import 'package:mobintranet/page/errorPage/error_page.dart';
import 'package:mobintranet/widget/datetime.dart';
import 'package:mobintranet/widget/master.dart';
import 'package:random_color/random_color.dart';

class RiwayatSurat extends StatefulWidget {
  final int idSurat;
  const RiwayatSurat({@required this.idSurat});
  @override
  _RiwayatSuratState createState() => _RiwayatSuratState();
}

class _RiwayatSuratState extends State<RiwayatSurat> {
  FontWeight font = FontWeight.bold;
  bool baca = false;
  // List<Surat> list;
  bool isi = false;
  final bloc = BlocDetailsuratBloc();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    bloc..add(HistorySuratBloc(idSurat: widget.idSurat));
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Container(
      height: height/3,
      child: BlocBuilder<BlocDetailsuratBloc, BlocDetailsuratState>(
        cubit: bloc,
        builder: (context, state) {
          if (state is HistorySuccess) {
            print("berhasil get history");
            return _listHistory(state);
          }
          if (state is HistoryWaiting) {
            return Container(
              child: Center(child: CircularProgressIndicator()),
            );
          }
          if (state is HistoryFailed) {
            return Container(
              child: Text("Loading History Gagal"),
            );
          }
          return Container();
        },
      ),
    );
  }

  Widget _listHistory(BlocDetailsuratState state) {
    RandomColor _randomColor = RandomColor();
    List<Surat> isiData;
    if (state is HistorySuccess) {
      isiData = state.dataHistory;
    }
    return Container(
        child: ListView.builder(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          physics: AlwaysScrollableScrollPhysics(),
          itemCount: isi ? 0 : isiData.length,
          itemBuilder: (context, index) {
            return Container(
                padding: EdgeInsets.symmetric(vertical: 1.0, horizontal: 1.0),
                // height: 60,
                // width: double.maxFinite,
                child: GestureDetector(
                  onTap: () {
                    // setState(() {
                    //   indexListSurat = surat.length - index -1;
                    // });
                    print("cek index surat");
                    // bloc
                    //   ..add(
                    //       GetDetailSuratEvent(idSurat: isiData[index].idSurat));
                  },
                  child: Card(
                    color: (isiData[index].status == "tutup")
                        ? Colors.white
                        : Colors.grey.shade200,
                    elevation: 0,
                    child: Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 13, horizontal: 1),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            flex: 1,
                            child: Container(
                              height: 50.0,
                              width: 50.0,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: _randomColor.randomColor(
                                    colorBrightness: ColorBrightness.dark),
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  isiData[index].pengirim == null
                                      ? Container()
                                      : Text(
                                          Master().hurufdepan(
                                              isiData[index].namaTujuan),
                                          style: TextStyle(
                                              fontSize: 30,
                                              color: Colors.white),
                                        )
                                ],
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 4,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      isiData[index].tujuan,
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                          fontWeight:
                                              (isiData[index].status == "tutup")
                                                  ? font
                                                  : FontWeight.w400,
                                          fontSize: 14),
                                    ),
                                    // isiData[index].perihal == null
                                    //     ? Container()
                                    //     : Text(
                                    //         isiData[index].perihal.length < 40
                                    //             ? isiData[index].perihal
                                    //             : isiData[index]
                                    //                     .perihal
                                    //                     .substring(0, 40) +
                                    //                 '...',
                                    //         textAlign: TextAlign.left,
                                    //         style: TextStyle(
                                    //             fontWeight:
                                    //                 (isiData[index].status ==
                                    //                         "tutup")
                                    //                     ? font
                                    //                     : FontWeight.w400,
                                    //             fontSize: 12),
                                    //       ),
                                    // Text(
                                    //   isiData[index].catatan == null
                                    //       ? "Catatan Kosong"
                                    //       : isiData[index].catatan.length < 40
                                    //           ? isiData[index].catatan
                                    //           : isiData[index]
                                    //                   .catatan
                                    //                   .substring(0, 40) +
                                    //               '...',
                                    //   textAlign: TextAlign.left,
                                    //   style: TextStyle(fontSize: 12),
                                    // )
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            flex: 0,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                isiData[index].tanggal == null
                                    ? Container()
                                    : Text(
                                        Tanggal().tgl(isiData[index].tanggal),
                                        style: TextStyle(fontSize: 10),
                                      ),
                                (isiData[index].status == "buka")
                                    ? Icon(
                                        Icons.mark_email_read_sharp,
                                        color: Colors.green,
                                      )
                                    : Icon(
                                        Icons.mark_email_unread_sharp,
                                        color: Colors.lightBlue,
                                      )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ));
          },
        ),
      // ),
    );
  }
}
