import 'package:flutter/material.dart';
import 'package:mobintranet/model/Surat.dart';
import 'package:mobintranet/widget/datetime.dart';
import 'package:mobintranet/widget/master.dart';
import 'package:random_color/random_color.dart';

class DetailSurat extends StatefulWidget {
  final List<Surat> list;
  final int index;
  final bool out;
  const DetailSurat({Key key, this.list, this.index, this.out})
      : super(key: key);
  @override
  _DetailSuratState createState() => _DetailSuratState();
}

class _DetailSuratState extends State<DetailSurat> {
  RandomColor _randomColor = RandomColor();
  IconData more = Icons.expand_more;
  bool status = false;
  

  @override
  Widget build(BuildContext context) {
    List<Surat> surat = widget.list;
    int posisi = widget.index;

    //isi
    String hurufdepan =
        widget.out ? surat[posisi].tujuan : surat[posisi].pengirim;
    String namapengirim =
        widget.out ? surat[posisi].namaTujuan : surat[posisi].namaPengirim;
    String kepada =
        widget.out ? surat[posisi].namaPengirim : surat[posisi].namaTujuan;
    String dari = widget.out ? surat[posisi].tujuan : surat[posisi].pengirim;
    String ke = widget.out ? surat[posisi].pengirim : surat[posisi].tujuan;

    print(posisi);
    return Scaffold(
      appBar: AppBar(
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(Icons.arrow_back),
            ],
          ),
        ),
        title: Text("Detail page"),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
        child: Column(
          children: <Widget>[
            //header
            Row(
              children: <Widget>[
                Expanded(
                  flex: 3,
                  child: surat[posisi].perihal == null
                      ? Container()
                      : Text(
                          surat[posisi].perihal,
                          style: TextStyle(fontSize: 20),
                        ),
                ),
                Expanded(
                  flex: 0,
                  child: Icon(Icons.star_border),
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),

            //Pengirim
            Container(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 0,
                    child: Container(
                      height: 40.0,
                      width: 40.0,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: _randomColor.randomColor(
                            colorBrightness: ColorBrightness.light),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            Master().hurufdepan(hurufdepan),
                            style: TextStyle(fontSize: 30, color: Colors.white),
                          )
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    flex: 3,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          namapengirim,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 15),
                        ),
                        status
                            ? InkWell(
                                onTap: () {
                                  setState(() {
                                    more = Icons.expand_more;
                                    status = false;
                                  });
                                },
                                child: Row(
                                  children: <Widget>[
                                    Text(
                                      "Kepada ${kepada}",
                                      style: TextStyle(fontSize: 12),
                                    ),
                                    Icon(
                                      more,
                                      size: 15,
                                    ),
                                  ],
                                ),
                              )
                            : InkWell(
                                onTap: () {
                                  setState(() {
                                    more = Icons.expand_less;
                                    status = true;
                                  });
                                },
                                child: Row(
                                  children: <Widget>[
                                    Text(
                                      "Kepada ${kepada}",
                                      style: TextStyle(fontSize: 12),
                                    ),
                                    Icon(
                                      more,
                                      size: 15,
                                    ),
                                  ],
                                ),
                              ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 0,
                    child: Icon(Icons.more_vert),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 5,
            ),
            status ? pop(surat, posisi, dari, ke) : Container(),
            SizedBox(
              height: 0,
            ),

            //isi
            Container(
              padding: EdgeInsets.all(15.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  surat[posisi].catatan == null
                      ? Container()
                      : Text(surat[posisi].catatan),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              width: 350,
              height: 130,
              padding: EdgeInsets.all(10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Icon(
                    Icons.insert_drive_file,
                    color: Colors.white,
                    size: 40,
                  ),
                  // Text("Dokumen ${surat[posisi].perihal}"),
                  Flexible(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                            child: Text("Dokumen ${surat[posisi].perihal}")),
                        SizedBox(
                          width: 10,
                        ),
                        InkWell(
                          child: Icon(
                            Icons.file_download,
                            color: Colors.grey,
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
              decoration: BoxDecoration(
                color: Colors.grey[200],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            //bawah
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: 90,
                    height: 80,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(Icons.history),
                        Text("Riwayat"),
                      ],
                    ),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        border: Border.all(width: 1, color: Colors.grey[300])),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Container(
                    width: 100,
                    height: 80,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(Icons.reply),
                        Text("Balas"),
                      ],
                    ),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        border: Border.all(width: 1, color: Colors.grey[300])),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Container(
                    width: 100,
                    height: 80,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(Icons.send),
                        Text("Disposisi"),
                      ],
                    ),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        border: Border.all(width: 1, color: Colors.grey[300])),
                  ),
                  Container(),
                  Container(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Widget pop(List<Surat> surat, int posisi, String dari, String ke) {
  //popmore
  return Container(
    padding: EdgeInsets.all(10),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Text("Dari"),
            ),
            Expanded(
              flex: 4,
              child: Text(dari),
            )
          ],
        ),
        SizedBox(
          height: 5,
        ),
        Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Text("Kepada"),
            ),
            Expanded(
              flex: 4,
              child: Text(ke),
            )
          ],
        ),
        SizedBox(
          height: 5,
        ),
        Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Text("Tanggal"),
            ),
            Expanded(
              flex: 4,
              child: Text(Tanggal().tgl(surat[posisi].tanggal)),
            )
          ],
        ),
      ],
    ),
    decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(5)),
        border: Border.all(width: 1.0, color: Colors.grey[300])),
  );
}
