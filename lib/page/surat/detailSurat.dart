import 'dart:ui';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mobintranet/bloc/bloc_surat/bloc_detailSurat_bloc/bloc_detailsurat_bloc.dart';
import 'package:mobintranet/model/Surat.dart';
import 'package:mobintranet/page/surat/postDisposisi.dart';
import 'package:mobintranet/page/surat/replyMail.dart';
import 'package:mobintranet/page/surat/riwayat.dart';
import 'package:mobintranet/util/Db8Widget.dart';
import 'package:mobintranet/util/DbColors.dart';
import 'package:mobintranet/util/DbExtension.dart';
import 'package:mobintranet/widget/datetime.dart';
import 'package:mobintranet/widget/master.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:random_color/random_color.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ext_storage/ext_storage.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:flushbar/flushbar.dart';

class PageDetailSurat extends StatefulWidget {
  final Surat list;
  // final int index;
  final bool out;
  const PageDetailSurat({Key key, this.list, this.out}) : super(key: key);
  @override
  _PageDetailSuratState createState() => _PageDetailSuratState();
}

class _PageDetailSuratState extends State<PageDetailSurat> {
  double percentagePdf = 0.0;
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  IconData more = Icons.expand_more;
  bool status = false;
  String tokens;
  String progress;
  double percentage = 0.0;
  bool _isLoading = false;
  final blocDetail = BlocDetailsuratBloc();
  bool showHistory = false;
  FontWeight font = FontWeight.bold;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getPermission();
  }

  void getPermission() async {
    final prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('isitoken');
    setState(() {
      tokens = token;
    });
    print("getPermission");
    Map<PermissionGroup, PermissionStatus> permissions =
        await PermissionHandler().requestPermissions([PermissionGroup.storage]);
  }

  @override
  Widget build(BuildContext context) {
    changeStatusColor(fbiru1);
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    Surat surat = widget.list;

    //isi
    int idSurat = surat.idSurat;
    String hurufdepan = widget.out ? surat.tujuan : surat.pengirim;
    String namapengirim = widget.out ? surat.namaTujuan : surat.namaPengirim;
    String kepada = widget.out ? surat.namaPengirim : surat.namaTujuan;
    String dari = widget.out ? surat.tujuan : surat.pengirim;
    String ke = widget.out ? surat.pengirim : surat.tujuan;
    return Scaffold(
        key: scaffoldKey,
        appBar: AppBar(
          leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.arrow_back),
              ],
            ),
          ),
          title: Text("Detail page"),
        ),
        body: Container(
          child: Column(
            children: [
              Expanded(
                // flex:1,
                child: SingleChildScrollView(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                  child: Column(
                    children: <Widget>[
                      //header
                      Row(
                        children: <Widget>[
                          Expanded(
                            flex: 3,
                            child: surat.perihal == null
                                ? Container()
                                : Text(
                                    surat.perihal,
                                    style: TextStyle(fontSize: 20),
                                  ),
                          ),
                          Expanded(
                            flex: 0,
                            child: Icon(Icons.star_border),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),

                      //Pengirim
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              flex: 0,
                              child: Container(
                                height: 40.0,
                                width: 40.0,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colors.red,
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      Master().hurufdepan(hurufdepan),
                                      style: TextStyle(
                                          fontSize: 30, color: Colors.white),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              flex: 3,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    namapengirim,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 15),
                                  ),
                                  status
                                      ? InkWell(
                                          onTap: () {
                                            setState(() {
                                              more = Icons.expand_more;
                                              status = false;
                                            });
                                          },
                                          child: Row(
                                            children: <Widget>[
                                              Text(
                                                "Kepada ${kepada}",
                                                style: TextStyle(fontSize: 12),
                                              ),
                                              Icon(
                                                more,
                                                size: 15,
                                              ),
                                            ],
                                          ),
                                        )
                                      : InkWell(
                                          onTap: () {
                                            setState(() {
                                              more = Icons.expand_less;
                                              status = true;
                                            });
                                          },
                                          child: Row(
                                            children: <Widget>[
                                              Text(
                                                "Kepada ${kepada}",
                                                style: TextStyle(fontSize: 12),
                                              ),
                                              Icon(
                                                more,
                                                size: 15,
                                              ),
                                            ],
                                          ),
                                        ),
                                ],
                              ),
                            ),
                            Expanded(
                              flex: 0,
                              child: Icon(Icons.more_vert),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      status ? pop(surat, dari, ke) : Container(),
                      SizedBox(
                        height: 0,
                      ),

                      //isi
                      Container(
                        padding: EdgeInsets.all(15.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            surat.catatan == null
                                ? Container()
                                : Text(surat.catatan),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                        width: 350,
                        height: 130,
                        padding: EdgeInsets.all(10),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Icon(
                              Icons.insert_drive_file,
                              color: Colors.white,
                              size: 40,
                            ),
                            buttonDownload(context, surat)
                            // Text("Dokumen ${surat[posisi].perihal}"),
                          ],
                        ),
                        decoration: BoxDecoration(
                          color: Colors.grey[200],
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      //bawah
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              width: 90,
                              height: 80,
                              child: InkWell(
                                onTap: () {
                                  print("Cek");
                                  setState(() {
                                    if (showHistory == false) {
                                      showHistory = true;
                                    } else {
                                      showHistory = false;
                                    }
                                  });
                                  // return RiwayatSurat();
                                },
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(Icons.history),
                                    Text("Riwayat"),
                                  ],
                                ),
                              ),
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5)),
                                  border: Border.all(
                                      width: 1, color: Colors.grey[300])),
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            Container(
                              width: 100,
                              height: 80,
                              child: InkWell(
                                onTap: () {
                                  Navigator.push(context, MaterialPageRoute(builder: (context) => ReplyMailPage(id:idSurat)));
                                },
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(Icons.reply),
                                    Text("Balas"),
                                  ],
                                ),
                              ),
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5)),
                                  border: Border.all(
                                      width: 1, color: Colors.grey[300])),
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            Container(
                              width: 100,
                              height: 80,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  InkWell(
                                      onTap: () {
                                        Navigator.push(context, MaterialPageRoute(builder: (context) => PostDisposisi(id:idSurat)));
                                      }, child: Icon(Icons.send)),
                                  Text("Disposisi"),
                                ],
                              ),
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5)),
                                  border: Border.all(
                                      width: 1, color: Colors.grey[300])),
                            ),
                            // Container(),
                            // Container(),
                          ],
                        ),
                      ),
                      SizedBox(height: 10),
                      Container(
                        width: width,
                        // height: height,
                        padding: EdgeInsets.only(bottom: 30, top: 10),
                        decoration: boxDecoration(),
                        child: Text(
                          "Daftar History",
                          textAlign: TextAlign.center,
                          style: TextStyle(fontWeight: font),
                        ),
                      ),
                      showHistory
                          ? RiwayatSurat(idSurat: surat.idSurat)
                          : Container(child: Text("")),
                      // SizedBox(height: 50,)
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
  }

  showPrintedMessageSuccess(String title, String msg) {
    Flushbar(
      title: title,
      message: msg,
      duration: Duration(seconds: 3),
      icon: Icon(
        Icons.info,
        color: Colors.blue,
      ),
    )..show(context);
  }

  Future downloadMethod(BuildContext context, pathSurat) async {
    ProgressDialog pr;
    pr = new ProgressDialog(context, type: ProgressDialogType.Normal);
    pr.style(message: "Downloading file ...");
    try {
      await pr.show();
      const _apiDownloadSurat = "https://intranet.lan.go.id:8446/isisurat/";
      Dio dio = Dio();
      String path = await ExtStorage.getExternalStoragePublicDirectory(
          ExtStorage.DIRECTORY_DOWNLOADS);
      dio.download(
        _apiDownloadSurat + pathSurat.substring(12),
        '$path/sample.pdf',
        onReceiveProgress: (rcv, total) {
          setState(() {
            var percent = rcv / total * 100;
            _isLoading = true;
            progress = percent.ceil().toString();
            pr.update(message: "Please Wait : $progress %");
            if (progress == "100") {
              // pr.update(message: "Download Success");
              pr.hide();
              showPrintedMessageSuccess(
                  "Download Success", "Saved to Download");
            }
          });
        },
        options: Options(
            responseType: ResponseType.bytes,
            followRedirects: false,
            headers: {"Authorization": "Bearer " + tokens}),
        deleteOnError: true,
      );
      // pr.hide();
    } catch (e) {
      print(e);
    }
    setState(() {
      _isLoading = false;
    });
    // return showPrintedMessageSuccess("Download");
  }

  Widget buttonDownload(BuildContext context, Surat surat) {
    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Flexible(child: Text("Dokumen ${surat.perihal}")),
          SizedBox(
            width: 0,
          ),
          IconButton(
              icon: Icon(Icons.file_download),
              onPressed: () async {
                downloadMethod(context, surat.path);
              }),
        ],
      ),
    );
  }
}

Widget pop(Surat surat, String dari, String ke) {
  //popmore
  return Container(
    padding: EdgeInsets.all(10),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Text("Dari"),
            ),
            Expanded(
              flex: 4,
              child: Text(dari),
            )
          ],
        ),
        SizedBox(
          height: 5,
        ),
        Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Text("Kepada"),
            ),
            Expanded(
              flex: 4,
              child: Text(ke),
            )
          ],
        ),
        SizedBox(
          height: 5,
        ),
        Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Text("Tanggal"),
            ),
            Expanded(
              flex: 4,
              child: Text(Tanggal().tgl(surat.tanggal)),
            )
          ],
        ),
      ],
    ),
    decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(5)),
        border: Border.all(width: 1.0, color: Colors.grey[300])),
  );
}
