import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mobintranet/Loader.dart';
import 'package:mobintranet/api/manageapi.dart';
import 'package:mobintranet/bloc/bloc_daftar_pegawai/bloc_daftar_pegawai_bloc.dart';
import 'package:mobintranet/bloc/bloc_surat/bloc_detailSurat_bloc/bloc_detailsurat_bloc.dart';
import 'package:mobintranet/bloc/bloc_surat/bloc_surat_masuk/bloc_surat_masuk_bloc.dart';
import 'package:mobintranet/bloc/main_bloc.dart';
import 'package:mobintranet/page/errorPage/error_page.dart';
import 'package:mobintranet/page/surat/detail.dart';
import 'package:mobintranet/model/Surat.dart';
import 'package:mobintranet/page/surat/detailSurat.dart';
import 'package:mobintranet/page/surat/tambahsurat.dart';
import 'package:mobintranet/util/DbColors.dart';
import 'package:mobintranet/util/DbImages.dart';
import 'package:mobintranet/widget/datetime.dart';
import 'package:mobintranet/widget/master.dart';
import 'package:random_color/random_color.dart';
import 'package:mobintranet/page/surat/widgetSurat.dart';

class KotakMasuk extends StatefulWidget {
  @override
  _KotakMasukState createState() => _KotakMasukState();
}

class _KotakMasukState extends State<KotakMasuk> {
  FontWeight font = FontWeight.bold;
  bool baca = false;
  List<Surat> list;
  bool isi = false;
  double xOffset = 0;
  double yOffset = 0;
  double scaleFactor = 1;
  int indexListSurat;

  bool isDrawerOpen = false;
  final bloc = BlocSuratMasukBloc();
  final blocDetail = BlocDetailsuratBloc();
  List<Surat> dataSuratMasuk;
  String filterSurat ="Semua";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    bloc..add(GetSuratMasukEvent());
    
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
        transform: Matrix4.translationValues(xOffset, yOffset, 0)
          ..scale(scaleFactor)
          ..rotateY(isDrawerOpen ? -0.5 : 0),
        duration: Duration(milliseconds: 250),
        decoration: BoxDecoration(
            color: social_app_background_color,
            borderRadius: BorderRadius.circular(isDrawerOpen ? 40 : 0.0)),
        child: BlocBuilder<BlocSuratMasukBloc, BlocSuratMasukState>(
          cubit: bloc,
          builder: (context, state) {
            if (state is GetSuratMasukSuccess) {
              return isDrawerOpen
                  ? pageMinKotakMasuk(state)
                  : pageMaxKotakMasuk(state);
            }
            if (state is GetSuratMasukWaiting) {
              return Loader();
            }
            if (state is GetSuratMasukFailed) {
              return ErrorPage();
            }
            return Container();
          },
        ));
  }

  Widget pageMaxKotakMasuk(GetSuratMasukSuccess data) {
    Size media = MediaQuery.of(context).size;
    var width = MediaQuery.of(context).size.width;
    List<Surat> isiData = data.listData.where((el) => el.statusApp == "Surat Masuk").toList();
    return Scaffold(
      // bottomNavigationBar: isDrawerOpen == false
      //     ? InkWell(
      //         onTap: () {
      //           Navigator.push(context,
      //               MaterialPageRoute(builder: (context) => TambahSurat()));
      //         },
      //         child: Container(
      //           width: width * 0.2,
      //           height: width * 0.2,
      //           alignment: Alignment.bottomRight,
      //           child: Image.asset(social_fab_msg),
      //         ),
      //       )
      //     : null,
      body: Container(
        // color: social_app_background_color,
        child: FutureBuilder(
            // future: API.getinbox(mainBloc.tokenapi),
            builder: (context, res) {
              return data.listData != null
                  ? listinbox((filterSurat != "Semua")?isiData.where((i) => i.derajatSurat == filterSurat).toList():isiData, media, font, baca, isi)
                  : ErrorPage();
            }),
      ),
    );
  }

  Widget pageMinKotakMasuk(GetSuratMasukSuccess data) {
    Size media = MediaQuery.of(context).size;
    var width = MediaQuery.of(context).size.width;
    List<Surat> isiData = data.listData.where((el) => el.statusApp == "Surat Masuk").toList();
    return Container(
      // color: social_app_background_color,
      child: FutureBuilder(
          // future: API.getinbox(mainBloc.tokenapi),
          builder: (context, res) {
            return data.listData != null
                ? listinbox((filterSurat != "Semua")?isiData.where((i) => i.derajatSurat == filterSurat).toList():isiData, media, font, baca, isi)
                : Loader();
          }),
    );
  }

  Widget listinbox(
      List<Surat> surat, Size media, FontWeight font, bool baca, bool isi) {
    return InkWell(
      onTap: () {
        if (isDrawerOpen == true) {
          setState(() {
            xOffset = 0;
            yOffset = 0;
            scaleFactor = 1;
            isDrawerOpen = false;
          });
        }
      },
      child: Container(
        margin: EdgeInsets.only(top: isDrawerOpen ? 10 : 50),
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  isDrawerOpen
                      ? IconButton(
                          icon: Icon(Icons.arrow_back_ios),
                          onPressed: () {
                            setState(() {
                              xOffset = 0;
                              yOffset = 0;
                              scaleFactor = 1;
                              isDrawerOpen = false;
                            });
                          },
                        )
                      : IconButton(
                          icon: Icon(Icons.menu),
                          onPressed: () {
                            setState(() {
                              xOffset = 230;
                              yOffset = 150;
                              scaleFactor = 0.6;
                              isDrawerOpen = true;
                            });
                          }),
                  Column(
                    children: [
                      Text(
                        'Kotak Masuk',
                        style: TextStyle(fontSize: 20),
                      ),
                    ],
                  ),
                  InkWell(
                    onTap: (){
                      _handleClickMe();
                    },
                    child: Icon(Icons.format_list_bulleted_rounded),
                  )
                  // CircleAvatar()
                ],
              ),
            ),
            BlocListener<BlocDetailsuratBloc, BlocDetailsuratState>(
              cubit: blocDetail,
              listener: (context, state) {
                if(state is GetDetailSuratWaiting){
                  return Loader();
                }
                if (state is GetDetailSuratFailed) {
                  return ErrorPage();
                }
                if (state is GetDetailSuratSuccess) {
                  setState(() {
                    surat[indexListSurat].status = "buka";
                  });
                  // print(index);
                  // print(surat.length-index-1);
                  print("cek index ke page");
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => PageDetailSurat(
                                out: false,
                                list: state.listData,
                                // index: surat.length - index - 1,
                              )));
                }
              },
              child: Container(
                child: BlocBuilder<BlocDetailsuratBloc, BlocDetailsuratState>(
                  cubit: blocDetail,
                  builder: (context, state) {
                    if (state is GetDetailSuratWaiting) {
                      return gesturDetector(surat, media, font, baca, isi);
                    }
                    if (state is GetDetailSuratFailed) {
                      return gesturDetector(surat, media, font, baca, isi);
                    }
                    if (state is GetDetailSuratSuccess) {
                      return gesturDetector(surat, media, font, baca, isi);
                    }
                    return gesturDetector(surat, media, font, baca, isi);
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Future<void> _handleClickMe() async {
    return showDialog<void>(
      context: context,
      // barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16),
          ),
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          child: DialogFilter(onChanged: (value){
            print(value);
            print("cek isi");
            setState(() {
              filterSurat = value;
            });
          },),
        );
      },
    );
  }

  Widget gesturDetector(
      List<Surat> surat, Size media, FontWeight font, bool baca, bool isi) {
    RandomColor _randomColor = RandomColor();
    return Expanded(
      child: ListView.builder(
        itemCount: isi ? 0 : surat.length,
        itemBuilder: (context, index) {
          return Container(
              padding: EdgeInsets.symmetric(vertical: 1.0, horizontal: 1.0),
              height: 90,
              width: double.maxFinite,
              child: GestureDetector(
                onTap: () {
                  setState(() {
                    indexListSurat = surat.length - index -1;
                  });
                  print("cek index surat");
                  blocDetail
                    ..add(GetDetailSuratEvent(idSurat: surat[surat.length - index -1].idSurat));
                },
                child: Card(
                  color: (surat[surat.length - index - 1].status == "tutup")
                      ? Colors.white
                      : Colors.grey.shade200,
                  elevation: 0,
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 13, horizontal: 1),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Container(
                            height: 50.0,
                            width: 50.0,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: _randomColor.randomColor(
                                  colorBrightness: ColorBrightness.dark),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                surat[surat.length - index - 1].pengirim == null
                                    ? Container()
                                    : Text(
                                        Master().hurufdepan(
                                            surat[surat.length - index - 1]
                                                .pengirim),
                                        style: TextStyle(
                                            fontSize: 30, color: Colors.white),
                                      )
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 4,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    surat[surat.length - index - 1].pengirim,
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        fontWeight:
                                            (surat[surat.length - index - 1].status == "tutup")
                                                ? font
                                                : FontWeight.w400,
                                        fontSize: 14),
                                  ),
                                  surat[surat.length - index - 1].perihal ==
                                          null
                                      ? Container()
                                      : Text(
                                          surat[surat.length - index - 1]
                                                      .perihal
                                                      .length <
                                                  40
                                              ? surat[surat.length - index - 1]
                                                  .perihal
                                              : surat[surat.length - index - 1]
                                                      .perihal
                                                      .substring(0, 40) +
                                                  '...',
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              fontWeight:
                                                  (surat[surat.length - index - 1].status ==
                                                          "tutup")
                                                      ? font
                                                      : FontWeight.w400,
                                              fontSize: 12),
                                        ),
                                  Text(
                                    surat[index].catatan == null
                                        ? "Catatan Kosong"
                                        : surat[index].catatan.length < 40
                                            ? surat[index].catatan
                                            : surat[index]
                                                    .catatan
                                                    .substring(0, 40) +
                                                '...',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(fontSize: 12),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          flex: 0,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              surat[surat.length - index - 1].tanggal == null
                                  ? Container()
                                  : Text(
                                      Tanggal().tgl(
                                          surat[surat.length - index - 1]
                                              .tanggal),
                                      style: TextStyle(fontSize: 10),
                                    ),
                                    (surat[surat.length - index - 1].status == "buka")?
                                    Icon(Icons.mark_email_read_sharp, color: Colors.green,):Icon(Icons.mark_email_unread_sharp, color: Colors.lightBlue,)
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ));
        },
      ),
    );
  }
}
