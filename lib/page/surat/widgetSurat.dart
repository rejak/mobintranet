import 'package:flutter/material.dart';
import 'package:mobintranet/bloc/bloc_surat/bloc_surat_masuk/bloc_surat_masuk_bloc.dart';

class DialogFilter extends StatefulWidget {
  // final GetSuratMasukSuccess dataSurat;
  final ValueChanged<String> onChanged;

  const DialogFilter({Key key,this.onChanged})
      : super(key: key);

  @override
  _DialogFilterState createState() => _DialogFilterState();
}

class _DialogFilterState extends State<DialogFilter> {
  List<String> _derajatsurat = <String>[
      '',
      'Biasa',
      'Penting',
      'Segera',
      'Sangat Segera'
    ];
    int value;
    String valueCallBack;
  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: new BoxDecoration(
          color: Colors.white,
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(16),
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              blurRadius: 10.0,
              offset: const Offset(0.0, 10.0),
            ),
          ],
        ),
        width: MediaQuery.of(context).size.width,
        child: Padding(
            padding: EdgeInsets.all(20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Filter Jenis Surat",
                      style: TextStyle(fontSize: 20),
                    )
                  ],
                ),
                SizedBox(height: 20),
                ListView.builder(
                  itemCount: _derajatsurat.length,
                  // scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemBuilder: (context, i) {
                    return RadioListTile(
                      value: i,
                      groupValue: value,
                      activeColor: Colors.blueGrey,
                      title: (i!=0)?Text(_derajatsurat[i]):Text("Semua"),
                      onChanged: (ind) {
                        print(ind);
                        setState(() {
                          value=ind;
                          if(ind == 0){
                            valueCallBack="Semua";
                          } else {
                            valueCallBack=_derajatsurat[i];
                          }
                          
                        });
                        print(valueCallBack);
                      },
                    );
                  },
                ),
                SizedBox(height: 10),
                Padding(
                  padding: EdgeInsets.fromLTRB(40, 20, 40, 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      InkWell(
                        onTap: () {
                          widget.onChanged(valueCallBack);
                          Navigator.of(context).pop(true);
                        },
                        child: Text("Oke"),
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.of(context).pop(true);
                        },
                        child: Text("Tutup"),
                      )
                    ],
                  ),
                )
              ],
            )));
  }
}

