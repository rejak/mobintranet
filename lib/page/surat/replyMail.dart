import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_document_picker/flutter_document_picker.dart';
import 'package:mobintranet/bloc/bloc_surat/bloc_surat_keluar/bloc_surat_keluar_bloc.dart';
import 'package:mobintranet/model/Surat.dart';
import 'package:native_pdf_renderer/native_pdf_renderer.dart';
import 'package:flushbar/flushbar.dart';

class ReplyMailPage extends StatefulWidget {
  final int id;
  const ReplyMailPage({@required this.id});
  @override
  _ReplyMailPageState createState() => _ReplyMailPageState();
}

class _ReplyMailPageState extends State<ReplyMailPage> {
  File lengthFile;
  PdfPageImage thumbnail;
  String namaDoc;
  TextEditingController _formCatatan = TextEditingController();
  final bloc = BlocSuratKeluarBloc();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Balas Surat"),
        actions: [
          IconButton(
            onPressed: () {
              return _pickDocument();
            },
            icon: Icon(Icons.attachment_outlined),
          ),
          SizedBox(
            height: 30,
          ),
          BlocListener<BlocSuratKeluarBloc, BlocSuratKeluarState>(
            cubit: bloc,
            listener: (context, state) {
              if (state is PostReplyMailSuccess) {
                showPrintedMessageSuccess(
                    "Status Kirim", "Surat Sukses Terkirim");
              }
            },
            child: Container(
              child: BlocBuilder<BlocSuratKeluarBloc, BlocSuratKeluarState>(
                cubit: bloc,
                builder: (context, state) {
                  if (state is PostReplyMailWaiting) {
                    return Container(
                        child: Center(
                      child: CircularProgressIndicator(),
                    ));
                  }
                  if (state is PostReplyMailSuccess) {
                    return _sendReplyMail();
                  }
                  if (state is PostReplyMailWaitingFailed) {
                    return _sendReplyMail();
                  }
                  return _sendReplyMail();
                },
              ),
            ),
          )
        ],
      ),
      body: Container(
        padding: EdgeInsets.all(10),
        child: Column(
          children: [
            TextField(
              maxLines: 2,
              controller: _formCatatan,
              decoration: InputDecoration(
                  // hintText: "Catatan ...",
                  border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black)),
                  hintStyle: TextStyle(color: Colors.black),
                  labelText: "Catatan",
                  labelStyle: TextStyle(color: Colors.black)),
            ),
            SizedBox(
              height: 10,
            ),
            _thumbnail()
          ],
        ),
      ),
    );
  }

  showPrintedMessageSuccess(String title, String msg) {
    Flushbar(
      onStatusChanged: (val) {
        print("sini status");
        print(val.index);
        if (val.index == 1) {
          Navigator.pop(context);
        }
      },
      title: title,
      message: msg,
      duration: Duration(seconds: 3),
      icon: Icon(
        Icons.info,
        color: Colors.blue,
      ),
    )..show(context);
  }

  Widget _sendReplyMail() {
    return IconButton(
      onPressed: () {
        Surat isi = Surat(catatan: _formCatatan.text.toString(), idSurat: widget.id);
        bloc..add(PostReplyMailEvent(dataPost: isi, filepdf: lengthFile));
      },
      icon: Icon(Icons.send_outlined),
    );
  }

  Widget _thumbnail() {
    var width = MediaQuery.of(context).size.width;
    return (thumbnail != null)
        ? ClipRRect(
            borderRadius: BorderRadius.circular(8.0),
            // decoration: BoxDecoration(
            //   border: Border.all(color: Colors.white),
            //   borderRadius: BorderRadius.all(Radius.circular(
            //           5.0) //                 <--- border radius here
            //       ),
            // ),
            // width: width,
            // height: 200,
            child: Column(
              children: [
                Container(
                  width: width,
                  height: 130,
                  color: Colors.black,
                  child: Image(
                    // width: 640,
                    fit: BoxFit.none,
                    image: MemoryImage(thumbnail.bytes),
                  ),
                ),
                Container(
                  width: width,
                  height: 68,
                  color: Colors.grey,
                  child: Padding(
                    padding: EdgeInsets.only(left: 15, right: 15),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Icon(
                          Icons.image,
                          size: 40,
                          color: Colors.red,
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              namaDoc.toString(),
                              style:
                                  TextStyle(color: Colors.white, fontSize: 18),
                            ),
                            Text(
                              (lengthFile.lengthSync().toString().length <= 3)
                                  ? lengthFile.lengthSync().toString() + " B"
                                  : (lengthFile.lengthSync().toString().length >
                                              3 &&
                                          lengthFile
                                                  .lengthSync()
                                                  .toString()
                                                  .length <=
                                              6)
                                      ? lengthFile
                                              .lengthSync()
                                              .toString()
                                              .substring(
                                                  0,
                                                  lengthFile
                                                          .lengthSync()
                                                          .toString()
                                                          .length -
                                                      3) +
                                          " KB"
                                      : (lengthFile
                                                      .lengthSync()
                                                      .toString()
                                                      .length >
                                                  6 &&
                                              lengthFile
                                                      .lengthSync()
                                                      .toString()
                                                      .length <=
                                                  9)
                                          ? lengthFile
                                                  .lengthSync()
                                                  .toString()
                                                  .substring(
                                                      0,
                                                      lengthFile
                                                              .lengthSync()
                                                              .toString()
                                                              .length -
                                                          6) +
                                              " MB"
                                          : null,
                              style:
                                  TextStyle(color: Colors.white, fontSize: 12),
                            ),
                          ],
                        ),
                        InkWell(
                          onTap: () {
                            setState(() {
                              namaDoc = null;
                              lengthFile = null;
                              thumbnail = null;
                            });
                          },
                          child: Icon(Icons.delete_forever_outlined,
                              size: 40, color: Colors.white),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          )
        : Container();
  }

  Future<PdfDocument> _pickDocument() async {
    final path = await FlutterDocumentPicker.openDocument();
    PdfDocument document = await PdfDocument.openFile(path);
    final page = await document.getPage(1);
    final pageImage = await page.render(width: page.width, height: page.height);
    setState(() {
      namaDoc = path.substring(41);
      lengthFile = File(path);
      thumbnail = pageImage;
    });
  }
}
