import 'dart:convert';
import 'dart:io';
import 'dart:ui';
import 'dart:async';

import 'package:async/async.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mobintranet/Loader.dart';
import 'package:mobintranet/api/manageapi.dart';
import 'package:mobintranet/bloc/bloc_daftar_pegawai/bloc_daftar_pegawai_bloc.dart';
import 'package:mobintranet/bloc/bloc_surat/bloc_surat_keluar/bloc_surat_keluar_bloc.dart';
import 'package:mobintranet/bloc/main_bloc.dart';
import 'package:mobintranet/model/Surat.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:intl/intl.dart';
import 'package:mobintranet/model/contohModel.dart';
import 'package:mobintranet/model/pegawai.dart';
import 'package:mobintranet/model/user.dart';
import 'package:mobintranet/page/pesan.dart';
import 'package:mobintranet/util/Db8Widget.dart';
import 'package:mobintranet/util/DbColors.dart';
import 'package:mobintranet/util/DbConstant.dart';
import 'package:mobintranet/util/colors.dart';
import 'package:mobintranet/widget/multiform.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_rounded_date_picker/rounded_picker.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_document_picker/flutter_document_picker.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:printing/printing.dart';
import 'package:flutter/services.dart';
import 'package:native_pdf_renderer/native_pdf_renderer.dart';
import 'package:flushbar/flushbar.dart';

class TambahSurat extends StatefulWidget {
  @override
  _TambahSuratState createState() => _TambahSuratState();
}

Surat suratbaru = new Surat();

class _TambahSuratState extends State<TambahSurat> {
  final format = DateFormat("yyyy-MM-dd");
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  TextEditingController nomor = TextEditingController();
  TextEditingController perihal = TextEditingController();
  TextEditingController lampiran = TextEditingController();
  TextEditingController catatan = TextEditingController();
  TextEditingController lokasi = TextEditingController();
  final bloc = BlocDaftarPegawaiBloc();
  final blocSurat = BlocSuratKeluarBloc();
  File lengthFile;
  PdfPageImage thumbnail;
  List<Pegawai> listPegawai;
  List<dynamic> _tujuan;
  List<UserModel> _tujuanNama = [];
  List<dynamic> _tujuanUsername = [];
  String namaDoc;
  List<String> _jenissurat = <String>[
    '',
    'Surat Edaran',
    'Surat Pemberitahuan',
    'Surat Undangan',
    'Surat Permintaan Data',
    'Nota Dinas'
  ];

  List<String> _asalSurat = <String>[
    '',
    'Fax',
    'Langsung',
    'Paket',
    'Pos',
    'Email'
  ];
  List<String> _sifatsurat = <String>['', 'Bebas', 'Rahasia'];
  List<String> _derajatsurat = <String>[
    '',
    'Biasa',
    'Penting',
    'Segera',
    'Sangat Segera'
  ];
  String _isijenis = '';
  String _isisifat = '';
  String _isiderajat = '';
  String _isiAsalSurat = '';
  String _isitujuan = '';
  String _path = '-';
  bool _pickFileInProgress = false;
  bool _iosPublicDataUTI = true;
  bool _checkByCustomExtension = false;
  bool _checkByMimeType = false;
  final _utiController = TextEditingController(
    text: 'com.sidlatau.example.mwfbak',
  );

  final _extensionController = TextEditingController(
    text: 'mwfbak',
  );

  final _mimeTypeController = TextEditingController(
    text: 'application/pdf image/png',
  );
  final TextEditingController _tanggalFormSurat = TextEditingController();
  final TextEditingController _tanggalAwal = TextEditingController();
  final TextEditingController _tanggalAkhir = TextEditingController();
  final TextEditingController _dariFormAsalInstansi = TextEditingController();
  final TextEditingController _boxFormPenyimpananSurat =
      TextEditingController();

  List _myActivities;
  String _myActivitiesResult;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    bloc..add(GetDataPegawaiEvent());
    _myActivities = [];
    _myActivitiesResult = '';
    print(_tujuanNama);
    setState(() {
      API.getmultipegawai(mainBloc.tokenapi).then((res) {
        _tujuan = res;
      });
    });
    getDataProfil();
  }

  void getDataProfil() async {
    final prefs = await SharedPreferences.getInstance();
    var isi = jsonDecode(prefs.getString("dataProfil"));
    var data = User.fromJson(isi);
    setState(() {
      _dariFormAsalInstansi.text = data.perEselonDua;
    });
  }

  // Future<void> showPopupMenu(List<Pegawai> data)async{
  //   // for(var i=0;i<data.length;i++){

  //   // }
  //   // print(data);
  //   // print("cek dlu");
  //   // print(data);
  //   await showMenu(
  //     context:context,
  //     position: RelativeRect.fill,
  //     items: data.map((item) {
  //       return PopupMenuItem<String>(
  //         child: new Text(item.nama),
  //         value: item.username.toString(),
  //       );
  //     }).toList()
  //   );
  // }
  Widget buttonSend() {
    return InkWell(
      onTap: () {
        Surat listSurat = Surat(
            lokasi: lokasi.text.toString(),
            tanggalMulai: _tanggalAwal.text.toString(),
            tanggalSelesai: _tanggalAkhir.text.toString(),
            penerimaRequest: _tujuanUsername,
            rakSurat: _boxFormPenyimpananSurat.text.toString(),
            instansiAsal: _dariFormAsalInstansi.text.toString(),
            nomorSurat: nomor.text.toString(),
            perihal: perihal.text.toString(),
            lampiran: lampiran.text.toString(),
            catatan: catatan.text.toString(),
            asalSurat: _isiAsalSurat.toString(),
            jenisSurat: _isijenis.toString(),
            sifatSurat: _isisifat.toString(),
            derajatSurat: _isiderajat.toString(),
            tanggalSurat: _tanggalFormSurat.text.toString());
        blocSurat
          ..add(PostSuratKeluarEvent(dataPost: listSurat, filePdf: lengthFile));
      },
      child: Icon(Icons.send),
    );
  }

  void pindahPage() {
    Navigator.of(context).pop(true);
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => PageSurat(
                  indexPage: 1,
                )));
  }

  showPrintedMessageSuccess(String title, String msg) {
    Flushbar(
      onStatusChanged: (val) {
        print("sini status");
        print(val.index);
        if (val.index == 1) {
          return pindahPage();
        }
      },
      title: title,
      message: msg,
      duration: Duration(seconds: 3),
      icon: Icon(
        Icons.info,
        color: Colors.blue,
      ),
    )..show(context);
  }

  showPrintedMessageFailed(String title, String msg) {
    Flushbar(
      title: title,
      message: msg,
      duration: Duration(seconds: 3),
      icon: Icon(
        Icons.warning_rounded,
        color: Colors.red,
      ),
    )..show(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: social_app_background_color,
      appBar: AppBar(
        title: Text("Tambah Surat"),
        actions: [
          InkWell(
            onTap: () {
              return _pickDocument();
            },
            child: Icon(Icons.attachment_outlined),
          ),
          SizedBox(
            width: 30,
          ),
          BlocListener<BlocSuratKeluarBloc, BlocSuratKeluarState>(
            cubit: blocSurat,
            listener: (context, state) {
              if (state is PostSuratSuccess) {
                showPrintedMessageSuccess(
                    "Status Kirim", "Surat Sukses Terkirim");
                // Navigator.of(context).pop(true);
                // Navigator.push(
                //     context,
                //     MaterialPageRoute(
                //         builder: (context) => PageSurat(
                //               indexPage: 1,
                //             )));
              }
              if (state is PostSuratWaiting) {
                return Center(
                    child: CircularProgressIndicator(
                  backgroundColor: Colors.white,
                ));
              }
              if (state is PostSuratFailed) {
                return showPrintedMessageFailed(
                    "Status Kirim", state.errorMessage.toString());
              }
            },
            child: Container(
              child: BlocBuilder<BlocSuratKeluarBloc, BlocSuratKeluarState>(
                cubit: blocSurat,
                builder: (context, state) {
                  if (state is PostSuratSuccess) {
                    return buttonSend();
                  }
                  if (state is PostSuratWaiting) {
                    return Center(
                        child: CircularProgressIndicator(
                            backgroundColor: Colors.white));
                  }
                  if (state is PostSuratFailed) {
                    return buttonSend();
                  }
                  if (state is BlocSuratKeluarInitial) {
                    return buttonSend();
                  }
                  return Container();
                },
              ),
            ),
          ),
          SizedBox(
            width: 10,
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          // width: 100,
          color: Colors.white,
          child: Column(
            children: [
              formIsiSurat(),
              // SizedBox(height: 100,),
              Padding(
                padding: EdgeInsets.all(5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(child: _jenisSurat()),
                    Expanded(child: _sifatSurat())
                    // Text("data")
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.all(5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(child: _derajatSurat()),
                    Expanded(child: _myAsalSurat())
                    // Text("data")
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.all(5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(child: _boxPenyimpananSurat()),
                    Expanded(child: _tanggalSurat())
                    // Text("data")
                  ],
                ),
              ),
              (_isijenis == "Surat Undangan")
                  ? Padding(
                      padding: EdgeInsets.all(5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(child: _formTanggalMulai()),
                          Expanded(child: _formTanggalAkhir())
                          // Text("data")
                        ],
                      ),
                    )
                  : Container(child: Text("")),
              (_isijenis == "Surat Undangan")
                  ? Padding(
                      padding: EdgeInsets.all(5),
                      child: _lokasi(),
                    )
                  : Container(child: Text("")),
              Padding(
                padding: EdgeInsets.all(5),
                child: _dariAsalInstansi(),
              ),
              Padding(
                padding: EdgeInsets.all(5),
                child: _nomorSurat(),
              ),
              Padding(
                padding: EdgeInsets.all(5),
                child: _perihalSurat(),
              ),
              Padding(
                padding: EdgeInsets.all(5),
                child: _lampiranSurat(),
              ),
              Padding(
                padding: EdgeInsets.all(5),
                child: _catatanSurat(),
              )
              // Expanded(
              //   child: Row(
              //     children: [
              //       TextField(

              //       ),
              //       TextField(
              //         // hin
              //       )
              //     ],
              //     // children: [_jenisSurat(), _sifatSurat()],
              //   ),
              // )
            ],
          ),
        ),
      ),
    );
  }

  Future<PdfDocument> _pickDocument() async {
    final path = await FlutterDocumentPicker.openDocument();
    PdfDocument document = await PdfDocument.openFile(path);
    final page = await document.getPage(1);
    final pageImage = await page.render(width: page.width, height: page.height);
    setState(() {
      namaDoc = path.substring(41);
      lengthFile = File(path);
      thumbnail = pageImage;
    });
  }

  Widget _lokasi() {
    return TextFormField(
      // enabled: false,
      controller: lokasi,
      decoration: InputDecoration(
        labelText: 'Lokasi',
      ),
    );
  }

  Widget _catatanSurat() {
    return TextFormField(
      // enabled: false,
      controller: catatan,
      decoration: InputDecoration(
        labelText: 'Catatan Surat',
      ),
    );
  }

  Widget _lampiranSurat() {
    return TextFormField(
      // enabled: false,
      controller: lampiran,
      decoration: InputDecoration(
        labelText: 'Lampiran Surat',
      ),
    );
  }

  Widget _perihalSurat() {
    return TextFormField(
      // enabled: false,
      controller: perihal,
      decoration: InputDecoration(
        labelText: 'Perihal Surat',
      ),
    );
  }

  Widget _nomorSurat() {
    return TextFormField(
      // enabled: false,
      controller: nomor,
      decoration: InputDecoration(
        labelText: 'Nomor Surat',
      ),
    );
  }

  Widget _dariAsalInstansi() {
    return TextFormField(
      enabled: false,
      controller: _dariFormAsalInstansi,
      decoration: InputDecoration(
        labelText: 'Dari Asal Instansi/Satker/Unit Kerja',
      ),
    );
  }

  Widget _boxPenyimpananSurat() {
    return TextFormField(
      controller: _boxFormPenyimpananSurat,
      decoration: InputDecoration(
        labelText: 'Box Penyimpanan Surat',
      ),
    );
  }

  Widget _tanggalSurat() {
    return TextFormField(
      onTap: () {
        return DatePickerSurat(1);
      },
      controller: _tanggalFormSurat,
      decoration: InputDecoration(
        labelText: 'Tanggal Surat',
      ),
    );
  }

  Widget _formTanggalMulai() {
    return TextFormField(
      onTap: () {
        return DatePickerSurat(2);
      },
      controller: _tanggalAwal,
      decoration: InputDecoration(
        labelText: 'Tanggal Awal',
      ),
    );
  }

  Widget _formTanggalAkhir() {
    return TextFormField(
      onTap: () {
        return DatePickerSurat(3);
      },
      controller: _tanggalAkhir,
      decoration: InputDecoration(
        labelText: 'Tanggal Akhir',
      ),
    );
  }

  DatePickerSurat(statusForm) async {
    DateTime newDateTime = await showRoundedDatePicker(
        listDateDisabled: [
          DateTime.now().subtract(Duration(days: 2)),
          DateTime.now().subtract(Duration(days: 4)),
          DateTime.now().subtract(Duration(days: 6)),
          DateTime.now().subtract(Duration(days: 8)),
          DateTime.now().subtract(Duration(days: 10)),
          DateTime.now().add(Duration(days: 2)),
          DateTime.now().add(Duration(days: 4)),
          DateTime.now().add(Duration(days: 6)),
          DateTime.now().add(Duration(days: 8)),
          DateTime.now().add(Duration(days: 10)),
        ],
        textActionButton: tr('tab_service.configure_time_leave.filter_by.button.clear'),
        onTapActionButton: () {

        },
        textPositiveButton: tr('tab_service.configure_time_leave.filter_by.button.ok'),
        textNegativeButton: tr('tab_service.configure_time_leave.filter_by.button.close'),
        context: context,
        background: Colors.white,
        theme: ThemeData.dark(),
        imageHeader: AssetImage("assets/sdm/header2.png"),
        description: "LAN RI",
        onTapDay: (DateTime dateTime, bool available) {
          var df = new DateFormat("EEE, d MMM yyyy HH:mm:ss z");
          setState(() {
            if (statusForm == 1) {
              _tanggalFormSurat.text = dateTime.year.toString() +
                  '-' +
                  dateTime.month.toString() +
                  '-' +
                  dateTime.day.toString();
            } else if (statusForm == 2) {
              _tanggalAwal.text = dateTime.year.toString() +
                  '-' +
                  dateTime.month.toString() +
                  '-' +
                  dateTime.day.toString();
            } else {
              _tanggalAkhir.text = dateTime.year.toString() +
                  '-' +
                  dateTime.month.toString() +
                  '-' +
                  dateTime.day.toString();
            }
          });
          if (!available) {
            showDialog(
                context: context,
                builder: (c) => CupertinoAlertDialog(
                      title: Text("This date cannot be selected."),
                      actions: <Widget>[
                        CupertinoDialogAction(
                          child: Text("OK"),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        )
                      ],
                    ));
          }
          return available;
        },
        builderDay: (DateTime dateTime, bool isCurrentDay, bool isSelected,
            TextStyle defaultTextStyle) {
          if (isSelected) {
            return Container(
              decoration: BoxDecoration(
                  color: Colors.orange[600], shape: BoxShape.circle),
              child: Center(
                child: Text(
                  dateTime.day.toString(),
                  style: defaultTextStyle,
                ),
              ),
            );
          }

          if (dateTime.day == 10) {
            return Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.pink[300], width: 4),
                  shape: BoxShape.circle),
              child: Center(
                child: Text(
                  dateTime.day.toString(),
                  style: defaultTextStyle,
                ),
              ),
            );
          }
          if (dateTime.day == 12) {
            return Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.pink[300], width: 4),
                  shape: BoxShape.circle),
              child: Center(
                child: Text(
                  dateTime.day.toString(),
                  style: defaultTextStyle,
                ),
              ),
            );
          }

          return Container(
            child: Center(
              child: Text(
                dateTime.day.toString(),
                style: defaultTextStyle,
              ),
            ),
          );
        });
  }

  Widget _myAsalSurat() {
    return FormField(
      builder: (FormFieldState state) {
        return InputDecorator(
          decoration: InputDecoration(
            labelText: 'Asal Surat',
          ),
          isEmpty: _isiAsalSurat == '',
          child: new DropdownButtonHideUnderline(
            child: new DropdownButton(
              value: _isiAsalSurat,
              isDense: true,
              onChanged: (String newValue) {
                setState(() {
                  suratbaru.jenisSurat = newValue;
                  _isiAsalSurat = newValue;
                  state.didChange(newValue);
                });
              },
              items: _asalSurat.map((String value) {
                return new DropdownMenuItem(
                  value: value,
                  child: new Text(value),
                );
              }).toList(),
            ),
          ),
        );
      },
    );
  }

  Widget _derajatSurat() {
    return FormField(
      builder: (FormFieldState state) {
        return InputDecorator(
          decoration: InputDecoration(
            labelText: 'Derajat Surat',
          ),
          isEmpty: _isiderajat == '',
          child: new DropdownButtonHideUnderline(
            child: new DropdownButton(
              value: _isiderajat,
              isDense: true,
              onChanged: (String newValue) {
                setState(() {
                  suratbaru.jenisSurat = newValue;
                  _isiderajat = newValue;
                  state.didChange(newValue);
                });
              },
              items: _derajatsurat.map((String value) {
                return new DropdownMenuItem(
                  value: value,
                  child: new Text(value),
                );
              }).toList(),
            ),
          ),
        );
      },
    );
  }

  Widget _sifatSurat() {
    return FormField(
      builder: (FormFieldState state) {
        return InputDecorator(
          decoration: InputDecoration(
            labelText: 'Sifat Surat',
          ),
          isEmpty: _isisifat == '',
          child: new DropdownButtonHideUnderline(
            child: new DropdownButton(
              value: _isisifat,
              isDense: true,
              onChanged: (String newValue) {
                setState(() {
                  suratbaru.jenisSurat = newValue;
                  _isisifat = newValue;
                  state.didChange(newValue);
                });
              },
              items: _sifatsurat.map((String value) {
                return new DropdownMenuItem(
                  value: value,
                  child: new Text(value),
                );
              }).toList(),
            ),
          ),
        );
      },
    );
  }

  Widget _jenisSurat() {
    return FormField(builder: (FormFieldState state) {
      return InputDecorator(
        decoration: InputDecoration(
          labelText: 'Jenis Surat',
        ),
        isEmpty: _isijenis == '',
        child: new DropdownButtonHideUnderline(
          child: new DropdownButton(
            value: _isijenis,
            isDense: true,
            onChanged: (String newValue) {
              setState(() {
                suratbaru.jenisSurat = newValue;
                _isijenis = newValue;
                state.didChange(newValue);
              });
            },
            items: _jenissurat.map((String value) {
              return new DropdownMenuItem(
                value: value,
                child: new Text(value),
              );
            }).toList(),
          ),
        ),
      );
    });
  }

  Widget formIsiSurat() {
    return BlocBuilder<BlocDaftarPegawaiBloc, BlocDaftarPegawaiState>(
      cubit: bloc,
      builder: (context, state) {
        if (state is GetDaftarPegawaiSukses) {
          return fieldTo(state);
        }
        if (state is GetDaftarPegawaiWaiting) {
          return fieldToWaiting();
        }
        return Container();
      },
    );
  }

  Widget fieldToWaiting() {
    return Padding(
      padding: EdgeInsets.all(5),
      child: TextField(
        enabled: false,
        decoration: InputDecoration(
            prefixIcon: Padding(
                padding:
                    EdgeInsets.only(left: 10, right: 10, bottom: 10, top: 15),
                child: Text("Tujuan")),
            suffixIcon: Padding(
              padding:
                  EdgeInsets.only(left: 10, right: 10, bottom: 10, top: 15),
              child: IconButton(
                icon: Icon(
                  Icons.arrow_drop_down,
                  color: Colors.black,
                ),
                onPressed: () {
                  print("hidden");
                  // for(var i=0;i<data.listData.length;i++){
                  // showPopupMenu(data.listData);
                  // }
                },
              ),
            )),
      ),
    );
  }

  Widget fieldTo(GetDaftarPegawaiSukses data) {
    var width = MediaQuery.of(context).size.width;
    return Padding(
      padding: EdgeInsets.only(top: 10, left: 5, right: 5, bottom: 5),
      child: Column(
        children: [
          DropdownSearch<UserModel>(
            items: [],
            maxHeight: 300,
            onFind: (String filter) => getData(filter, data),
            label: "Tujuan",
            onChanged: (val) {
              print(val.nama);
              print(val.username);
              setState(() {
                _tujuanNama
                    .add(UserModel(nama: val.nama, username: val.username));
                _tujuanUsername.add({'penerima': val.username.toString()});
              });
              print(jsonEncode(_tujuanUsername));
            },
            showSearchBox: true,
          ),
          Padding(
            padding: EdgeInsets.all(5),
            child: GridView.builder(
              scrollDirection: Axis.vertical,
              itemCount: _tujuanNama.length,
              shrinkWrap: true,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 5,
                crossAxisSpacing: 1,
                // mainAxisSpacing: 1,
              ),
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (BuildContext context, index) {
                return InputChip(
                  avatar: CircleAvatar(
                    backgroundColor: Theme.of(context).accentColor,
                    child: Text(_tujuanNama[index]
                        .username
                        .substring(0, 1)
                        .toUpperCase()),
                  ),
                  label: Text(_tujuanNama[index].nama),
                  onPressed: () {
                    // replytile.removeWhere((item) => item.id == '001')
                    setState(() {
                      _tujuanNama.removeWhere(
                          (element) => element.nama == _tujuanNama[index].nama);
                      _tujuanUsername.removeWhere((element) =>
                          element.penerima == _tujuanUsername[index].penerima);
                    });
                  },
                );
              },
            ),
          ),
          (thumbnail != null)
              ? Padding(
                  padding: EdgeInsets.all(5),
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.all(Radius.circular(
                              5.0) //                 <--- border radius here
                          ),
                    ),
                    width: width,
                    height: 200,
                    child: Column(
                      children: [
                        Container(
                          width: width,
                          height: 130,
                          color: Colors.black,
                          child: Image(
                            // width: 640,
                            fit: BoxFit.none,
                            image: MemoryImage(thumbnail.bytes),
                          ),
                        ),
                        Container(
                          width: width,
                          height: 68,
                          color: Colors.grey,
                          child: Padding(
                            padding: EdgeInsets.only(left: 15, right: 15),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Icon(
                                  Icons.image,
                                  size: 40,
                                  color: Colors.red,
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      namaDoc.toString(),
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 18),
                                    ),
                                    Text(
                                      (lengthFile
                                                  .lengthSync()
                                                  .toString()
                                                  .length <=
                                              3)
                                          ? lengthFile.lengthSync().toString() +
                                              " B"
                                          : (lengthFile
                                                          .lengthSync()
                                                          .toString()
                                                          .length >
                                                      3 &&
                                                  lengthFile
                                                          .lengthSync()
                                                          .toString()
                                                          .length <=
                                                      6)
                                              ? lengthFile
                                                      .lengthSync()
                                                      .toString()
                                                      .substring(
                                                          0,
                                                          lengthFile
                                                                  .lengthSync()
                                                                  .toString()
                                                                  .length -
                                                              3) +
                                                  " KB"
                                              : (lengthFile
                                                              .lengthSync()
                                                              .toString()
                                                              .length >
                                                          6 &&
                                                      lengthFile
                                                              .lengthSync()
                                                              .toString()
                                                              .length <=
                                                          9)
                                                  ? lengthFile
                                                          .lengthSync()
                                                          .toString()
                                                          .substring(
                                                              0,
                                                              lengthFile
                                                                      .lengthSync()
                                                                      .toString()
                                                                      .length -
                                                                  6) +
                                                      " MB"
                                                  : null,
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 12),
                                    ),
                                  ],
                                ),
                                InkWell(
                                  onTap: () {
                                    setState(() {
                                      namaDoc = null;
                                      lengthFile = null;
                                      thumbnail = null;
                                    });
                                  },
                                  child: Icon(Icons.delete_forever_outlined,
                                      size: 40, color: Colors.white),
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ))
              : Container(),
        ],
      ),
    );
  }

  Future<List<UserModel>> getData(filter, GetDaftarPegawaiSukses data) async {
    // var response = await Dio().get(
    //   "http://5d85ccfb1e61af001471bf60.mockapi.io/user",
    //   queryParameters: {"filter": filter},
    // );
    var models = UserModel.fromJsonList(data.listDataNonModel);
    print(models);
    return models;
  }
}
