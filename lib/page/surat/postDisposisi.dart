import 'dart:convert';
import 'dart:ui';
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mobintranet/bloc/bloc_daftar_pegawai/bloc_daftar_pegawai_bloc.dart';
import 'package:mobintranet/bloc/bloc_surat/bloc_surat_keluar/bloc_surat_keluar_bloc.dart';
import 'package:mobintranet/model/Surat.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:intl/intl.dart';
import 'package:mobintranet/model/contohModel.dart';
import 'package:mobintranet/model/pegawai.dart';
import 'package:mobintranet/model/user.dart';
import 'package:mobintranet/page/pesan.dart';
import 'package:mobintranet/util/DbColors.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter_rounded_date_picker/rounded_picker.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flushbar/flushbar.dart';

class PostDisposisi extends StatefulWidget {
  final int id;
  const PostDisposisi({this.id});
  @override
  _PostDisposisiState createState() => _PostDisposisiState();
}

Surat suratbaru = new Surat();

class _PostDisposisiState extends State<PostDisposisi> {
  final format = DateFormat("yyyy-MM-dd");
  TextEditingController catatan = TextEditingController();
  final bloc = BlocDaftarPegawaiBloc();
  final blocSurat = BlocSuratKeluarBloc();

  String textTujuan;
  List<Pegawai> listPegawai;
  List<dynamic> _tujuan;
  List<UserModel> _tujuanNama = [];
  List<dynamic> _tujuanUsername = [];
  String namaDoc;

  List<String> _disposisiSurat = <String>[
    '',
    'Selesaikan / Tindak Lanjut',
    'Saran / Pendapat',
    'Pelajari',
    'Untuk Menjadi Perhatian',
    'File',
    'Tanggapan',
    'Siapkan Draft / Jawaban',
    'Untuk Analisi / Evaluasi',
    'Laporan / Laporkan',
    'Harap Dihadiri / Mewakili',
    'Untuk Diselesaikan',
    'Harap Dipenuhi',
    'Koodinasikan',
    'Monitor',
    'Jadwalkan',
    'Usp'
  ];
  List<String> _sifatsurat = <String>[
    '',
    'Biasa',
    'Penting',
    'Segera',
    'Sangat Segera'
  ];
  String _isiDisposisi = '';
  String _isisifat = '';
  String _isitujuan = '';
  final TextEditingController _tanggalFormSurat = TextEditingController();
  final TextEditingController _dariFormAsalInstansi = TextEditingController();

  List _myActivities;
  String _myActivitiesResult;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    bloc..add(GetDataPegawaiEvent());
  }

  void getDataProfil() async {
    final prefs = await SharedPreferences.getInstance();
    var isi = jsonDecode(prefs.getString("dataProfil"));
    var data = User.fromJson(isi);
    setState(() {
      _dariFormAsalInstansi.text = data.perEselonDua;
    });
  }

  Widget buttonSend() {
    return InkWell(
      onTap: () {
        Surat isi = Surat(idSurat: widget.id,namaTujuan: textTujuan,disposisi: _isiDisposisi,sifatSurat: _isisifat,tanggalSurat: _tanggalFormSurat.text.toString(),catatanDisposisi: catatan.text.toString());
        blocSurat..add(PostDisposisiMail(datapost: isi));
      },
      child: Icon(Icons.send),
    );
  }

  void pindahPage() {
    Navigator.of(context).pop(true);
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => PageSurat(
                  indexPage: 1,
                )));
  }

  showPrintedMessageSuccess(String title, String msg) {
    Flushbar(
      onStatusChanged: (val) {
        print("sini status");
        print(val.index);
        if (val.index == 1) {
          return Navigator.pop(context);
        }
      },
      title: title,
      message: msg,
      duration: Duration(seconds: 3),
      icon: Icon(
        Icons.info,
        color: Colors.blue,
      ),
    )..show(context);
  }

  showPrintedMessageFailed(String title, String msg) {
    Flushbar(
      title: title,
      message: msg,
      duration: Duration(seconds: 3),
      icon: Icon(
        Icons.warning_rounded,
        color: Colors.red,
      ),
    )..show(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: social_app_background_color,
      appBar: AppBar(
        title: Text("Tambah Surat"),
        actions: [
          BlocListener<BlocSuratKeluarBloc, BlocSuratKeluarState>(
            cubit: blocSurat,
            listener: (context, state) {
              if (state is PostDisposisiMailSuccess) {
                showPrintedMessageSuccess(
                    "Status Kirim", "Surat Sukses Terkirim");
              }
              if (state is PostDisposisiMailWaiting) {
                return Center(
                    child: CircularProgressIndicator(
                  backgroundColor: Colors.white,
                ));
              }
              if (state is PostDisposisiMailFailed) {
                return showPrintedMessageFailed(
                    "Status Kirim", state.errorMessage.toString());
              }
            },
            child: Container(
              child: BlocBuilder<BlocSuratKeluarBloc, BlocSuratKeluarState>(
                cubit: blocSurat,
                builder: (context, state) {
                  if (state is PostDisposisiMailSuccess) {
                    return buttonSend();
                  }
                  if (state is PostDisposisiMailWaiting) {
                    return Center(
                        child: CircularProgressIndicator(
                            backgroundColor: Colors.white));
                  }
                  if (state is PostDisposisiMailFailed) {
                    return buttonSend();
                  }
                  if (state is BlocSuratKeluarInitial) {
                    return buttonSend();
                  }
                  return Container();
                },
              ),
            ),
          ),
          SizedBox(
            width: 10,
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          color: Colors.white,
          child: Column(
            children: [
              formIsiSurat(),
              Padding(
                padding: EdgeInsets.all(5),
                child: _formDisposisiSurat(),
              ),
              Padding(
                padding: EdgeInsets.all(5),
                child: _sifatSurat(),
              ),
              Padding(
                padding: EdgeInsets.all(5),
                child: _tanggalSurat(),
              ),
              Padding(
                padding: EdgeInsets.all(5),
                child: _catatanSurat(),
              )
            ],
          ),
        ),
      ),
    );
  }


  Widget _catatanSurat() {
    return TextFormField(
      maxLines: 2,
      // enabled: false,
      controller: catatan,
      decoration: InputDecoration(
        labelText: 'Catatan Surat',
      ),
    );
  }

  Widget _tanggalSurat() {
    return TextFormField(
      onTap: () {
        return DatePickerSurat(1);
      },
      controller: _tanggalFormSurat,
      decoration: InputDecoration(
        labelText: 'Tanggal Surat',
      ),
    );
  }

  DatePickerSurat(statusForm) async {
    DateTime newDateTime = await showRoundedDatePicker(
        listDateDisabled: [
          DateTime.now().subtract(Duration(days: 2)),
          DateTime.now().subtract(Duration(days: 4)),
          DateTime.now().subtract(Duration(days: 6)),
          DateTime.now().subtract(Duration(days: 8)),
          DateTime.now().subtract(Duration(days: 10)),
          DateTime.now().add(Duration(days: 2)),
          DateTime.now().add(Duration(days: 4)),
          DateTime.now().add(Duration(days: 6)),
          DateTime.now().add(Duration(days: 8)),
          DateTime.now().add(Duration(days: 10)),
        ],
        textActionButton: tr('tab_service.configure_time_leave.filter_by.button.clear'),
        onTapActionButton: () {

        },
        textPositiveButton: tr('tab_service.configure_time_leave.filter_by.button.ok'),
        textNegativeButton: tr('tab_service.configure_time_leave.filter_by.button.close'),
        context: context,
        background: Colors.white,
        theme: ThemeData.dark(),
        imageHeader: AssetImage("assets/sdm/header2.png"),
        description: "LAN RI",
        onTapDay: (DateTime dateTime, bool available) {
          var df = new DateFormat("EEE, d MMM yyyy HH:mm:ss z");
          setState(() {
              _tanggalFormSurat.text = dateTime.year.toString() +
                  '-' +
                  dateTime.month.toString() +
                  '-' +
                  dateTime.day.toString();
          });
          if (!available) {
            showDialog(
                context: context,
                builder: (c) => CupertinoAlertDialog(
                      title: Text("This date cannot be selected."),
                      actions: <Widget>[
                        CupertinoDialogAction(
                          child: Text("OK"),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        )
                      ],
                    ));
          }
          return available;
        },
        builderDay: (DateTime dateTime, bool isCurrentDay, bool isSelected,
            TextStyle defaultTextStyle) {
          if (isSelected) {
            return Container(
              decoration: BoxDecoration(
                  color: Colors.orange[600], shape: BoxShape.circle),
              child: Center(
                child: Text(
                  dateTime.day.toString(),
                  style: defaultTextStyle,
                ),
              ),
            );
          }

          if (dateTime.day == 10) {
            return Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.pink[300], width: 4),
                  shape: BoxShape.circle),
              child: Center(
                child: Text(
                  dateTime.day.toString(),
                  style: defaultTextStyle,
                ),
              ),
            );
          }
          if (dateTime.day == 12) {
            return Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.pink[300], width: 4),
                  shape: BoxShape.circle),
              child: Center(
                child: Text(
                  dateTime.day.toString(),
                  style: defaultTextStyle,
                ),
              ),
            );
          }

          return Container(
            child: Center(
              child: Text(
                dateTime.day.toString(),
                style: defaultTextStyle,
              ),
            ),
          );
        });
  }


  Widget _sifatSurat() {
    return FormField(
      builder: (FormFieldState state) {
        return InputDecorator(
          decoration: InputDecoration(
            labelText: 'Sifat Surat',
          ),
          isEmpty: _isisifat == '',
          child: new DropdownButtonHideUnderline(
            child: new DropdownButton(
              value: _isisifat,
              isDense: true,
              onChanged: (String newValue) {
                setState(() {
                  suratbaru.jenisSurat = newValue;
                  _isisifat = newValue;
                  state.didChange(newValue);
                });
              },
              items: _sifatsurat.map((String value) {
                return new DropdownMenuItem(
                  value: value,
                  child: new Text(value),
                );
              }).toList(),
            ),
          ),
        );
      },
    );
  }

  Widget _formDisposisiSurat() {
    return FormField(
      builder: (FormFieldState state) {
        return InputDecorator(
          decoration: InputDecoration(
            labelText: 'Disposisi Surat',
          ),
          isEmpty: _isiDisposisi == '',
          child: new DropdownButtonHideUnderline(
            child: new DropdownButton(
              value: _isiDisposisi,
              isDense: true,
              onChanged: (String newValue) {
                setState(() {
                  // suratbaru.jenisSurat = newValue;
                  _isiDisposisi = newValue;
                  state.didChange(newValue);
                });
              },
              items: _disposisiSurat.map((String value) {
                return new DropdownMenuItem(
                  value: value,
                  child: new Text(value),
                );
              }).toList(),
            ),
          ),
        );
      },
    );
  }

  Widget formIsiSurat() {
    return BlocBuilder<BlocDaftarPegawaiBloc, BlocDaftarPegawaiState>(
      cubit: bloc,
      builder: (context, state) {
        if (state is GetDaftarPegawaiSukses) {
          return fieldTo(state);
        }
        if (state is GetDaftarPegawaiWaiting) {
          return fieldToWaiting();
        }
        return Container();
      },
    );
  }

  Widget fieldToWaiting() {
    return Padding(
      padding: EdgeInsets.all(5),
      child: TextField(
        enabled: false,
        decoration: InputDecoration(
            prefixIcon: Padding(
                padding:
                    EdgeInsets.only(left: 10, right: 10, bottom: 10, top: 15),
                child: Text("Tujuan")),
            suffixIcon: Padding(
              padding:
                  EdgeInsets.only(left: 10, right: 10, bottom: 10, top: 15),
              child: IconButton(
                icon: Icon(
                  Icons.arrow_drop_down,
                  color: Colors.black,
                ),
                onPressed: () {
                  print("hidden");
                },
              ),
            )),
      ),
    );
  }

  Widget fieldTo(GetDaftarPegawaiSukses data) {
    var width = MediaQuery.of(context).size.width;
    return Padding(
      padding: EdgeInsets.only(top: 10, left: 5, right: 5, bottom: 5),
      child: Column(
        children: [
          DropdownSearch<UserModel>(
            items: [],
            maxHeight: 300,
            onFind: (String filter) => getData(filter, data),
            label: "Tujuan",
            onChanged: (val) {
              setState(() {
                textTujuan = val.username;
                // _tujuanNama
                //     .add(UserModel(nama: val.nama, username: val.username));
                // _tujuanUsername.add({'penerima': val.username.toString()});
              });
            },
            showSearchBox: true,
          ),
        ],
      ),
    );
  }

  Future<List<UserModel>> getData(filter, GetDaftarPegawaiSukses data) async {
    var models = UserModel.fromJsonList(data.listDataNonModel);
    print(models);
    return models;
  }
}
