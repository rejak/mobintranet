import 'package:flutter/material.dart';
import 'package:mobintranet/page/pengumuman/detail.dart';
import 'package:mobintranet/page/surat/detail.dart';

class Pengumuman extends StatefulWidget {
  final String token;
  final String kode;

  const Pengumuman({Key key, this.token, this.kode}) : super(key: key);
  @override
  _PengumumanState createState() => _PengumumanState();
}

class _PengumumanState extends State<Pengumuman> {
  bool ada =true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
  
        leading: Container(),
        title: Text("Pengumuman"),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width ,

      child: Column(
        children: <Widget>[
          Expanded(
            child: ListView.builder(
              itemCount: 1,
              itemBuilder: (context,index){
                return Container(
                  height: 90,
                  width: MediaQuery.of(context).size.width ,
                  child: GestureDetector(
                      onTap: (){
                        setState(() {
                          ada=false;
                        });
                        Navigator.push(context, MaterialPageRoute(builder: (context) => DetailNotif()) );
                      },
                      child: Card(
                      elevation: 0,
                      child: Container(
                        
                        padding: EdgeInsets.symmetric(vertical: 14,horizontal: 1),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            SizedBox(width: 10,),
                            Expanded(
                                flex: 0,
                                child: Stack(
                                  children: <Widget>[
                                    Container(
                                    height: 30.0,
                                    width: 30.0,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Colors.blue,
                                    ),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        Icon(Icons.notifications,color: Colors.white,size: 14,),
                                      ],
                                    ),
                                  ),
                                  ada? Positioned(
                                    right: 1,
                                    top: 0,
                                    child: Container(
                                      constraints: BoxConstraints(
                                        minWidth: 10,
                                        minHeight: 10,
                                      ),
                                      child: Container() ,
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Colors.red,
                                      ),
                                    ),
                                  ):Container(),   
                                  ],
                                ),
                            ),
                            SizedBox(width: 10,),
                            Expanded(
                              flex: 4,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text("LAN Sambut CPNS Lulusan PKN STAN",textAlign: TextAlign.left, style: TextStyle( fontWeight: FontWeight.bold,fontSize: 17),),
                                      Flexible(child: Text("Jakarta - Lembaga Administrasi Negara (LAN)...",textAlign: TextAlign.left,style: TextStyle(fontSize: 13),)),
                                    
                                      ],
                                      ),
                                          ],
                                ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    ),
    );
  }
}