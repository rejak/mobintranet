import 'package:flutter/material.dart';
import 'package:mobintranet/util/DbColors.dart';
import 'package:mobintranet/util/DbImages.dart';

class ErrorPage extends StatelessWidget {
  const ErrorPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: fbiru2,
      child: Center(
        child: Image.asset(errorPage, fit: BoxFit.fill)
      ),
    );
  }
}