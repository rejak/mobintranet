import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mobintranet/api/manageapi.dart';
import 'package:mobintranet/bloc/bloc_daftar_pegawai/bloc_daftar_pegawai_bloc.dart';
import 'package:mobintranet/model/user.dart';
import 'package:mobintranet/page/absen/absen.dart';
import 'package:mobintranet/page/absenwfh/absenwfh.dart';
import 'package:mobintranet/page/hukum/home.dart';
import 'package:mobintranet/page/sdm/menuSdm.dart';
import 'package:mobintranet/page/sdm/sdm.dart';
import 'package:mobintranet/widget/datetime.dart';

import 'package:permission_handler/permission_handler.dart';

import 'absenwfh/kamera.dart';

class Home1 extends StatefulWidget {
  
  final String token;
  final String kode;

  const Home1({Key key, this.token, this.kode}) : super(key: key);
  
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home1> {
  int _current = 0;
  String eselon;
  User userr;
  String nama;
  String jk;
  String namahari;
  String jammasuk="-";
  String jamkeluar="-";
  bool cekpermisi;

  List<User> userList = new List<User>();

  final PermissionHandler _permissionHandler = PermissionHandler();

  //api
   void _ambilUser(){
    API.getUser(widget.token).then((response){
      setState(() {
        Map<String,dynamic> user = jsonDecode(response.body);
        var usr= User.fromJson(user);
        userr=usr;
        //user.map<User>((json)=> User.fromJson(json));
        nama=user['username'];
        String jen=user['jenisKelamin'];
        if(jen=='L'){jk="Pak";}
        else{jk="Bu";}
        List<String> nam = nama.split('.');
        nama=nam[0];
        setState(() {
          eselon= user['eselon'];
          });
          //blocdata.seteselon(user['eselon']);
      });
    });
  }

  void _getabsen(){
    API.getabsen(widget.token).then((res){
      setState(() {
        List<dynamic> absen= jsonDecode(res.body);
        // print(absen[0]);
        if(absen!=null){
        jammasuk=absen[0]['jam_masuk'];
        jammasuk=jammasuk.substring(0,5);
        jamkeluar=absen[0]['jam_keluar'];
        jamkeluar=jamkeluar.substring(0,5);
        }
      });
    });
  }

  // Permission permission;

Future<bool> _requestPermission(PermissionGroup permission) async {
    var result = await _permissionHandler.requestPermissions([permission]);
    if (result[permission] == PermissionStatus.granted) {
      return true;
    }
    return false;
  }
Future<bool> hasPermission(PermissionGroup permission) async {
    var permissionStatus =
        await _permissionHandler.checkPermissionStatus(permission);
    return permissionStatus == PermissionStatus.granted;
  }


   Future<bool> requestContactsPermission() async {
    return _requestPermission(PermissionGroup.contacts);
  }
  /// Requests the users permission to read their location when the app is in use
  Future<bool> requestLocationPermission() async {
    return _requestPermission(PermissionGroup.locationWhenInUse);
  }
  Future<bool> requestCamera() async {
    return _requestPermission(PermissionGroup.camera);
  }

  Future<bool> hasContactsPermission() async {
    return hasPermission(PermissionGroup.contacts);
  }

  Future<bool> hasLocationPermission() async {
    return hasPermission(PermissionGroup.locationWhenInUse);
  }
  //end permission

  
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _ambilUser();
    _getabsen();
   
   
    // requestContactsPermission();
    requestCamera();
    requestLocationPermission();
    setState(() {
      namahari=Tanggal().hariini();
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MultiBlocProvider(
        providers: [
          BlocProvider<BlocDaftarPegawaiBloc>(
            create: (BuildContext context) => BlocDaftarPegawaiBloc()..add(GetDataPegawaiEvent()),
          )
        ],
        child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.all(15.0),
          child: ListView(
            children: <Widget>[
              Container(
                height: 30,
                decoration: BoxDecoration(
                  color: Colors.blue[900],
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8),
                    topRight: Radius.circular(8),
                  )
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("Selamat Datang $jk $nama ",style: TextStyle(color: Colors.white),),
                    ],
                  ),
                ),
              ),
              Container(
                height: 80,
                decoration: BoxDecoration(
                  color: Colors.blue[800],
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(8),
                    bottomRight: Radius.circular(8),
                  ),
                ), 
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(" ${namahari}",style: TextStyle(color: Colors.white,fontSize: 20.0),),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("Datang :",style: TextStyle(color: Colors.white,fontSize: 15.0),),
                            SizedBox(width: 10.0,),
                            Text("${jammasuk}",style: TextStyle(color: Colors.white,fontSize: 15.0),),
                            SizedBox(width: 50.0,),
                            Text("Pulang :",style: TextStyle(color: Colors.white,fontSize: 15.0),),
                            SizedBox(width: 10.0,),
                            Text("${jamkeluar}",style: TextStyle(color: Colors.white,fontSize: 15.0),),
                          ],
                        ),
                      ],
                    ),
                  ),
                  
                ],),
              ),

              SizedBox(height: 20,),
              Text("Layanan :",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 15),),
              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  
                  // HeaderItem(
                  //   warnatombol: Colors.red[500],
                  //   ikontombol:  Icons.timer,
                  //   judul: "Laporan WFH",
                  //   notif: 0,
                  //   aktif: true,
                  //   // url: (context)=> Absenwfh(),
                  //   url: (context)=> Absenwfh(tanggal: namahari),
                  // ),

                  // HeaderItem(
                  //   warnatombol: Colors.blue[500],
                  //   ikontombol: Icons.timer,
                  //   judul: "Absen",
                  //   notif: 0,
                  //   aktif: true,
                  //   url: (context)=> Absen(tanggal: namahari,jamdatang: jammasuk,jampulang: jamkeluar,),
                  // ),
                  
                  HeaderItem(
                    warnatombol: Colors.deepPurple[500],
                    ikontombol: Icons.gavel,
                    judul: "Hukum",
                    aktif: true,
                    notif: 0,
                    url: (context)=>HomeHukum(),
                  ),
                  HeaderItem(
                    warnatombol: Colors.orange[500],
                    ikontombol: Icons.person,
                    judul: "SDM",
                    aktif: true,
                    notif: 0,
                    url: (context)=>MenuSdm(),
                  ),
                  
                ],
              ),
              //SizedBox(height: 20,),
              // Row(
              //   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              //   children: <Widget>[
                  
              //     HeaderItem(
              //       warnatombol: Colors.green[500],
              //       ikontombol: Icons.tab,
              //       judul: "Umum",
              //       aktif: false,
              //       notif: 0,
              //       url: (context)=>Home(token: widget.token,kode: widget.kode,),
              //     ),
              //     // HeaderItem(
              //     //   warnatombol: Colors.deepPurple[500],
              //     //   ikontombol: Icons.assistant,
              //     //   judul: "Pusdatin",
              //     //   notif: 0,
              //     //   aktif: false,
              //     // ),
              //     HeaderItem(
              //       warnatombol: Colors.pink[500],
              //       ikontombol: Icons.monetization_on,
              //       judul: "Keuangan",
              //       notif: 0,
              //       aktif: false,
              //       url: (context)=>Home(token: widget.token,kode: widget.kode,),
              //     ),
              //     HeaderItem(
              //       warnatombol: Colors.brown[500],
              //       ikontombol: Icons.shop,
              //       judul: "Gratifikasi",
              //       notif: 0,
              //       aktif: false,
              //       url: (context)=>Home(token: widget.token,kode: widget.kode,),
              //     ),
              //     HeaderItem(
              //       warnatombol: Colors.black,
              //       judul: "Menu Lain",
              //       notif: 0,
              //       aktif: false,
              //       url: (context)=>Home(token: widget.token,kode: widget.kode,),
              //     ),
              //   ],
              // ),
              SizedBox(height: 40,),
              CarouselSlider(
                height: 180.0,
                items: [1].map((i) {
                  return Builder(
                    builder: (BuildContext context) {
                      return Container(
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.symmetric(horizontal: 5.0),
                        decoration: BoxDecoration(
                          color: Colors.amber
                        ),
                        child: Image.network("http://lan.go.id/media/k2/items/cache/3250ef2cd767a7edfd5574c428bc91dd_XL.jpg",fit: BoxFit.cover,)
                      );
                    },
                  );
                }).toList(),
              ),
             
              
            ],
          ),
        ),
      ),
      )
    );
  }

  // void requestPermission() async{
  //   var result=SimplePermissions.requestPermission(permission);
  //   print("request :"+ result.toString());
  //   }


}
class HeaderItem extends StatelessWidget {
   final String images;
  final String judul;
  final Color textcolor;
  final Color warnatombol;
  final IconData ikontombol;
  final int notif;
  final Widget Function(BuildContext) url;
  final bool aktif;

  const HeaderItem({Key key, this.images, this.judul, this.textcolor, this.warnatombol,this.ikontombol, this.notif, this.url, this.aktif, }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        InkWell(
          onTap: (){
            Navigator.push(context, MaterialPageRoute(builder: url));
          },
          child: Stack(
            children: <Widget>[
                Container(
                width: 55.0,
                height: 55.0,
                decoration: new BoxDecoration(
                  shape: BoxShape.circle,
                  color: warnatombol ?? Colors.blue,
                ),
                child: Padding(
                  padding: EdgeInsets.all(15),
                  child: Icon(ikontombol ?? Icons.apps, color: Colors.white,),
                ),
                ),
              Positioned(

                child: aktif?Container():Container(
                  padding: EdgeInsets.only(top: 10),
                width: 55.0,
                height: 55.0,
                decoration: new BoxDecoration(
                  
                  shape: BoxShape.circle,
                  color: Colors.black.withOpacity(0.5) ?? Colors.blue,
                ),
                child: 
                Center(
                  child: Text("Comming Soon",style: TextStyle(color: Colors.white,fontSize: 12,fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                ),
                  
                ),
              ),
              

              (notif!=0)?Positioned(
                right: 1,
                top: 0,
                child: Container(
                  constraints: BoxConstraints(
                    minWidth: 15,
                    minHeight: 15,
                  ),
                  child: Container(
                    child: 
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                        Text('$notif',style: TextStyle(color: Colors.white,fontSize: 10),),
                      ],),
                  ) ,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.red,
                  ),
                ),
              ):Container(),
            ],
          ),
        ),
        
        SizedBox(height: 10,),
        Text(
          judul,
          style: TextStyle(color: textcolor ?? Colors.black),
        ),
      ],
    );
  }
}