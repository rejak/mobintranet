import 'dart:convert';
import 'dart:io';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mobintranet/Animation/fade_animation.dart';
import 'package:mobintranet/api/manageapi.dart';
import 'package:mobintranet/bloc/blocdata.dart';
import 'package:mobintranet/bloc/main_bloc.dart';
import 'package:mobintranet/model/Db8Model.dart';
import 'package:mobintranet/model/user.dart';
import 'package:mobintranet/page/absenwfh/Nabsen.dart';
import 'package:mobintranet/page/absenwfh/absenwfh.dart';
import 'package:mobintranet/page/hukum/home.dart';
import 'package:mobintranet/page/pesan.dart';
import 'package:mobintranet/page/profil.dart';
import 'package:mobintranet/page/sdm/menuSdm.dart';
import 'package:mobintranet/page/sdm/sdm.dart';
import 'package:mobintranet/util/Db8Widget.dart';
import 'package:mobintranet/util/DbColors.dart';
import 'package:mobintranet/util/DbConstant.dart';
import 'package:mobintranet/util/DbDataGenerator.dart';
import 'package:mobintranet/util/DbImages.dart';
import 'package:mobintranet/util/DbStrings.dart';
import 'package:mobintranet/widget/datetime.dart';
import 'package:page_transition/page_transition.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:mobintranet/util/images.dart';
import 'package:trust_location/trust_location.dart';

void main() => runApp(EasyLocalization(child: Main()));

class Main extends StatefulWidget {
  static String tag = '/Home';
  final String token;
  final String kode;

  const Main({Key key, this.token, this.kode}) : super(key: key);

  @override
  MainState createState() => MainState();
}

class MainState extends State<Main> with SingleTickerProviderStateMixin {
  List<DB8Scene> mList1;
  List<DB8Rooms> mList2;
  int _current = 0;
  String eselon;
  User userr;
  String nama;
  String jk;
  String namahari;
  String jammasuk = "-";
  String jamkeluar = "-";
  bool cekpermisi;
  String stskerja = "";

  List<User> userList = new List<User>();

  final PermissionHandler _permissionHandler = PermissionHandler();
  AnimationController animController;
  Animation animation;

  //api
  void _ambilUser() {
    API.getUser(widget.token).then((response) {
      setState(() {
        Map<String, dynamic> user = jsonDecode(response.body);
        var usr = User.fromJson(user);
        userr = usr;
        //user.map<User>((json)=> User.fromJson(json));
        nama = user['username'];
        String jen = user['jenisKelamin'];
        if (jen == 'L') {
          jk = "Pak";
        } else {
          jk = "Bu";
        }
        List<String> nam = nama.split('.');
        nama = nam[0];
        setState(() {
          eselon = user['eselon'];
        });
        //blocdata.seteselon(user['eselon']);
      });
    });
  }

  void _getabsen() {
    API.getabsen(widget.token).then((res) {
      setState(() {
        List<dynamic> absen = jsonDecode(res.body);
        // print(absen[0]);
        if (absen != null) {
          jammasuk = absen[0]['jam_masuk'].toString();
          jammasuk = jammasuk.substring(0, 5);
          jamkeluar = absen[0]['jam_keluar'];
          jamkeluar = jamkeluar.substring(0, 5);
        }
      });
    });
  }

  // Permission permission;

  Future<bool> _requestPermission(PermissionGroup permission) async {
    var result = await _permissionHandler.requestPermissions([permission]);
    if (result[permission] == PermissionStatus.granted) {
      return true;
    }
    return false;
  }

  Future<bool> hasPermission(PermissionGroup permission) async {
    var permissionStatus =
        await _permissionHandler.checkPermissionStatus(permission);
    return permissionStatus == PermissionStatus.granted;
  }

  Future<bool> requestContactsPermission() async {
    return _requestPermission(PermissionGroup.contacts);
  }

  /// Requests the users permission to read their location when the app is in use
  Future<bool> requestLocationPermission() async {
    return _requestPermission(PermissionGroup.locationWhenInUse);
  }

  Future<bool> requestCamera() async {
    return _requestPermission(PermissionGroup.camera);
  }

  Future<bool> hasContactsPermission() async {
    return hasPermission(PermissionGroup.contacts);
  }

  Future<bool> hasLocationPermission() async {
    return hasPermission(PermissionGroup.locationWhenInUse);
  }
  //end permission

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _ambilUser();
    _getabsen();

    // requestContactsPermission();
    requestCamera();
    requestLocationPermission();
    setState(() {
      namahari = Tanggal().hariini();
      stskerja = mainBloc.statuswork;
    });

    API.getlaporan(mainBloc.tokenapi).then((res) {
      DateTime masuk;
      DateTime keluar;
      if (res.absenMasuk != null) {
        masuk = DateTime.parse(res.absenMasuk).toLocal();
        blocdata.setjammasuk("${masuk.hour}:${masuk.minute}:${masuk.second}");
      } else {
        blocdata.setjammasuk("-");
      }

      if (res.absenPulang != null) {
        keluar = DateTime.parse(res.absenPulang).toLocal();
        blocdata
            .setjamkeluar("${keluar.hour}:${keluar.minute}:${keluar.second}");
      } else {
        blocdata.setjamkeluar("-");
      }
      print("masukkk");
      print(blocdata.jammasuk);
    });

    mList1 = getScene();
    mList2 = getRooms();

    animController =
        AnimationController(duration: Duration(seconds: 2), vsync: this);
    animation = Tween<double>(begin: 250, end: 260).animate(animController)
      ..addListener(() {})
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          animController.reverse();
        } else if (status == AnimationStatus.dismissed) {
          animController.forward();
        }
      });
    animController.forward();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var h = MediaQuery.of(context).size.height;
    width = width - 50;
    var mSelection = 0;

    Widget mOption(var icon, var value, var subValue) {
      return Row(
        children: <Widget>[
          // SvgPicture.asset(icon, width: 22, height: 22),
          SizedBox(width: 8),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              text(value, fontFamily: fontMedium),
              text(subValue, textColor: db8_textColorSecondary, fontSize: 12.0),
            ],
          )
        ],
      );
    }

    Widget mHeading(var value) {
      return Container(
        margin: EdgeInsets.all(16.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            text(value, fontFamily: fontMedium),
            // Icon(Icons.add, color: db8_textColorPrimary, size: 18),
          ],
        ),
      );
    }

    final scene = Container(
      height: 90,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: mList1.length,
        shrinkWrap: true,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: mList1[index].url == "hukum"
                          ? (context) => HomeHukum()
                          : mList1[index].url == "sdm"
                              ? (context) => MenuSdm()
                              : mList1[index].url == "surat"
                                  ? (context) => PageSurat(
                                        indexPage: 0,
                                      )
                                  : null));
            },
            child: Container(
              alignment: Alignment.center,
              margin: EdgeInsets.only(left: 16),
              // decoration: boxDecoration(bgColor: mSelection == index ? merah : db8_white),
              decoration: boxDecoration(bgColor: mList1[index].color),
              padding: EdgeInsets.fromLTRB(16, 10, 16, 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SvgPicture.asset(mList1[index].img,
                      width: width * 0.09,
                      height: width * 0.09,
                      color: db8_white),
                  SizedBox(height: 4),
                  text(
                      (mList1[index].name == "Hukum")
                          ? tr('tab_service.law')
                          : (mList1[index].name == "SDM")
                              ? tr('tab_service.sdm')
                              : (mList1[index].name == "Persuratan")
                                  ? tr('tab_service.correspondence')
                                  : "",
                      textColor: db8_white)
                ],
              ),
            ),
          );
        },
      ),
    );

    return Scaffold(
      backgroundColor: fbiru1,
      body: SafeArea(
        child: Container(
          // height: 10,
          child: Stack(
            children: [
              Container(
                height: width * 2,
                margin: EdgeInsets.all(16),
                child: Stack(
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Opacity(
                            opacity: 0.5,
                            child: AnimatedBuilder(
                              animation: animation,
                              builder: (BuildContext context, Widget child) {
                                return FadeAnimation(
                                    1,
                                    Image.asset(
                                      "assets/login/vektor2.png",
                                      width: animation.value,
                                    ));
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            GestureDetector(
                              onTap: () {
                                print("tess");
                                Navigator.push(
                                  context,
                                  PageTransition(
                                      duration: Duration(milliseconds: 500),
                                      type: PageTransitionType.scale,
                                      alignment: Alignment.center,
                                      child: Profil()),
                                );
                              },
                              child: Row(
                                children: [
                                  SvgPicture.asset(profil,
                                      color: db8_white, width: 24, height: 24),
                                ],
                              ),
                            ),
                            text("Intranet LAN",
                                textColor: db8_white,
                                fontSize: textSizeNormal,
                                fontFamily: fontBold),
                            SvgPicture.asset(db8_ic_notification,
                                color: db8_white, width: 24, height: 24),
                          ],
                        ),
                        SizedBox(height: 10),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Column(
                              children: <Widget>[
                                text(
                                    (jk == "Pak")
                                        ? tr('title') +
                                            " " +
                                            tr('gender_male') +
                                            " $nama"
                                        : tr('title') +
                                            " " +
                                            tr('gender_female') +
                                            " $nama",
                                    textColor: db8_white,
                                    fontSize: textSizeNormal),
                              ],
                            )
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Column(
                              children: <Widget>[
                                (Platform.isAndroid)
                                    ? text(
                                        tr('job_description') +
                                            " : " +
                                            stskerja,
                                        textColor: db8_white,
                                        fontSize: textSizeMedium)
                                    : Container(),
                                text("${namahari}",
                                    textColor: db8_white,
                                    fontSize: textSizeSmall),
                              ],
                            )
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 75),
                child: SingleChildScrollView(
                  padding: EdgeInsets.only(top: width * 0.3),
                  child: Container(
                    alignment: Alignment.topLeft,
                    height: MediaQuery.of(context).size.height - h * 0.15,
                    decoration: BoxDecoration(
                        color: db4_LayoutBackgroundWhite,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(24),
                            topRight: Radius.circular(24))),
                    child: Column(
                      children: <Widget>[
                        // FadeAnimation(0.5,Image.asset("assets/login/vektor2.png")),
                        Container(
                          decoration: boxDecoration(bgColor: db8_viewColor),
                          margin: EdgeInsets.fromLTRB(16, 20, 16, 20),
                          padding: EdgeInsets.all(8),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      mOption("", mainBloc.jammasuk,
                                          tr('tab_absent.hours_of_entry'))
                                    ],
                                  ),
                                  flex: 3),
                              Expanded(
                                child: GestureDetector(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => Nabsen(
                                                  tanggal: namahari,
                                                )));
                                  },
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Container(
                                        alignment: Alignment.center,
                                        margin: EdgeInsets.only(left: 16),
                                        decoration:
                                            boxDecoration(bgColor: fbiru1),
                                        padding:
                                            EdgeInsets.fromLTRB(16, 10, 16, 10),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            SvgPicture.asset(jam_pulang,
                                                width: width * 0.09,
                                                height: width * 0.09,
                                                color: db8_white),
                                            SizedBox(height: 4),
                                            text(tr('tab_absent.absent'),
                                                textColor: db8_white,
                                                fontSize: textSizeSmall)
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                flex: 3,
                              ),
                              // SizedBox(width: 8),
                              Expanded(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      mOption("", mainBloc.jamkeluar,
                                          tr('tab_absent.home_hour'))
                                    ],
                                  ),
                                  flex: 3)
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(bottom: 16),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(24),
                                  topRight: Radius.circular(24)),
                              color: db8_viewColor),
                          child: Column(
                            children: <Widget>[
                              mHeading(tr('tab_service.title')),
                              scene,
                              mHeading(tr('tab_announcements.title')),
                              SizedBox(
                                height: 225,
                                child: ListView.builder(
                                  scrollDirection: Axis.horizontal,
                                  itemCount: mList2.length,
                                  shrinkWrap: true,
                                  itemBuilder: (context, index) {
                                    return Rooms(mList2[index], index);
                                  },
                                ),
                              ),
                              // SizedBox(height: 50,),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class Rooms extends StatelessWidget {
  DB8Rooms model;

  Rooms(DB8Rooms model, int pos) {
    this.model = model;
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Container(
      margin: EdgeInsets.only(left: 16),
      width: MediaQuery.of(context).size.width * 0.7,
      decoration: boxDecoration(),
      padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            child:
                Image.asset(model.img, height: width * 0.3, fit: BoxFit.fill),
          ),
          text(model.name, fontFamily: fontMedium),
          text(model.device, textColor: db8_textColorSecondary),
        ],
      ),
    );
  }
}
