
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mobintranet/Loader.dart';
import 'package:mobintranet/api/manageapi.dart';
import 'package:mobintranet/bloc/bloc_hukum/bloc_pengajuan/bloc_pengajuan_bloc.dart';
import 'package:mobintranet/bloc/main_bloc.dart';
import 'package:mobintranet/model/pengajuan_hukum.dart';
import 'package:mobintranet/util/DbColors.dart';
import 'package:mobintranet/widget/master.dart';

class Pengajuan extends StatefulWidget {
  @override
  _PengajuanState createState() => _PengajuanState();
}

class _PengajuanState extends State<Pengajuan> {
  final bloc = BlocPengajuanBloc();
  List<Pengajuan_hukum> list;
  String jenis="Peraturan LAN";
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    bloc..add(GetPengajuanEvent());
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child:  Column(
          children: <Widget>[
            Text("Daftar Pengajuan Produk Hukum",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            SizedBox(height: 10,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                RaisedButton(
                  color: fbiru1,
                  onPressed: (){
                    setState(() {
                      jenis="Peraturan LAN";
                    });
                  },
                  child: Text("Peraturan LAN",style: TextStyle(color: Colors.white),),
                ),
                RaisedButton(
                  color: fbiru1,
                  onPressed: (){
                    setState(() {
                      jenis="Peraturan Kepala LAN";
                    });
                  },
                  child: Text("Peraturan Kepala LAN",style: TextStyle(color: Colors.white),),
                ),
              ],
            ),
            Expanded(
              child: BlocBuilder<BlocPengajuanBloc, BlocPengajuanState>(
                cubit: bloc,
                builder: (context, state) {
                  if(state is GetPengajuanSukses){
                    return state.listData!=null?
                  cardlist(state.listData,jenis)
                  :Loader();
                  }
                  if(state is GetPengajuanGagal){
                    return Container(child: Text("Error"));
                  }
                  if(state is GetPengajuanWaiting){
                    return Loader();
                  }
                  return Loader();
                },
              )
            ),
            
          ],
        ),
   
    );
  }

  Widget cardlist(List<Pengajuan_hukum> pengajuan, String jenis){
    return Container(
      child: ListView.builder(
        itemCount: pengajuan.length,
        itemBuilder: (context,posisi){
          return 
          pengajuan[posisi].jenis!= jenis?
            Container(
            height: 70,
            child: InkWell(
              onTap: (){
                print("produk  hukum");
              },
              child: Card(
                child: Container(
                  padding: EdgeInsets.all(5),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(Icons.description)
                          ],
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(pengajuan[posisi].judulPeraturan,style: TextStyle(fontWeight: FontWeight.bold),),
                            Text(pengajuan[posisi].jenis),
                          ],
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Text("Rabu 23 Jan 2020",style: TextStyle(fontSize: 12),),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                label("PerKa", forange1),
                                label("Disetujui", fhijau1)
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ):Container();
        },
      ),
    );
  }
}