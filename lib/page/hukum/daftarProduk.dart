import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mobintranet/Loader.dart';
import 'package:mobintranet/api/manageapi.dart';
import 'package:mobintranet/bloc/bloc_hukum/bloc_produk/bloc_produk_bloc.dart';
import 'package:mobintranet/bloc/main_bloc.dart';
import 'package:mobintranet/model/produk_hukum.dart';
import 'package:mobintranet/util/DbColors.dart';
import 'package:mobintranet/widget/datetime.dart';
import 'package:mobintranet/widget/master.dart';

class DaftarProduk extends StatefulWidget {
  @override
  _DaftarProdukState createState() => _DaftarProdukState();
}

class _DaftarProdukState extends State<DaftarProduk> {
  List<ProdukHukum> list;
  final bloc = BlocProdukBloc();
  String status="Masih Berlaku";
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    bloc..add(GetProdukEvent());
  }
  @override
  Widget build(BuildContext context) {
    
    return Container(
      padding: EdgeInsets.all(10),
      child:  Column(
          children: <Widget>[
            Text("Daftar Produk Hukum",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            SizedBox(height: 10,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                RaisedButton(
                  color: fbiru1,
                  onPressed: (){
                    setState(() {
                      status="Masih Berlaku";
                    });
                  },
                  child: Text("Masih Berlaku",style: TextStyle(fontSize: 11 ,color: Colors.white),),
                ),
                RaisedButton(
                  color: fbiru1,
                  onPressed: (){
                    setState(() {
                      status="Tidak Berlaku";
                    });
                  },
                  child: Text("Tidak Berlaku",style: TextStyle(fontSize: 11 ,color: Colors.white),),
                ),
                RaisedButton(
                  color: fbiru1,
                  onPressed: (){
                    setState(() {
                      status=null;
                    });
                  },
                  child: Text("Tidak Ada Masa Berlaku",style: TextStyle(fontSize: 9 ,color: Colors.white),),
                ),
              ],
            ),
            Expanded(
              child: BlocBuilder<BlocProdukBloc, BlocProdukState>(
                cubit: bloc,
                builder: (context, state) {
                  if(state is GetProdukSukses){
                    return state.listData !=null ?
                  cardlist(state.listData)
                  :Loader();
                  }
                  if(state is GetProdukWaiting){
                    return Loader();
                  }
                  if(state is GetProdukGagal){
                    return Container(child: Text("Error"),);
                  }
                  return Loader();
                },
              )
            ),
          ],
        ),
   
    );
  }

  Widget cardlist(List<ProdukHukum> produk){
    return Container(
      child: ListView.builder(
        itemCount: produk.length,
        itemBuilder: (context,posisi){
          
          return 
          produk[posisi].keberlakuan== status? 
          Container(
              height: 100,
              child: InkWell(
                onTap: (){
                  print("produk  hukum");
                },
                child: Card(
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(Icons.description)
                            ],
                          ),
                        ),
                        Expanded(
                          flex: 4,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(produk[posisi].namaPeraturan.length<40? produk[posisi].namaPeraturan:produk[posisi].namaPeraturan.substring(0,40)+'...' ,style: TextStyle(fontWeight: FontWeight.bold),),
                              Text(produk[posisi].nomor),
                            ],
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Text(produk[posisi].tanggalPenetapan!=null?Tanggal().tgl(produk[posisi].tanggalPenetapan):'-',style: TextStyle(fontSize: 12),),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  label(produk[posisi].keberlakuan!=null?produk[posisi].keberlakuan:'Tidak ada keterangan', fungu),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ):Container();
        },
      )
    );
  }

}