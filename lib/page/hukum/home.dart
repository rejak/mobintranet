import 'package:flutter/material.dart';
import 'package:mobintranet/page/hukum/daftarProduk.dart';
import 'package:mobintranet/page/hukum/kalender.dart';
import 'package:mobintranet/page/hukum/pengajuan.dart';
import 'package:mobintranet/util/DbColors.dart';

class HomeHukum extends StatefulWidget {
  @override
  _HomeHukumState createState() => _HomeHukumState();
}

class _HomeHukumState extends State<HomeHukum> {
  int selectmenu =0;
  
  final _page = [
    Pengajuan(),
    DaftarProduk(),
    Kalender(),
  ];

  void _ontap(int index){
    setState(() {
      selectmenu=index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        backgroundColor: fbiru1,
        onPressed: (){

        },
        child: Icon(Icons.add_circle),
      ),
      appBar: AppBar(
        backgroundColor: fbiru1,
        title: Text("Produk Hukum"),
        leading: InkWell(
          onTap: (){Navigator.pop(context);},
          child: Icon(Icons.arrow_back),
        ),
      ),
      body: _page.elementAt(selectmenu),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            backgroundColor: fbiru1,
            icon: Icon(Icons.add_box),
            title: Text("Pengajuan"),
          ),
          BottomNavigationBarItem(
            backgroundColor: fbiru1,
            icon: Icon(Icons.chrome_reader_mode),
            title: Text("Produk"),
          ),
          BottomNavigationBarItem(
            backgroundColor: fbiru1,
            icon: Icon(Icons.calendar_today),
            title: Text("Kalender"),
          ),
        ],
        type: BottomNavigationBarType.fixed,
        currentIndex: selectmenu,
        onTap: _ontap,
      ),
      
    );
  }
}