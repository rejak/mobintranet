import 'package:flutter/material.dart';
import 'package:mobintranet/page/absen/log.dart';

class Absen extends StatefulWidget {

  final String tanggal;
  final String jamdatang;
  final String jampulang;

  const Absen({Key key, this.tanggal, this.jamdatang, this.jampulang}) : super(key: key);

  @override
  _AbsenState createState() => _AbsenState();
}

class _AbsenState extends State<Absen> {
  @override
  Widget build(BuildContext context) {
   return Scaffold(
      appBar: AppBar(
        leading: InkWell(
          onTap: (){
            Navigator.pop(context);
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(Icons.arrow_back),
            ],
          ),
        ),
        title: Text("Absensi"),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 20,top: 10),
            child: InkWell(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=> LogAbsen()));
              },
              child: Row(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Icon(Icons.timer),
                      Text("Log ",style: TextStyle(fontSize: 10),),
                    ],
                  ),
                  
                ],
              ),
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              width: double.infinity,
              height: 100,
              child: Card(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.person_outline, size: 40,color: Colors.blue,),
                    Text(widget.tanggal,style: TextStyle(fontWeight: FontWeight.normal, fontSize: 20),),
                  ],
                ), 
              ),
            ),
            Container(
              width: double.infinity,
              height: 100,
              child: Card(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text("Jam mulai kerja",style: TextStyle(fontSize: 16),),
                        Text("${widget.jamdatang} WIB",style: TextStyle(fontSize: 25)),
                      ],
                    ),
                    VerticalDivider(color: Colors.grey[250],thickness: 1),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text("Jam Pulang kerja",style: TextStyle(fontSize: 16)),
                        Text("${widget.jampulang}",style: TextStyle(fontSize: 25)),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: 100,
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 3,
                    child: Card(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text("Sakit",style: TextStyle(fontSize: 16),),
                          Divider(color: Colors.grey[200],thickness: 1,),
                          Text("-",style: TextStyle(fontSize: 35,color: Colors.blue),),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Card(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text("BAK",style: TextStyle(fontSize: 16),),
                          Divider(color: Colors.grey[200],thickness: 1,),
                          Text("-",style: TextStyle(fontSize: 35,color: Colors.blue),),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Card(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text("Dinas",style: TextStyle(fontSize: 16),),
                          Divider(color: Colors.grey[200],thickness: 1,),
                          Text("-",style: TextStyle(fontSize: 35,color: Colors.blue),),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),

          ],
        ),
      ),
    );
  }
}