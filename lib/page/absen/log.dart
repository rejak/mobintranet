
import 'package:flutter/material.dart';
import 'package:mobintranet/api/manageapi.dart';
import 'package:mobintranet/bloc/main_bloc.dart';
import 'package:mobintranet/model/absen.dart';
import 'package:mobintranet/widget/datetime.dart';

class LogAbsen extends StatefulWidget {
  @override
  _LogAbsenState createState() => _LogAbsenState();
}

DateTime date = DateTime.now();


class _LogAbsenState extends State<LogAbsen> {
  String bulan="Januari";
  
  int year = date.year;
  int month=1;
  List<Absen> list;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    API.getabsenbytahun(mainBloc.tokenapi, year).then((res){
      list=res;
    });
  }

   

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      
      body: Column(
        children: <Widget>[
          SizedBox(height: 20,),
          Expanded(
            flex: 1,
            child:  tabfilter(),
          ),
          Expanded(
            flex: 4,
            child:
              Container(
                width: double.infinity,
                height: double.infinity,
                child: FutureBuilder(
                        future: API.getabsenbytahun(mainBloc.tokenapi, year),
                        builder: (context,res){
                          return list!=null?
                          listabsen(list,month):Center(child: CircularProgressIndicator(),);
                        },
                      ),
              ),
          ),
        ],
      ), 
    );
  }

Widget listabsen(List<Absen> absen , int bulan){
 
  return ListView.builder(
    itemCount: absen.length,
    itemBuilder: (context,posisi){
      return absen[posisi].tglmasuk!=null && bulan == splitbulan(absen[posisi].tglmasuk, posisi)?
       Container(
        height: 80,
        child: Card(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    flex: 3,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(Tanggal().tglabsen(absen[posisi].tglmasuk),style: TextStyle(fontSize: 16),),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Icon(Icons.timer,size: 12,),
                                Text("Masuk : "+absen[posisi].jammasuk.substring(0,5),style: TextStyle(fontSize: 12),),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Icon(Icons.timer,size: 12,),
                                Text("Keluar : "+absen[posisi].jamkeluar.substring(0,5),style: TextStyle(fontSize: 12),),
                              ],
                            ),
                          ],
                        ),
                        
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(height: 5,),
            Text("Keterangan : Hadir",style: TextStyle(fontWeight: FontWeight.bold),),
            ],
          ),
        ),
      ):
      Container();
    },
  );
  
  
}
int splitbulan(String split,int posisi){
  // String split=absen[posisi].tglmasuk;
  var bulan= split.split('-');
  
  return int.parse(bulan[1]);
}
Widget pilihtahun(){
  return Container(
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text('${year}',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold,color: Colors.white),),
        InkWell(
          onTap: (){
            setState(() {
              year=year+1;
              API.getabsenbytahun(mainBloc.tokenapi, year).then((res){
                list=res;
              });
            });
          },
          child: Icon(Icons.keyboard_arrow_up,color: Colors.white,)),
        InkWell(
          onTap: (){
            setState(() {
              year=year-1;
              API.getabsenbytahun(mainBloc.tokenapi, year).then((res){
                list=res;
              });
            });
          },
          child: Icon(Icons.keyboard_arrow_down,color: Colors.white,))
      ],
    ),
  );
}

Widget tabfilter(){
  return Container(
        child: Column(
          children: <Widget>[
            SizedBox(height: 5,),
            Container(
              padding: EdgeInsets.all(5),
              child: 
                Stack(
                  children: <Widget>[
                    Positioned(
                      left: 20,
                      top: 1,
                      child: InkWell(
                        onTap: (){
                          Navigator.pop(context);
                        },
                        child: Icon(Icons.arrow_back,size: 30,color: Colors.white,)),
                    ),
                    Column(
                    children: <Widget>[
                      Text(bulan+' ${year}',style: TextStyle(color: Colors.white,fontSize: 25),),
                      Padding(
                        padding: EdgeInsets.only(left: 10,right: 10),
                        child: Divider(
                          color: Colors.grey[200],
                          thickness: 2,
                        ),
                      ),
                      pilihtahun(),
                      SizedBox(height: 10,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          tombolbulan(1),
                          tombolbulan(2),
                          tombolbulan(3),
                          tombolbulan(4),
                          tombolbulan(5),
                          tombolbulan(6),
                          tombolbulan(7),
                          tombolbulan(8),
                          tombolbulan(9),
                          tombolbulan(10),
                          tombolbulan(11),
                          tombolbulan(12),
                        ],
                      ),
                      SizedBox(height: 10,),
                    ],
              ),
                  ],
                ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(topLeft: Radius.circular(10),topRight: Radius.circular(10)),
                color: Colors.pink
              ),
            ),
          ],
        ),
      );
}

Widget tombolbulan(int angka){
  return InkWell(
            onTap: (){
              setState(() {
                if(angka==1){month=1;bulan="Januari";}
                if(angka==2){month=2;bulan="Febuari";}
                if(angka==3){month=3;bulan="Maret";}
                if(angka==4){month=4;bulan="April";}
                if(angka==5){month=5;bulan="Mei";}
                if(angka==6){month=6;bulan="Juni";}
                if(angka==7){month=7;bulan="Juli";}
                if(angka==8){month=8;bulan="Agustus";}
                if(angka==9){month=9;bulan="September";}
                if(angka==10){month=10;bulan="Oktober";}
                if(angka==11){month=11;bulan="November";}
                if(angka==12){month=12;bulan="Desember";}
              });
              API.getabsenbytahun(mainBloc.tokenapi, year).then((res){
                list=res;
              });
              print('bln $angka');
            },
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                Text("$angka",style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold),),
              ],) ,
              constraints: BoxConstraints(
                minHeight: 25,
                minWidth: 25,
              ),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.white
              ),
            ),
          );
}
}
