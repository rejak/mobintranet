
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mobintranet/Loader.dart';
import 'package:mobintranet/model/user.dart';
import 'package:mobintranet/util/DbColors.dart';
import 'package:mobintranet/util/DbImages.dart';
import 'package:mobintranet/util/colors.dart';
import 'package:mobintranet/util/Style.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:mobintranet/login.dart';
import 'package:mobintranet/bloc/main_bloc.dart';
import 'package:mobintranet/api/manageapi.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:mobintranet/page/p_gantipassword.dart';
import 'dart:convert';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:easy_localization/easy_localization.dart';
// import 'SDEditProfileScreen.dart';

class Profil extends StatefulWidget {
  final String token;
  final String kode;

  const Profil({Key key, this.token, this.kode}) : super(key: key);
  @override
  _ProfilState createState() => _ProfilState();
}

class _ProfilState extends State<Profil> {
  bool _loading = true;
  String nama, nip, jabatan, unit, gol, bidang, tmtgol, tmtjab, tmtunit;
  void _ambiluser() async{
    final prefs = await SharedPreferences.getInstance();
    var getDataProfil = jsonDecode(prefs.getString('dataProfil'));
    var isi = User.fromJson(getDataProfil);
    print("sini");
    print(isi.golonganRuang);
    // print(isi2.eselon);
    // print(isi2.grading);
    // print(isi2.kode);
    // print(isi2.unit);
    // print(isi2.unitkerja);
    // print(isi2.wilayah);
    // print(User.fromJson(isi));
    setState(() {
        nama = isi.nama;
        nip = isi.nip;
        jabatan = isi.jabatan;
        unit = isi.perEselonDua;
        gol = isi.golonganRuang;
        bidang = isi.unitKerja;
        tmtgol = isi.tmtGolongan;
        tmtjab = isi.tmtJabatan;
        tmtunit = isi.tmtUnit;
        _loading = false;
      });
    // API.getUser(mainBloc.tokenapi).then((res) {
    //   // print(res.body);
    //   var isi = jsonDecode(res.body);
    //   print(isi);
    //   setState(() {
    //     nama = isi['nama'];
    //     nip = isi['nip'];
    //     jabatan = isi['jabatan'];
    //     unit = isi['perEselonDua'];
    //     gol = isi['golonganRuang'];
    //     bidang = isi['unitKerja'];
    //     tmtgol = isi['tmtGolongan'];
    //     tmtjab = isi['tmtJabatan'];
    //     tmtunit = isi['tmtUnit'];
    //     _loading = false;
    //   });

    //   print(isi);
    // });
  }

  void setpref() async {
    final prefs = await SharedPreferences.getInstance();
    bool statuslogin = false;
    prefs.setString('isitoken', '');
    prefs.setBool('login', statuslogin);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _ambiluser();
    _taskPieData = List<charts.Series<Task, String>>();
    _generateData();
  }

  List<charts.Series<Task, String>> _taskPieData;

  _generateData() {
    var taskData = [
      Task(task: 'Completed', value: 82.0, color: Colors.blue),
      Task(task: 'On going', value: 22.0, color: Colors.deepOrangeAccent),
    ];

    _taskPieData.add(
      charts.Series(
        data: taskData,
        domainFn: (Task task, _) => task.task,
        measureFn: (Task task, _) => task.value,
        colorFn: (Task task, _) => charts.ColorUtil.fromDartColor(task.color),
        id: 'Montlhy',
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    changeStatusColor(fbiru1);
    Size size = MediaQuery.of(context).size;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text(tr('profile.title')),
        backgroundColor: fbiru1,
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(Icons.arrow_back),
        ),
        elevation: 0,
      ),
      body: _loading? Loader(): SingleChildScrollView(
        physics: ScrollPhysics(),
        child: Container(
          child: Stack(
            children: <Widget>[
              Container(
                height: 250,
                width: size.width,
                color: fbiru1,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                      ),
                      height: 80,
                      width: 80,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(40),
                        child: Icon(
                          Icons.people,
                          color: Colors.white,
                          size: 50,
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 20),
                      child: Text(
                        '${nama}',
                        style: boldTextStyle(textColor: Colors.white),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      child: Text(
                        '${jabatan}',
                        style: secondaryTextStyle(
                          size: 12,
                          textColor: Colors.white.withOpacity(0.7),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      child: Text(
                        '${unit}',
                        style: secondaryTextStyle(
                          size: 12,
                          textColor: Colors.white.withOpacity(0.7),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Container(
                        height: 130,
                        margin: EdgeInsets.only(
                          top: 250.00 - 50,
                        ),
                        padding: EdgeInsets.all(20),
                        decoration: boxDecoration(
                          radius: 8,
                          backGroundColor: Colors.white,
                          spreadRadius: 2,
                          blurRadius: 10,
                        ),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Center(
                              child: InkWell(
                                onTap: () {
                                  Navigator.push(context, MaterialPageRoute(builder: (context)=> ChangePass()));
                                },
                                child: Icon(
                                  Icons.security,
                                  color: Colors.yellow,
                                  size: 50,
                                ),
                              ),
                            ),
                            Center(child: Text(tr('profile.change_password')))
                          ],
                        ),
                      ),
                    ),
                    // SizedBox(
                    //   width: 15,
                    // ),
                    // Expanded(
                    //   flex: 1,
                    //   child: Container(
                    //     height: 130,
                    //     margin: EdgeInsets.only(
                    //       top: 250.00 - 50,
                    //     ),
                    //     padding: EdgeInsets.all(20),
                    //     decoration: boxDecoration(
                    //       radius: 8,
                    //       backGroundColor: Colors.white,
                    //       spreadRadius: 2,
                    //       blurRadius: 10,
                    //     ),
                    //     child: Column(
                    //       mainAxisSize: MainAxisSize.min,
                    //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //       crossAxisAlignment: CrossAxisAlignment.center,
                    //       children: <Widget>[
                    //         (Localizations.localeOf(context).languageCode ==
                    //                   "id")
                    //               ? GestureDetector(
                    //                   onTap: () {
                    //                     this.setState(() {
                    //                       context.setLocale(Locale("en", "US"));
                    //                       print(Localizations.localeOf(context)
                    //                           .languageCode);
                    //                     });
                    //                   },
                    //                   child: Row(
                    //                     mainAxisAlignment: MainAxisAlignment.center,
                    //                     children: [
                    //                       SvgPicture.asset(ind,
                    //                           width: 50, height: 50),
                    //                       // SizedBox(
                    //                       //   width: 5,
                    //                       // ),
                    //                       // Text(Localizations.localeOf(context).countryCode, style: TextStyle(color: Colors.white),)
                    //                     ],
                    //                   ),
                    //                 )
                    //               : GestureDetector(
                    //                   onTap: () {
                    //                     print("tess");
                    //                     this.setState(() {
                    //                       context.setLocale(Locale("id", "ID"));
                    //                       print(Localizations.localeOf(context)
                    //                           .languageCode);
                    //                     });
                    //                   },
                    //                   child: Row(
                    //                     mainAxisAlignment: MainAxisAlignment.center,
                    //                     children: [
                    //                       SvgPicture.asset(eng,
                    //                           width: 50, height: 50),
                    //                       // SizedBox(
                    //                       //   width: 5,
                    //                       // ),
                    //                       // Text(Localizations.localeOf(context).countryCode,style: TextStyle(color: Colors.white))
                    //                     ],
                    //                   ),
                    //                 ),
                    //                 Center(child: Text(Localizations.localeOf(context).countryCode, style: TextStyle(color: Colors.black),))
                    //       ],
                    //     ),
                    //   ),
                    // ),
                    SizedBox(
                      width: 15,
                    ),
                    Expanded(
                      flex: 1,
                      child: Container(
                        height: 130,
                        margin: EdgeInsets.only(
                          top: 250.00 - 50,
                        ),
                        padding: EdgeInsets.all(20),
                        decoration: boxDecoration(
                            radius: 8,
                            backGroundColor: Colors.white,
                            spreadRadius: 2,
                            blurRadius: 10),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Center(
                              child: InkWell(
                                onTap: () {
                                  setpref();
                                  Navigator.of(context).pushNamedAndRemoveUntil(
                                      '/login',
                                      (Route<dynamic> route) => false);
                                },
                                child: Icon(
                                  Icons.exit_to_app,
                                  color: Colors.red,
                                  size: 50,
                                ),
                              ),
                            ),
                            Center(child: Text(tr('profile.logout')))
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Center(
                child: Container(
                  margin: EdgeInsets.only(
                      top: 250.00 + 100, bottom: 25, left: 16, right: 16),
                  padding: EdgeInsets.only(
                    top: 15,
                    left: 15,
                    right: 15,
                    bottom: 15
                  ),
                  decoration: boxDecorations(
                    showShadow: true,
                  ),
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: Text(tr('profile.personal_information.title'),
                                style: boldTextStyle(size: 16)),
                          ),
                        ],
                      ),
                      SizedBox(height: 15,),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              child: Stack(
                                children: [
                                  Column(
                                    children: [
                                      Container(
                                        child: Row(
                                          children: [
                                            Expanded(flex: 2,child: Text(tr('profile.personal_information.full_name'))),
                                            Expanded(flex: 4,child: Text(": ${nama}")),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        child: Row(
                                          children: [
                                            Expanded(flex: 2,child: Text(tr('profile.personal_information.num_nip'))),
                                            Expanded(flex: 4,child: Text(": ${nip}")),
                                          ],
                                        ),
                                      ),Container(
                                        child: Row(
                                          children: [
                                            Expanded(flex: 2,child: Text(tr('profile.personal_information.class'))),
                                            Expanded(flex: 4,child: Text(": ${gol}")),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        child: Row(
                                          children: [
                                            Expanded(flex: 2,child: Text(tr('profile.personal_information.position'))),
                                            Expanded(flex: 4,child: Text(": ${jabatan}")),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        child: Row(
                                          children: [
                                            Expanded(flex: 2,child: Text(tr('profile.personal_information.sector'))),
                                            Expanded(flex: 4,child: Text(": ${bidang}")),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        child: Row(
                                          children: [
                                            Expanded(flex: 2,child: Text(tr('profile.personal_information.the_work_unit'))),
                                            Expanded(flex: 4,child: Text(": ${unit}")),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        child: Row(
                                          children: [
                                            Expanded(flex: 2,child: Text(tr('profile.personal_information.tmt_class'))),
                                            Expanded(flex: 4,child: Text(": ${tmtgol}")),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        child: Row(
                                          children: [
                                            Expanded(flex: 2,child: Text(tr('profile.personal_information.tmt_position'))),
                                            Expanded(flex: 4,child: Text(": ${tmtjab}")),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        child: Row(
                                          children: [
                                            Expanded(flex: 2,child: Text(tr('profile.personal_information.tmt_position'))),
                                            Expanded(flex: 4,child: Text(": ${tmtunit}")),
                                          ],
                                        ),
                                      ),
                                      SizedBox(height: 15,),
                                    ],
                                  )
                                ],
                              )
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class Task {
  String task;
  double value;
  Color color;

  Task({this.task, this.value, this.color});
}
