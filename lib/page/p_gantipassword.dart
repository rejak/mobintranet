import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:mobintranet/api/manageapi.dart';
import 'package:mobintranet/bloc/main_bloc.dart';
import 'package:mobintranet/model/user.dart';

class ChangePass extends StatefulWidget {
  @override
  _ChangePassState createState() => _ChangePassState();
}


class _ChangePassState extends State<ChangePass> {

  final TextEditingController password = TextEditingController();
final TextEditingController confirmpass = TextEditingController();
// final TextEditingController _passC = TextEditingController();
String username;

bool disabled=true;
@override
  void initState() {
    // TODO: implement initState
    super.initState();
    API.getUser(mainBloc.tokenapi).then((res){
      Map<String,dynamic> user = jsonDecode(res.body);
        var usr= User.fromJson(user);
        User userr=usr;
        username= userr.username;
        print(userr.username);
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Ganti Password "),
      ),
      body: Container(
        child: Card(
          child: Column(
            children: <Widget>[
              TextFormField(
                controller: password,
                obscureText: true,
                decoration: InputDecoration(
                  hintText: 'Masukan Password',
                  labelText: 'Password'
                ),
              ),
              TextFormField(
                onChanged: (isi){
                  print(isi);
                  setState(() {
                    if(password.text!=isi){
                      disabled=true;
                      print(disabled);
                    }else{disabled=false;}
                  });
                },
                obscureText: true,
                controller: confirmpass,
                decoration: InputDecoration(
                  hintText: 'Masukan konformasi Password',
                  labelText: 'Konfirmasi Password'
                ),
              ),
              SizedBox(height: 10,),

              (disabled)?
              Text("Password tidak sama",style: TextStyle(color: Colors.red),)
              :Container(),
              FlatButton(
                textColor: Colors.white,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("Kirim")
                  ],
                ),
                color: Colors.blue,
                onPressed: disabled? null: (){
                  print(password.text);
                  print(mainBloc.user);
                  API.changepass(mainBloc.tokenapi, username, password.text, confirmpass.text).then((res){
                    Navigator.pop(context);
                  });
                } )
            ],
            
          ),
        ),
      ),
    );
  }
}