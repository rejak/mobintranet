import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:http/http.dart';
import 'package:http/io_client.dart';
import 'package:mobintranet/api/manageapi.dart';

import 'package:mobintranet/function/alertdialog.dart';
import 'package:mobintranet/mainpage.dart';
import 'package:mobintranet/model/loginModel.dart';
import 'package:mobintranet/bloc/blocdata.dart';
import 'package:mobintranet/function/saveCurrentLogin.dart';
import 'package:mobintranet/model/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<LoginModel> requestLoginAPI(BuildContext context, String username, String password,status) async  {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  final url = "https://intranet.lan.go.id:8444/oauth/token";
  bool _saving=false;
  Map<String, String> body = {
    'username': username,
    'password': password,
    'grant_type':'password',
  };

  String usrnm = 'cliente';
  String pass = 'password';
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$usrnm:$pass'));
  print(basicAuth);
  print(username);
  print(password);

HttpClient client = new HttpClient();
client.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);
IOClient ioClient = new IOClient(client);

  Response response = await ioClient.post(Uri.parse(url),headers: {'Authorization': basicAuth},body: body,);
  try {
    print(response.statusCode);
  print(response.body);
  Map<String,dynamic> respon =jsonDecode(response.body);
  //print('isi token:,${respon['access_token']}');
  blocdata = BlocData();
  User data = await API.getUser2Coba(respon['access_token']);
  print(data.golonganRuang);
  print("cek gol");
  print(jsonEncode(data));
  var encodeData = jsonEncode(data);
       preferences.setString('dataProfil',encodeData.toString());
       var coba = jsonDecode(preferences.getString("dataProfil"));
       print(User.fromJson(coba).toJson());
  // API.getUser(respon['access_token']).then((res){
      //  var isi= jsonDecode(res.body);
        API.getStswork(data.nip).then((res){
          print(res.body);
          var i= jsonDecode(res.body);
          var data=i[0];
          print(data['STATUS_HADIR']);
          blocdata.setstatuswork(data['STATUS_HADIR']);
        });
    // });
  API.getlaporan(respon['access_token']).then((res){
        DateTime masuk;
        DateTime keluar;
        // print(res.absenMasuk);
        // print(res.absenPulang);
        print("cek absen");
        if(res.absenMasuk!=null){
          masuk=DateTime.parse(res.absenMasuk).toLocal();
        blocdata.setjammasuk("${masuk.hour}:${masuk.minute}:${masuk.second}"); }
        else{
          blocdata.setjammasuk("-");
        }

        if(res.absenPulang!=null){
          keluar=DateTime.parse(res.absenPulang).toLocal();
        blocdata.setjamkeluar("${keluar.hour}:${keluar.minute}:${keluar.second}"); }
        else{
          blocdata.setjamkeluar("-");
        }
        print("masukkk");
        print(blocdata.jammasuk);
    });

  // showDialog(
  //   context: context,
  //   barrierDismissible: false,
  //   builder: (BuildContext context) => new Dialog(
  //     child: Container(
  //       width: 300,
  //       height: 80,
  //       padding: EdgeInsets.only(left: 20),
  //       child: new Row(
  //         children: [
  //           new CircularProgressIndicator(),
  //           SizedBox(width: 20),
  //           Text("Proses Login"),
  //         ],
  //       ),
  //     ),
  //   ),
  // );
  if (response.statusCode == 200) {
    //blocdata.setusername(username);
    http.Response data = await ioClient.get(Uri.parse('https://103.206.244.107:8445/employee'),
    headers: {'Authorization': 'Bearer '+respon['access_token']});
    var isidata =jsonDecode(data.body);
    User isiDataModel = User.fromJson(isidata);
    print("mybody");
    print(data.body);
    print(isiDataModel.jk);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('jenisKelamin', isiDataModel.jk.toString());
    await prefs.setString('myName', isiDataModel.nama.toString());
    //if (isidata['grading']=='6') {
      
      final responseJson = json.decode(response.body);
      saveCurrentLogin(responseJson);
    //   new Future.delayed(new Duration(seconds: 3), () {
    //   Navigator.pop(context); //pop dialog
    //   Navigator.of(context).pushReplacement(
    //     new MaterialPageRoute(
    //       settings: const RouteSettings(
    //         name: '/mainpage'),
    //         builder: (context)=> new MainPage(token: '${respon['access_token']}',kode: '${username}',),
    //     ),
    //   );
    // });
      print("tes model");
      print(LoginModel.fromJson(responseJson));
      return LoginModel(username,respon['access_token'],"","");
    //} 
    // else if (isidata['grading']=='7') {
    //   final responseJson = json.decode(response.body);
    //   //print(responseJson);
    //   saveCurrentLogin(responseJson);
    //   Navigator.of(context).pushReplacementNamed('/mainpagekepala');
    //   return LoginModel.fromJson(responseJson);
    // }
    // else{}
  }
   else {
    final responseJson = json.decode(response.body);
    saveCurrentLogin(responseJson);
    
    new Future.delayed(new Duration(seconds: 1), () {
    Navigator.pop(context); //pop dialog
    showDialogSingleButton(context, "Login Gagal", "Username atau Password yang anda masukan salah, Silahkan Coba Lagi.", "Coba Lagi");
  });
    return null;
  }
  } catch (e) {
    return throw Exception("Data tidak ditemukan");
  }
  

}
