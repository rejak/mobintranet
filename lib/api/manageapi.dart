import 'dart:convert';
import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'dart:io';
import 'package:http/http.dart';
import 'package:http/http.dart' as http;
import 'package:http/io_client.dart';
import 'package:mobintranet/model/Surat.dart';
import 'package:mobintranet/model/absen.dart';
import 'package:mobintranet/model/pegawai.dart';
import 'package:mobintranet/model/pengajuan_hukum.dart';
import 'package:mobintranet/model/produk_hukum.dart';
import 'package:mobintranet/model/absenwfh.dart';
import 'package:intl/intl.dart';
import 'package:mobintranet/model/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

const urlpeg='https://intranet.lan.go.id:8445/employee';
const urlabsen = 'https://intranet.lan.go.id:8445/employee/jam';
const urlinbox = 'https://intranet.lan.go.id:8446/surat/inbox';
const urloutbox = 'https://intranet.lan.go.id:8446/surat/outbox';
const urlproduk = 'https://intranet.lan.go.id:8446/hukum/daftarhukum';
const urlpengajuanhukum = 'https://intranet.lan.go.id:8446/hukum/pengajuan';
const urlpegawai = 'https://intranet.lan.go.id:8445/employee/ambilsemua';
//laporan
const urllaporan = 'https://intranet.lan.go.id:8446/laporan';
// const urlsimpan = 'https://intranet.lan.go.id:8446/laporan/simpan';
// const urlpulang = 'https://intranet.lan.go.id:8446/laporan/checkout';

class API{
  //umum
  static Future<bool> cektoken(token) async{
    print("celll");
     HttpClient client = new HttpClient();
client.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);
IOClient ioClient = new IOClient(client);

    var res= await ioClient.get(Uri.parse(urlpeg),headers: {'Authorization': 'Bearer '+token});
    print(res.statusCode);
    print("cek token");
    if(res.statusCode==200){
      return true;
    }else{
      return false;
    }
  }

  //home 
   static Future getUser(token) async{ 
     SharedPreferences preferences = await SharedPreferences.getInstance();
    HttpClient client = new HttpClient();
    client.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);

    final res = ioClient.get(Uri.parse(urlpeg),headers: {'Authorization': 'Bearer '+token});
     print("cek akun");
     print(res);
      //  var isi = jsonDecode(res);
      //  User finalIsi = User.fromJson(isi);
      //  preferences.setString('dataProfil',finalIsi.toString());
      //  print("cek local storage");
      //  var dataProfil = preferences.getString('dataProfil');
      //  print(dataProfil);
      //  print("cek local storage");
      //  print(finalIsi);
       return res;
  }

  static Future<User> getUser2Coba(token) async { 
    HttpClient client = new HttpClient();
    client.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);

    var isi = await ioClient.get(Uri.parse(urlpeg),headers: {'Authorization': 'Bearer '+token});
    var isi2 = json.decode(isi.body);
    User finalIsi = User.fromJson(isi2);
    return finalIsi;
  }

   static Future getStswork(nip) { 
    HttpClient client = new HttpClient();
    client.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    //current date
      DateTime now = DateTime.now();
      DateFormat formatter = DateFormat('MM/dd/yyyy');
      String formatted = formatter.format(now);
      print(formatted); // something like 2013-04-20
      
    return ioClient.get(Uri.parse("http://absen.lan.go.id/statushadir.php?nip="+nip+"&tgl="+formatted));
  }

  static Future getabsen(token){
    HttpClient client = new HttpClient();
    client.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);

    return ioClient.get(Uri.parse(urlabsen),headers: {'Authorization':'Bearer '+token});
  }

  static Future<List<Absen>> getabsenbytahun(token,tahun) async{
    List<Absen> list=[];
    HttpClient client = new HttpClient();
    client.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);

    var res= await ioClient.get(Uri.parse(urlabsen+"/${tahun}"),headers: {'Authorization':'Bearer '+token});

    if(res.statusCode==200){
      var data = jsonDecode(res.body);
      var resp= data as List;
      list= resp.map<Absen>((json)=> Absen.fromjson(json)).toList();
    }
    return list;
  }

  //surat
  static Future<List<Surat>> getinbox(token) async{

    List<Surat> list=[];
    HttpClient client = new HttpClient();
    client.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    var res = await ioClient.get(Uri.parse(urlinbox),headers: {'Authorization':'Bearer '+token});

    if(res.statusCode==200){
      var data = jsonDecode(res.body);
      var resp = data as List;
      list = resp.map<Surat>((json)=>Surat.fromJson(json)).toList();
    }
    return list;
  }

  static Future<List<Surat>> getoutbox(token) async{

    List<Surat> list=[];
    HttpClient client = new HttpClient();
    client.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    var res = await ioClient.get(Uri.parse(urloutbox),headers: {'Authorization':'Bearer '+token});

    if(res.statusCode==200){
      var data = jsonDecode(res.body);
      var resp = data as List;
      list = resp.map<Surat>((json)=>Surat.fromJson(json)).toList();
    }
    return list;
  }

  //end surat

  //hukum
  static Future<List<ProdukHukum>> getProdukHukum(token) async{
    List<ProdukHukum> list=[];
    HttpClient client = new HttpClient();
    client.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    var res = await ioClient.get(Uri.parse(urlproduk),headers: {'Authorization':'Bearer '+token});

    if(res.statusCode==200){
      var data= jsonDecode(res.body);
      var resp= data as List;
      list= resp.map<ProdukHukum>((json) => ProdukHukum.fromjson(json)).toList();
    }
    return list;
  }

  static Future<List<Pengajuan_hukum>> getpengajuanHukum(token)async{
    List<Pengajuan_hukum> list=[];
    HttpClient client = new HttpClient();
    client.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    var res = await ioClient.get(Uri.parse(urlpengajuanhukum),headers: {'Authorization':'Bearer '+token});

    if(res.statusCode==200){
      var data= jsonDecode(res.body);
      var resp= data as List;
      list= resp.map<Pengajuan_hukum>((json) => Pengajuan_hukum.fromjson(json)).toList();
    }

    return list;
  }

  //sdm
  static Future<List<dynamic>> getallpegawai(token) async{
    List<Pegawai> list=[];
    var respon;
    HttpClient client = new HttpClient();
    client.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    var res = await ioClient.get(Uri.parse(urlpegawai),headers: {'Authorization':'Bearer '+token});

    if(res.statusCode==200){
      var data= jsonDecode(res.body);
      print(data);
      print("cek di api 1");
      var resp= data as List;
      print(resp);
      respon=resp;
      print("cek di api 2");
      list= resp.map<Pegawai>((json) => Pegawai.fromJson(json)).toList();
      print(list);
    }

    return respon;

  }

   static Future<List<dynamic>> getmultipegawai(token) async{
    List<dynamic> list;
    HttpClient client = new HttpClient();
    client.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    var res = await ioClient.get(Uri.parse(urlpegawai),headers: {'Authorization':'Bearer '+token});

    if(res.statusCode==200){
      var data= jsonDecode(res.body);
      list= data as List;
    }

    return list;

  }
  //end sdm

  //absen wfh
  static Future<AbsenMwfh> getlaporan(token) async{
    print("cek status wfh");
    HttpClient client = new HttpClient();
    client.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    var res = await ioClient.get(Uri.parse(urllaporan),headers: {'Authorization':'Bearer '+token});

    var data= jsonDecode(res.body.toString());
    return AbsenMwfh.fromjson(data);

  }

  static Future<List<AbsenMwfh>> getalllaporan(token) async{
    List<dynamic> list =[];
    List<AbsenMwfh> listwfh =[];
    HttpClient client = new HttpClient();
    client.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    var res = await ioClient.get(Uri.parse(urllaporan+'/daftar'),headers: {'Authorization':'Bearer '+token});
    
     if(res.statusCode==200){
       
       var data= jsonDecode(res.body);
      list= data as List;
      list.forEach((f){
        AbsenMwfh isi= AbsenMwfh.fromjson(f);
        listwfh.add(isi);
      });
      print('${listwfh}');
    }

    return listwfh;
  }


  static Future<AbsenMwfh> initlaporan(token)async{
    List<AbsenMwfh> list=[];
    HttpClient client = new HttpClient();
    client.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    var res = await ioClient.post(Uri.parse(urllaporan),headers: {'Authorization':'Bearer '+token});
    var data= jsonDecode(res.body);
    return AbsenMwfh.fromjson(data);
  }

  static Future<dynamic> simpanlaporan(token,kondisi,kegiatan,output,progres)async{
    Dio dio = new Dio();
    
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate = (HttpClient client) {
      client.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      return client;
    };

    var isi = {
          "kondisi" : kondisi,
            "kegiatan" : kegiatan,
            "output" : output,
            "progres" : progres
        };
  print(isi);
  var res= await dio.post(urllaporan+'/simpandata',
    data:isi,
    options: Options(
      headers: {
        "Authorization":"Bearer "+token
      }
    ));
  //  var data = jsonDecode(res.data.toString());
   return res.data.toString();
  }

  static Future<dynamic> cekout(token)async{
    Dio dio = new Dio();

     (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate = (HttpClient client) {
      client.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      return client;
    };

    var res = await dio.post(urllaporan+'/checkout',
      options: Options(
        headers: {
          "Authorization":"Bearer "+token
        }
      )).catchError((err){return err;});
    // var data = jsonDecode(res.data);
    return res.data.toString();
  }

  static Future<dynamic> changepass(token,username,pass,confirmpass)async{
    Dio dio = new Dio();

     (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate = (HttpClient client) {
      client.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      return client;
    };

    var res = await dio.post(urlpeg+'/gantipassword',
      data: {
        "username" : username,
        "password" : pass,
        "konfirmasiPassword" : confirmpass
      },
      options: Options(
        headers: {
          "Authorization":"Bearer "+token
        }
      ));
    print(res.data.toString());
   // var data = jsonDecode(res.data);
    return res.data.toString();
  }

  static Future<dynamic> cekingps(token,long,lat,loc)async{
    Dio dio = new Dio();

     (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate = (HttpClient client) {
      client.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      return client;
    };

    var res = await dio.post(urllaporan+'/checkingps2',
      data: {
        "longitude" : long,
        "latitude" : lat,
        "lokasi" : loc
      },
      options: Options(
        headers: {
          "Authorization":"Bearer "+token
        }
      ));
    print(res.data.toString());
   // var data = jsonDecode(res.data);
    return res.data.toString();
  }

  static Future<dynamic> cekoutgps(token,long,lat,loc)async{
    Dio dio = new Dio();

     (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate = (HttpClient client) {
      client.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      return client;
    };

    var res = await dio.post(urllaporan+'/checkoutgps2',
      data: {
        "longitude" : long,
        "latitude" : lat,
        "lokasi" : loc
      },
      options: Options(
        headers: {
          "Authorization":"Bearer "+token
        }
      ));
    //var data = jsonDecode(res.data);
    return res.data.toString();
  }

   static Future<dynamic> ceksiang(token,long,lat,loc)async{
    Dio dio = new Dio();

     (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate = (HttpClient client) {
      client.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      return client;
    };

    var res = await dio.post(urllaporan+'/checkoutgps2',
      data: {
        "longitude" : long,
        "latitude" : lat,
        "lokasi" : loc
      },
      options: Options(
        headers: {
          "Authorization":"Bearer "+token
        }
      ));
    //var data = jsonDecode(res.data);
    return res.data.toString();
  }


  
  //end absen wfh

  


}