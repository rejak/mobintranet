import 'dart:convert';
import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'dart:io';
// import 'package:http/http.dart';
import 'package:http/http.dart' as client;
import 'package:ext_storage/ext_storage.dart';
import 'package:http/io_client.dart';
import 'package:mobintranet/model/Surat.dart';
import 'package:mobintranet/model/absen.dart';
import 'package:mobintranet/model/contohModel.dart';
import 'package:mobintranet/model/pegawai.dart';
import 'package:mobintranet/model/pengajuan_hukum.dart';
import 'package:mobintranet/model/produk_hukum.dart';
import 'package:mobintranet/model/absenwfh.dart';
import 'package:intl/intl.dart';
import 'package:mobintranet/model/user.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:path/path.dart';
import 'package:async/async.dart';
import 'package:http_parser/http_parser.dart';
import 'package:shared_preferences/shared_preferences.dart';

const _apiPostSurat = "https://intranet.lan.go.id:8446/surat/simpanSuratMasuk";
const _apiDetailSurat = "https://intranet.lan.go.id:8446/surat/inbox/";
const _apiDownloadSurat = "https://intranet.lan.go.id:8446/isisurat/";
const _apiHistorySurat = "https://intranet.lan.go.id:8446/surat/inbox/history/";
const _apiReplyMail = "https://intranet.lan.go.id:8446/surat/inbox/jawaban/";
const _apiPostDisposisi = "https://intranet.lan.go.id:8446/surat/inbox/disposisi/";

class ApiSurat {
  static Future<dynamic> postSurat(
      File filePdf, Surat dataPost, String tokens) async {
    String fileName = basename(filePdf.path);
    String base64Image = base64Encode(filePdf.readAsBytesSync());
    var stream =
        new client.ByteStream(DelegatingStream.typed(filePdf.openRead()));
    var length = await filePdf.length();
    dynamic dataPenerima = jsonEncode(dataPost.penerimaRequest.toList());
    String isi = '{'
        '"klasifikasiSurat": "${dataPost.sifatSurat}",'
        '"derajatSurat": "${dataPost.derajatSurat}",'
        '"nomorSurat": "${dataPost.nomorSurat}",'
        '"rakSurat": "${dataPost.rakSurat}",'
        '"jenisSurat": "${dataPost.jenisSurat}",'
        '"instansiAsal": "${dataPost.instansiAsal}",'
        '"tanggalSurat": "${dataPost.tanggalSurat}",'
        '"perihal": "${dataPost.perihal}",'
        '"tanggalMulai": "${(dataPost.tanggalMulai == null) ? dataPost.tanggalMulai : dataPost.tanggalMulai.toString()}",'
        '"tanggalSelesai": "${(dataPost.tanggalSelesai == null) ? dataPost.tanggalSelesai : dataPost.tanggalSelesai.toString()}",'
        '"lokasi": "${dataPost.lokasi}",'
        '"lampiran": "${dataPost.lampiran}",'
        '"catatan": "${dataPost.catatan}",'
        '"asalSurat": "${dataPost.asalSurat}",'
        '"penerimaRequests": $dataPenerima'
        '}';
        print(isi);
        print("cek isi");
    var request = client.MultipartRequest("POST", Uri.parse(_apiPostSurat));
    request.fields["suratJson"] = isi;
    request.headers.addAll({
      'Content-type': 'multipart/form-data',
      'Accept': '*/*',
      HttpHeaders.authorizationHeader: 'Bearer $tokens'
    });
    var pic = await client.MultipartFile("fileSurat", stream, length,
        filename: fileName,
        contentType: MediaType("multipart/form-data", "multipart/form-data"));
    request.files.add(pic);
    print(request.fields);
    print(request.files);
    client.Response response =
        await client.Response.fromStream(await request.send());
        print(response.body);
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      return "Kirim Surat Gagal";
    }
  }

  static Future<dynamic> getDetailSurat(int idSurat, String tokens) async {
    print(idSurat);
    var response = await client.get(Uri.parse(_apiDetailSurat + "$idSurat"), headers: {
      'Content-type': 'multipart/form-data',
      'Accept': '*/*',
      HttpHeaders.authorizationHeader: 'Bearer $tokens'
    });
    print(response.body);
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      return throw Exception("Data tidak ditemukan");
    }
  }

  static Future<List<dynamic>> getHistorySurat(String tokens, int idSurat) async {
    print(idSurat);
    var response = await client.get(Uri.parse(_apiHistorySurat + "$idSurat"), headers: {
      'Content-type': 'multipart/form-data',
      'Accept': '*/*',
      HttpHeaders.authorizationHeader: 'Bearer $tokens'
    });
    print("cek history");
    print(response.body);
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      return throw Exception("Data tidak ditemukan");
    }
  }

  //api reply mail
  static Future<bool> replyMail(String token, Surat data, File filePdf)async{
    String fileName =  basename(filePdf.path);
    var stream = new client.ByteStream(DelegatingStream.typed(filePdf.openRead()));
    var length = await filePdf.length();
    String isi ='{'
    '"jawabanDisposisi": ""''}';
    var request = client.MultipartRequest("POST", Uri.parse(_apiReplyMail+data.idSurat.toString()));
    request.fields["jawabanJson"]=isi;
    request.headers.addAll({
      'Content-type': 'multipart/form-data',
      'Accept': '*/*',
      HttpHeaders.authorizationHeader: 'Bearer $token'
    });
    var file = await client.MultipartFile("fileSurat",stream, length, filename: fileName, contentType: MediaType("multipart/form-data","multipart/form-data"));
    request.files.add(file);
    client.Response res = await client.Response.fromStream(await request.send());
    if(res.statusCode == 200){
      return true;
    } else {
      return false;
    }
  }

  //api post disposisi
  static Future<bool> postDisposisiMail(String token, Surat data)async{
    try {
      HttpClient client = new HttpClient();
    client.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    print("masuk api");
    List<Map<String,String>> isi = [{
      'tujuan': data.namaTujuan.toString(),
      'sifat': data.sifatSurat.toString(),
      'disposisi': data.disposisi.toString(),
      'catatanDisposisi': data.catatanDisposisi.toString(),
      'tanggalAkhirDisposisi': data.tanggalSurat.toString()
    }];
    print(isi);
    print(jsonEncode(isi));
    var respond = await ioClient.post(Uri.parse(_apiPostDisposisi+data.idSurat.toString()),body: jsonEncode(isi),headers: {
      "Authorization": "Bearer "+token,
      "content-type": "application/json"
    });
    if(respond.statusCode == 200){
      return true;
    } else {
      return false;
    }
    } catch (e) {
      return false;
    }
    
  }
}
