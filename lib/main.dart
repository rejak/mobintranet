import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mobintranet/SplashScreen.dart';
import 'package:mobintranet/bloc/bloc_login/bloc_login_bloc.dart';
import 'package:mobintranet/login.dart';
import 'package:mobintranet/page/pesan.dart';
import 'package:mobintranet/splash.dart';
import 'package:mobintranet/bloc/main_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:imei_plugin/imei_plugin.dart';
import 'locator.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  runApp(EasyLocalization(
      startLocale: Locale('id', 'ID'),
      supportedLocales: [Locale('id', 'ID'), Locale('en', 'US')],
      path: 'assets/language', // <-- change the path of the translation files
      fallbackLocale: Locale('id', 'ID'),
      child: MyApp()));
// void main() => runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

// This widget is the root of your application.
class _MyAppState extends State<MyApp> {
  
  bool _isCreatingLink = false;
  bool dynamicLink = false;
  String _linkMessage;
  @override
  void initState() {
    super.initState();
    mainBloc = MainBloc();
    initDynamicLinks();
    getIMEI();
  }

  void getIMEI()async{
    String imei = await ImeiPlugin.getImei();
    List<String> multiImei = await ImeiPlugin.getImeiMulti(); //for double-triple SIM phones
    String uuid = await ImeiPlugin.getId();
    print("imeii");
    print(uuid);
    print(imei);
    print(multiImei);
  }

  void initDynamicLinks() async {
    final PendingDynamicLinkData data =
        await FirebaseDynamicLinks.instance.getInitialLink();
    final Uri deepLink = data?.link;
    print("cek link 1");
    print(deepLink);

    if (deepLink != null) {
      print("cek link 2");
      setState(() {
        dynamicLink =true;
      });
      // Navigator.pushNamed(context, deepLink.path);
    } else {
      setState(() {
        print("else link null");
      });
    }

    // FirebaseDynamicLinks.instance.onLink(
    //     onSuccess: (PendingDynamicLinkData dynamicLink) async {
    //   final Uri deepLink = dynamicLink?.link;
    //   print("cek link");
    //   print(deepLink);
    //   if (deepLink != null) {
    //     print("Cek link 3");
    //     // Navigator.push(
    //     //     context,
    //     //     MaterialPageRoute(
    //     //         builder: (context) => PageSurat(
    //     //               indexPage: 1,
    //     //             )));
    //     // Navigator.pushNamed(context, deepLink.path);
    //   }
    // }, onError: (OnLinkErrorException e) async {
    //   print('onLinkError');
    //   print(e.message);
    // });
  }

  void linkPage(){
    print("link page");
    Navigator.pop(context);
    Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => PageSurat(
                      indexPage: 1,
                    )));
  }

  Future<void> _createDynamicLink(bool short) async {
    setState(() {
      _isCreatingLink = true;
    });

    final DynamicLinkParameters parameters = DynamicLinkParameters(
      uriPrefix: 'https://mobintranet.page.link/create-first-post',
      link: Uri.parse('https://www.google.com'),
      androidParameters: AndroidParameters(
        packageName: 'io.flutter.plugins.firebasedynamiclinksexample',
        minimumVersion: 0,
      ),
      dynamicLinkParametersOptions: DynamicLinkParametersOptions(
        shortDynamicLinkPathLength: ShortDynamicLinkPathLength.short,
      ),
      iosParameters: IosParameters(
        bundleId: 'com.google.FirebaseCppDynamicLinksTestApp.dev',
        minimumVersion: '0',
      ),
    );

    Uri url;
    if (short) {
      final ShortDynamicLink shortLink = await parameters.buildShortLink();
      url = shortLink.shortUrl;
    } else {
      url = await parameters.buildUrl();
    }

    setState(() {
      _linkMessage = url.toString();
      _isCreatingLink = false;
    });
  }

  @override
  void dispose() {
    mainBloc = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Mobintranet',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MultiBlocProvider(providers: [
        BlocProvider<BlocLoginBloc>(
          create: (BuildContext context) => BlocLoginBloc(),
        )
      ], child: ShSplashScreen(statusLink: dynamicLink,)),
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      initialRoute: '/',
      routes: {'/login': (context) => Login()},
    );
  }
}
