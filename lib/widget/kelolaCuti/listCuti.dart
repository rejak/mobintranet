// import 'package:flutter/material.dart';
// import 'package:mobintranet/page/sdm/OperasiCuti/editCuti.dart';
// import 'package:mobintranet/util/colors.dart';
// import 'package:mobintranet/util/constant.dart';
// import 'package:mobintranet/util/models/KeloladCutiModels.dart';

// import '../WLogin.dart';
// import 'package:flutter_localizations/flutter_localizations.dart';
// import 'package:easy_localization/easy_localization.dart';


// class T1ListItem extends StatelessWidget {
//   T1Model model;
//   int pos;

//   T1ListItem(T1Model model, int pos) {
//     this.model = model;
//     this.pos = pos;
//   }

//   @override
//   Widget build(BuildContext context) {
//     var dataLanguage = EasyLocalizationProvider.of(context).data;
//     var width = MediaQuery.of(context).size.width;
//     return EasyLocalizationProvider(
//       data: dataLanguage,
//           child: Scaffold(
//                       body: Padding(
//             padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
//             child: Card(
//               shadowColor: Colors.black,
//               elevation: 8,
//               clipBehavior: Clip.antiAlias,
//               shape:
//                   RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
//               child: Container(
//                 decoration: boxDecoration(radius: 10, showShadow: true),
//                 child: Stack(
//                   children: <Widget>[
//                     new Positioned(
//                       top: -65.0,
//                       right: -20.0,
//                       height: 440,
//                       child: new Image(
//                         image: new AssetImage('assets/sdm/bubble-3.png'),
//                       ),
//                     ),
//                     new Positioned(
//                       bottom: -20.0,
//                       height: 140,
//                       right: -40.0,
//                       child: new Image(
//                           image: new AssetImage('assets/sdm/kalender1.png')),
//                     ),
//                     Container(
//                       padding: EdgeInsets.all(16),
//                       child: Column(
//                         children: <Widget>[
//                           // Row(
//                           //   children: <Widget>[
//                           //     ClipRRect(
//                           //       child: CachedNetworkImage(
//                           //         imageUrl: model.img,
//                           //         width: width / 5.5,
//                           //         height: width / 6,
//                           //       ),
//                           //       borderRadius: BorderRadius.circular(12),
//                           //     ),
//                           //     Expanded(
//                           //       child: Container(
//                           //         padding: EdgeInsets.only(left: 16),
//                           //         child: Column(
//                           //           crossAxisAlignment: CrossAxisAlignment.start,
//                           //           children: <Widget>[
//                           //             Row(
//                           //               mainAxisAlignment:
//                           //                   MainAxisAlignment.spaceBetween,
//                           //               children: <Widget>[
//                           //                 text(model.name,
//                           //                     textColor: t1TextColorPrimary,
//                           //                     fontFamily: fontBold,
//                           //                     fontSize: textSizeNormal,
//                           //                     maxLine: 2),
//                           //                 text(model.duration,
//                           //                     fontSize: textSizeMedium),
//                           //               ],
//                           //             ),
//                           //             text(model.designation,
//                           //                 fontSize: textSizeLargeMedium,
//                           //                 textColor: t1TextColorPrimary,
//                           //                 fontFamily: fontMedium),
//                           //           ],
//                           //         ),
//                           //       ),
//                           //     )
//                           //   ],
//                           //   mainAxisAlignment: MainAxisAlignment.start,
//                           // ),
//                           // SizedBox(
//                           //   height: 16,
//                           // ),

//                           // menampilkan tanggal cuti
//                           Row(
//                             children: [
//                               SizedBox(
//                                 width: 100,
//                                 child: text(tr(
//                                                 'tab_service.configure_time_leave.search'),
//                                     fontSize: textSizeMedium,
//                                     maxLine: 2,
//                                     textColor: t1TextColorPrimary),
//                               ),
//                               SizedBox(
//                                 width: 10,
//                               ),
//                               text(":",
//                                   fontSize: textSizeMedium,
//                                   maxLine: 2,
//                                   textColor: t1TextColorPrimary),
//                               SizedBox(
//                                 width: 10,
//                               ),
//                               Container(
//                                 child: Container(
//                                   child: Expanded(
//                                     child: text("6-12-2020" + " s/d " + "8-12-2020",
//                                         fontSize: textSizeMedium,
//                                         maxLine: 2,
//                                         textColor: t1TextColorPrimary),
//                                   ),
//                                 ),
//                               ),
//                             ],
//                           ),
//                           // menampilkan alasan cuti
//                           Row(
//                             children: [
//                               SizedBox(
//                                 width: 100,
//                                 child: text("Alasan Cuti",
//                                     fontSize: textSizeMedium,
//                                     maxLine: 2,
//                                     textColor: t1TextColorPrimary),
//                               ),
//                               SizedBox(
//                                 width: 10,
//                               ),
//                               text(":",
//                                   fontSize: textSizeMedium,
//                                   maxLine: 2,
//                                   textColor: t1TextColorPrimary),
//                               SizedBox(
//                                 width: 10,
//                               ),
//                               Container(
//                                 child: Expanded(
//                                   child: text("Pulang ke kampung halaman",
//                                       fontSize: textSizeMedium,
//                                       maxLine: 2,
//                                       textColor: t1TextColorPrimary),
//                                 ),
//                               )
//                             ],
//                           ),
//                           // menampilkan status cuti
//                           Row(
//                             children: [
//                               SizedBox(
//                                 width: 100,
//                                 child: text("Status Cuti",
//                                     fontSize: textSizeMedium,
//                                     maxLine: 2,
//                                     textColor: t1TextColorPrimary),
//                               ),
//                               SizedBox(
//                                 width: 10,
//                               ),
//                               text(":",
//                                   fontSize: textSizeMedium,
//                                   maxLine: 2,
//                                   textColor: t1TextColorPrimary),
//                               SizedBox(
//                                 width: 10,
//                               ),
//                               text("Diizinkan",
//                                   fontSize: textSizeMedium,
//                                   maxLine: 2,
//                                   textColor: t1TextColorPrimary),
//                             ],
//                           ),
//                           //button
//                           Padding(
//                             padding: EdgeInsets.fromLTRB(
//                                 spacing_standard_new, spacing_middle, 0, 0),
//                             child: Row(
//                               mainAxisAlignment: MainAxisAlignment.start,
//                               children: [
//                                 InkWell(
//                                   onTap: () {
//                                     return Navigator.push(
//                                         context,
//                                         MaterialPageRoute(
//                                             builder: (context) => EditCuti()));
//                                   },
//                                   child: Row(
//                                     children: [
//                                       Icon(
//                                         Icons.edit,
//                                         color: Colors.orange,
//                                       ),
//                                       Text("Edit")
//                                     ],
//                                   ),
//                                 ),
//                                 SizedBox(width: 50),
//                                 InkWell(
//                                   onTap: () {},
//                                   child: Row(
//                                     children: [
//                                       Icon(
//                                         Icons.delete_forever,
//                                         color: Colors.red,
//                                       ),
//                                       Text("Hapus")
//                                     ],
//                                   ),
//                                 )
//                               ],
//                             ),
//                           )
//                         ],
//                       ),
//                     ),
//                     Container(
//                       width: 4,
//                       height: 35,
//                       margin: EdgeInsets.only(top: 16),
//                       color: pos % 2 == 0 ? t1TextColorPrimary : t1_colorPrimary,
//                     )
//                   ],
//                 ),
//               ),
//             )),
//           ),
//     );
//   }
// }