import 'package:flutter/material.dart';

class Tanggal{

  String hariini(){
    DateTime date = DateTime.now();
    var hari; var tgl = date.day; var bln; var thn=date.year;
    hari=date.weekday;
    //nama hari
    if(hari==1){hari= "Senin";}
    if(hari==2){hari= "Selasa";}
    if(hari==3){hari= "Rabu";}
    if(hari==4){hari= "Kamis";}
    if(hari==5){hari= "Jumat";}
    if(hari==6){hari= "Sabtu";}
    if(hari==7){hari= "Minggu";}

    //bln
    bln=date.month;
    if(bln==1){bln= "Jan";}
    if(bln==2){bln= "Feb";}
    if(bln==3){bln= "Mar";}
    if(bln==4){bln= "Apr";}
    if(bln==5){bln= "Mei";}
    if(bln==6){bln= "Jun";}
    if(bln==7){bln= "Jul";}
    if(bln==8){bln= "Agu";}
    if(bln==9){bln= "Sep";}
    if(bln==10){bln= "Okt";}
    if(bln==11){bln= "Nop";}
    if(bln==12){bln= "Des";}

    return "${hari}, ${tgl} ${bln} ${thn} ";

  }

  String tgl(var tanggal){
    DateTime date = DateTime.parse(tanggal+' 00:00:00.00');

    var hari; var tgl = date.day; var bln; var thn=date.year;
    hari=date.weekday;
    //nama hari
    if(hari==1){hari= "Senin";}
    if(hari==2){hari= "Selasa";}
    if(hari==3){hari= "Rabu";}
    if(hari==4){hari= "Kamis";}
    if(hari==5){hari= "Jumat";}
    if(hari==6){hari= "Sabtu";}
    if(hari==7){hari= "Minggu";}

    //bln
    bln=date.month;
    if(bln==1){bln= "Jan";}
    if(bln==2){bln= "Feb";}
    if(bln==3){bln= "Mar";}
    if(bln==4){bln= "Apr";}
    if(bln==5){bln= "Mei";}
    if(bln==6){bln= "Jun";}
    if(bln==7){bln= "Jul";}
    if(bln==8){bln= "Agu";}
    if(bln==9){bln= "Sep";}
    if(bln==10){bln= "Okt";}
    if(bln==11){bln= "Nop";}
    if(bln==12){bln= "Des";}

    return "${tgl} ${bln} ${thn} ";

  }

  tglabsen(String tanggal){
    var string = tanggal.split('-');
    String har= string[0];
    String bul= string[1];
    if(har.length==1){har='0'+har;};
    if(bul.length==1){bul='0'+bul;};
    String tang= string[2]+'-'+bul+'-'+har;
    DateTime date = DateTime.parse(tang+' 00:00:00.00');

    var hari; var tgl = date.day; var bln; var thn=string[2];
    hari=date.weekday;
    //nama hari
    if(hari==1){hari= "Senin";}
    if(hari==2){hari= "Selasa";}
    if(hari==3){hari= "Rabu";}
    if(hari==4){hari= "Kamis";}
    if(hari==5){hari= "Jumat";}
    if(hari==6){hari= "Sabtu";}
    if(hari==7){hari= "Minggu";}

    //bln
    bln=date.month;
    if(bln==1){bln= "Jan";}
    if(bln==2){bln= "Feb";}
    if(bln==3){bln= "Mar";}
    if(bln==4){bln= "Apr";}
    if(bln==5){bln= "Mei";}
    if(bln==6){bln= "Jun";}
    if(bln==7){bln= "Jul";}
    if(bln==8){bln= "Agu";}
    if(bln==9){bln= "Sep";}
    if(bln==10){bln= "Okt";}
    if(bln==11){bln= "Nop";}
    if(bln==12){bln= "Des";}

    return "${tgl} ${bln} ${thn} ";
  }
}