import 'package:flutter/material.dart';

class MyClipper extends CustomClipper<Path> {

  @override
  Path getClip(Size size) {
    var path = Path();
    path.lineTo(0, size.height-(size.height-5));
    path.lineTo(size.width-5, 5);
    path.lineTo(size.width-5, size.width);
    path.lineTo(size.width, size.width);
    path.lineTo(size.width, 0);
    // path.lineTo(0, 100);
    // path.lineTo(0, 0);
    // path.lineTo(size.height-90, size.height-90);
    // path.lineTo(size.width, 10);
    // path.lineTo(100, 0);
    // path.lineTo(90, 0);
    // path.lineTo(size.width/2, size.height-50);
    // path.lineTo(size.width-(size.width/4), size.height);
    // path.lineTo(size.width, size.height-50);
    // path.lineTo(size.width, 0);
    // path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}