import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mobintranet/model/Db8Model.dart';
import 'package:mobintranet/util/DbColors.dart';
import 'package:mobintranet/util/DbImages.dart';

Color primaryGreen = Color(0xff416d6d);
List<BoxShadow> shadowList = [
  BoxShadow(color: Colors.grey[300], blurRadius: 30, offset: Offset(0, 10))
];

List<Map> categories = [
  {'name': 'Surat Masuk', 'iconPath': 'images/surat/drawer/mail-symbol.svg'},
  {'name': 'Surat Keluar', 'iconPath': 'images/surat/drawer/email-outbox.svg'},
  {'name': 'Respond', 'iconPath': 'images/surat/drawer/reply-email.svg'},
  {'name': 'Disposisi', 'iconPath': 'images/surat/drawer/letter.svg'},
];

List<MenuSurat> drawerItems(){
  List<MenuSurat> categories = List<MenuSurat>();
  categories.add(MenuSurat(inbox,"Surat Masuk"));
  categories.add(MenuSurat(outbox,"Surat Keluar"));
  categories.add(MenuSurat(respond, "Respond"));
  categories.add(MenuSurat(disposisi,"Disposisi"));
  return categories;
}