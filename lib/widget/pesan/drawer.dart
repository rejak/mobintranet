import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:geolocator/geolocator.dart';
import 'package:mobintranet/model/Db8Model.dart';
import 'package:mobintranet/model/user.dart';
import 'package:mobintranet/page/pesan.dart';
import 'package:mobintranet/page/surat/ResponDisposisi.dart';
import 'package:mobintranet/page/surat/disposisi.dart';
import 'package:mobintranet/page/surat/kotakkeluar.dart';
import 'package:mobintranet/page/surat/kotakmasuk.dart';
import 'package:mobintranet/util/DbColors.dart';
import 'package:mobintranet/util/DbImages.dart';
import 'package:mobintranet/widget/pesan/cliper1.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'configuration.dart';

class Drawer2 extends StatefulWidget {
  final int currentPage;
  Drawer2({@required this.currentPage});
  @override
  _Drawer2State createState() => _Drawer2State(currentPage: this.currentPage);
}

class _Drawer2State extends State<Drawer2> with SingleTickerProviderStateMixin {
  String nama = "";
  String jabatan = "";
  final int currentPage;
  _Drawer2State({@required this.currentPage});
  final _layoutPage = [
    KotakMasuk(),
    KotakKeluar(),
    Disposisi(),
    ResponDisposisi()
  ];
  List<MenuSurat> listMenu;
  AnimationController animController;
  Animation animation;

  void getDataProfil() async {
    final prefs = await SharedPreferences.getInstance();
    var isi = jsonDecode(prefs.getString("dataProfil"));
    var data = User.fromJson(isi);
    setState(() {
      nama = data.nama;
      jabatan = data.jabatan;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getDataProfil();
    listMenu = drawerItems();
    print("coba");
    print(listMenu[0].title);
    print(currentPage);

    animController =
        AnimationController(duration: Duration(seconds: 2), vsync: this);
    animation = Tween<double>(begin: 0, end: 5).animate(animController)
      ..addListener(() {})
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          animController.reverse();
        } else if (status == AnimationStatus.dismissed) {
          animController.forward();
        }
      });
    animController.forward();
  }

  @override
  void dispose() {
    animController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    print(width);
    return Container(
      color: Colors.white,
      padding: EdgeInsets.only(top: 0, left: 0),
      child: Stack(
        children: [
          AnimatedBuilder(
            animation: animation,
            builder: (BuildContext context, Widget child) {
              return Positioned(
                left: 0,
                bottom: animation.value,
                width: width / 2,
                height: width / 2,
                child: Container(
                    color: Colors.white,
                    child: Padding(
                      padding: EdgeInsets.all(10),
                      child: Stack(
                        children: <Widget>[
                          Positioned(
                            top: 10,
                            right: 20,
                            child: ClipPath(
                              clipper: MyClipper(),
                              child: Opacity(
                                opacity: 0.1,
                                child: Container(
                                  width: 90,
                                  height: 90,
                                  color: fbiru2,
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            top: 30,
                            right: 40,
                            child: ClipPath(
                              clipper: MyClipper(),
                              child: Opacity(
                                opacity: 0.1,
                                child: Container(
                                  width: 70,
                                  height: 70,
                                  color: fbiru2,
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            top: 50,
                            right: 60,
                            child: ClipPath(
                              clipper: MyClipper(),
                              child: Opacity(
                                opacity: 0.1,
                                child: Container(
                                  width: 50,
                                  height: 50,
                                  color: fbiru2,
                                ),
                              ),
                            ),
                          ),
                          Container(
                            child: GridView.builder(
                                itemCount: 210,
                                scrollDirection: Axis.horizontal,
                                shrinkWrap: true,
                                gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 15,
                                  crossAxisSpacing: 10,
                                  mainAxisSpacing: 10,
                                ),
                                itemBuilder: (context, index) {
                                  return Opacity(
                                    opacity: 0.2,
                                    child: Container(
                                      padding: EdgeInsets.all(10),
                                      color: fbiru1,
                                    ),
                                  );
                                }),
                          )
                        ],
                      ),
                    )),
              );
            },
          ),
          AnimatedBuilder(
            animation: animation,
            builder: (BuildContext context, Widget child) {
              return Positioned(
                left: animation.value + 10,
                top: -50,
                width: width / 2,
                height: width / 2,
                child: Container(
                    color: Colors.white,
                    child: Padding(
                      padding: EdgeInsets.all(10),
                      child: Stack(
                        children: <Widget>[
                          Container(
                            child: GridView.builder(
                                itemCount: 210,
                                scrollDirection: Axis.horizontal,
                                shrinkWrap: true,
                                gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 15,
                                  crossAxisSpacing: 10,
                                  mainAxisSpacing: 10,
                                ),
                                itemBuilder: (context, index) {
                                  return Opacity(
                                    opacity: 0.2,
                                    child: Container(
                                      padding: EdgeInsets.all(10),
                                      color: fbiru1,
                                    ),
                                  );
                                }),
                          )
                        ],
                      ),
                    )),
              );
            },
          ),
          Positioned(
            left: -width,
            bottom: -100,
            width: width * 3.8,
            height: height * 1.9,
            child: Opacity(
              opacity: 0.1,
              child: Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image:
                            AssetImage("assets/login/vektorMenuSurat2.png"))),
              ),
            ),
          ),
          Positioned(
            // top: 95,
            bottom: -90,
            left: -140,
            child: Opacity(
              opacity: 1,
              child: Container(
                width: width * 0.8,
                height: width * 0.5,
                decoration: BoxDecoration(boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ], shape: BoxShape.circle, color: fbiru1.withOpacity(0.7)),
              ),
            ),
          ),
          Positioned(
            left: 10,
            top: 50,
            child: Row(
              children: [
                SvgPicture.asset(profil,
                    color: db8_black, width: 24, height: 24),
                SizedBox(
                  width: 10,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '$nama',
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 16),
                    ),
                    Text('$jabatan',
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.normal))
                  ],
                )
              ],
            ),
          ),
          Positioned(
            left: 10,
            top: (height / 6) + 50,
            child: Row(
              children: [
                InkWell(
                  onTap: () {
                    if (currentPage == 0) {
                    } else {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => PageSurat(
                                    indexPage: 0,
                                  )));
                    }
                  },
                  child: Row(
                    children: [
                      SvgPicture.asset(listMenu[0].icon,
                          color: (currentPage == 0)
                              ? Colors.blueGrey
                              : Colors.black,
                          width: 24,
                          height: 24),
                      SizedBox(
                        width: 10,
                      ),
                      Text(listMenu[0].title,
                          style: TextStyle(
                              color: (currentPage == 0)
                                  ? Colors.blueGrey
                                  : Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 20))
                    ],
                  ),
                )
              ],
            ),
          ),
          Positioned(
            left: 10,
            top: (height / 4.5) + 50,
            child: Row(
              children: [
                InkWell(
                  onTap: () {
                    if (currentPage == 1) {
                    } else {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => PageSurat(
                                    indexPage: 1,
                                  )));
                    }
                  },
                  child: Row(
                    children: [
                      SvgPicture.asset(listMenu[1].icon,
                          color: (currentPage == 1)
                              ? Colors.blueGrey
                              : Colors.black,
                          width: 24,
                          height: 24),
                      SizedBox(
                        width: 10,
                      ),
                      Text(listMenu[1].title,
                          style: TextStyle(
                              color: (currentPage == 1)
                                  ? Colors.blueGrey
                                  : Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 20))
                    ],
                  ),
                )
              ],
            ),
          ),
          Positioned(
            left: 10,
            top: (height / 3.5) + 50,
            child: Row(
              children: [
                InkWell(
                  onTap: () {
                    if (currentPage == 2) {
                    } else {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => PageSurat(
                                    indexPage: 2,
                                  )));
                    }
                  },
                  child: Row(
                    children: [
                      SvgPicture.asset(listMenu[2].icon,
                          color: (currentPage == 2)
                              ? Colors.blueGrey
                              : Colors.black,
                          width: 24,
                          height: 24),
                      SizedBox(
                        width: 10,
                      ),
                      Text(listMenu[2].title,
                          style: TextStyle(
                              color: (currentPage == 2)
                                  ? Colors.blueGrey
                                  : Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 20))
                    ],
                  ),
                )
              ],
            ),
          ),
          Positioned(
            left: 10,
            top: (height / 2.9) + 50,
            child: Row(
              children: [
                InkWell(
                  onTap: () {
                    if (currentPage == 3) {
                    } else {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => PageSurat(
                                    indexPage: 3,
                                  )));
                    }
                  },
                  child: Row(
                    children: [
                      SvgPicture.asset(listMenu[3].icon,
                          color: (currentPage == 3)
                              ? Colors.blueGrey
                              : Colors.black,
                          width: 24,
                          height: 24),
                      SizedBox(
                        width: 10,
                      ),
                      Text(listMenu[3].title,
                          style: TextStyle(
                              color: (currentPage == 3)
                                  ? Colors.blueGrey
                                  : Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 20)),
                      // Icon(Icons.arrow_forward_ios)
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
