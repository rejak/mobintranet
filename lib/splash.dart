import 'dart:convert';

import 'package:mobintranet/bloc/blocdata.dart';
import 'package:mobintranet/bloc/main_bloc.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:mobintranet/mainpage.dart';
import 'package:mobintranet/login.dart';
import 'package:mobintranet/api/manageapi.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
  
}

class _SplashState extends State<Splash> {
  String token;
  String kode;
  bool status;
  @override
  void initState() {
    super.initState();
    cekstatuslogin().then((val){
    if(val){
    API.cektoken(token).then((valu){
      if(valu){
        setState(() {
        status=val;
        print("status ok");
        });
      }else{
        setState(() {
        status=false;
        print("status expired");
        });
        
      }
    });
    }else{
      setState(() {
        status=false;
        print("status gagal");
      });
        }
      
    });
   
    
  }  
  
  Future<bool> cekstatuslogin() async {
    final prefs = await SharedPreferences.getInstance();
    bool loginStatus = prefs.getBool('login') ?? false;
    String tokens= prefs.getString('isitoken') ?? "tidak ada isii";
    String code = prefs.getString('user') ;
    print(tokens);
    if(loginStatus){
      blocdata = BlocData();
      API.getUser(tokens).then((res){
          var isi= jsonDecode(res.body);
            print(isi['nip']);
            API.getStswork(isi['nip']).then((res){
              print(res.body);
              var i= jsonDecode(res.body);
              var data=i[0];
              print(data['STATUS_HADIR']);
              blocdata.setstatuswork(data['STATUS_HADIR']);
            });
        });
      
      API.getlaporan(tokens).then((res){
            DateTime masuk;
            DateTime keluar;
            if(res.absenMasuk!=null){
              masuk=DateTime.parse(res.absenMasuk).toLocal();
            blocdata.setjammasuk("${masuk.hour}:${masuk.minute}:${masuk.second}"); }
            else{
              blocdata.setjammasuk("-");
            }

            if(res.absenPulang!=null){
              keluar=DateTime.parse(res.absenPulang).toLocal();
            blocdata.setjamkeluar("${keluar.hour}:${keluar.minute}:${keluar.second}"); }
            else{
              blocdata.setjamkeluar("-");
            }
            print("masukkk");
            print(blocdata.jammasuk);
        });

    }
    setState(() {
     token=tokens; 
     kode = code;
    });
    print('login status: ${loginStatus}');
    return loginStatus;
  }

  @override
  Widget build(BuildContext context) {
    return SplashScreen(
      seconds: 2,
      navigateAfterSeconds: status!=true?Login():MainPage(token: token,kode: kode,) ,
      title: new Text('Intranet LAN',
      style: new TextStyle(
        color: Colors.white,
        fontWeight: FontWeight.bold,
        fontSize: 30.0,
        fontFamily: "Poppins",
      ),),

      backgroundColor:Color.fromRGBO(0, 131, 255, 1),
      styleTextUnderTheLoader: new TextStyle(),
      photoSize: 100.0,
      onClick: ()=>print("Flutter Egypt"),
      loaderColor: Colors.white
    );
  }
}
