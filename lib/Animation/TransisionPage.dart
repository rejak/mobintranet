import 'package:flutter/material.dart';
import 'package:mobintranet/mainpage.dart';
import 'package:mobintranet/util/Db8Widget.dart';
import 'package:mobintranet/util/DbColors.dart';
import 'package:mobintranet/util/DbConstant.dart';
import 'package:mobintranet/util/images.dart';
import 'package:page_transition/page_transition.dart';
import 'package:easy_localization/easy_localization.dart';

import 'fade_animation.dart';

class PageTransit extends StatefulWidget {
  PageTransit(
      {this.buttonController,
      this.token,
      this.kode,
      this.jenisKelamin,
      this.username});
  final AnimationController buttonController;
  final String username;
  final String jenisKelamin;
  final String token;
  final String kode;
  @override
  _PageTransitState createState() => _PageTransitState(
      buttonController: buttonController,
      token: token,
      kode: kode,
      jenisKelamin: jenisKelamin,
      username: username);
}

class _PageTransitState extends State<PageTransit>
    with TickerProviderStateMixin {
  final String jenisKelamin;
  final String username;
  final String token;
  final String kode;
  _PageTransitState(
      {this.buttonController,
      this.token,
      this.kode,
      this.jenisKelamin,
      this.username});
  PageController _pageController;

  // AnimationController rippleController;
  AnimationController buttonController;

  // Animation<double> rippleAnimation;
  Animation<double> scaleAnimation;

  @override
  void initState() {
    print("siniii page");
    super.initState();
    print(widget.jenisKelamin);
    print(username);
    print(jenisKelamin);
    _pageController = PageController(initialPage: 0);

    // rippleController = AnimationController(
    //   vsync: this,
    //   duration: Duration(seconds: 1)
    // );

    buttonController =
        AnimationController(vsync: this, duration: Duration(seconds: 3))
          ..addStatusListener((status) {
            if (status == AnimationStatus.completed) {
              Navigator.of(context).pop(true);
              Navigator.push(
                context,
                PageTransition(
                    // duration: Duration(milliseconds: 1000),
                    type: PageTransitionType.size,
                    alignment: Alignment.bottomCenter,
                    child: MainPage(
                      token: token.toString(),
                      kode: kode.toString(),
                    )),
              );
            }
          });

    // rippleAnimation = Tween<double>(
    //   begin: 80.0,
    //   end: 90.0
    // ).animate(rippleController)..addStatusListener((status) {
    //   if (status == AnimationStatus.completed) {
    //     rippleController.reverse();
    //   } else if(status == AnimationStatus.dismissed) {
    //     rippleController.forward();
    //   }
    // });

    scaleAnimation =
        Tween<double>(begin: 1.0, end: 50.0).animate(CurvedAnimation(
            parent: buttonController,
            curve: Interval(
              0.550,
              0.999,
              curve: Curves.bounceInOut,
            )));

    // rippleController.forward();
    buttonController.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnimatedBuilder(
          animation: scaleAnimation,
          builder: (context, child) => (scaleAnimation.value <= 1)
              ? Transform.scale(
                  scale: scaleAnimation.value,
                  child: Container(
                    margin: EdgeInsets.all(10),
                    decoration:
                        BoxDecoration(shape: BoxShape.circle, color: fbiru2),
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          FadeAnimation(
                            0.10,
                            text(
                                tr('title'),
                                textColor: db8_white,
                                fontSize: textSizeNormal),
                          ),
                          FadeAnimation(
                            0.10,
                            text(
                                (jenisKelamin == "L")
                                    ? tr('gender_male') +
                                        " $username"
<<<<<<< HEAD
                                    : tr('gender_female') +
=======
                                    : tr('gander_female') +
>>>>>>> 3587862f399c6426aebd8a0e127b13be70ae5a32
                                        " $username",
                                textColor: db8_white,
                                fontSize: textSizeNormal),
                          ),
                          FadeAnimation(
                            0.8,
                            Image.asset(
                              ic_app_icon2,
                              width: 150,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              : Transform.scale(
                  scale: scaleAnimation.value,
                  child: Container(
                    margin: EdgeInsets.all(10),
                    decoration:
                        BoxDecoration(shape: BoxShape.circle, color: fbiru2),
                  ),
                )),
    );
  }
}
