import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:mobintranet/util/Db8Widget.dart';
import 'package:mobintranet/util/DbColors.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:mobintranet/util/DbConstant.dart';
import 'package:mobintranet/util/colors.dart';

class AnimationButtonLogin extends StatefulWidget {
  AnimationButtonLogin(
      {Key key,
      this.buttonController,
      this.clickCallback,
      this.width,
      this.shrinkButtonAnimation,
      this.zoomAnimation, this.status, this.username, this.password})
      : super(key: key);
  final bool status;
  TextEditingController username = TextEditingController();
  TextEditingController password = TextEditingController();
  final double width;
  final VoidCallback clickCallback;
  final AnimationController buttonController;
  final Animation shrinkButtonAnimation;
  final Animation zoomAnimation;

  @override
  _AnimationButtonLoginState createState() => _AnimationButtonLoginState(
      buttonController: buttonController,
      clickCallback: clickCallback,
      width: width,
      shrinkButtonAnimation: shrinkButtonAnimation,
      zoomAnimation: zoomAnimation);
}

class _AnimationButtonLoginState extends State<AnimationButtonLogin> {
  double width;
  VoidCallback clickCallback;
  AnimationController buttonController;
  Animation<double> shrinkButtonAnimation;
  Animation<double> zoomAnimation;
  _AnimationButtonLoginState(
      {this.buttonController,
      this.clickCallback,
      this.width,
      this.shrinkButtonAnimation,
      this.zoomAnimation});

  @override
  void initState() {
    shrinkButtonAnimation = new Tween(begin: 320.0, end: 70.0).animate(
      CurvedAnimation(parent: buttonController, curve: Interval(0.0, 0.150)),
    );
    // zoomAnimation = new Tween(begin: 70.0, end: 800.0).animate(CurvedAnimation(
    //     parent: buttonController,
    //     curve: Interval(
    //       0.550,
    //       0.999,
    //       curve: Curves.bounceInOut,
    //     )));
    // print(zoomAnimation.value);
    // print("cek value");
    // if(zoomAnimation.value < 70.0){
    //   print("sudah");
    // }else if(zoomAnimation.value==70){
    //   print("belum");
    // }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new AnimatedBuilder(
        animation: buttonController,
        builder: (context, child) => 
        // zoomAnimation.value <= 300
        //     ? 
            Padding(
                padding: EdgeInsets.only(bottom: width / 2),
                child: Container(
                  width: shrinkButtonAnimation.value,
                  height: 50,
                  // height: double.infinity,
                  child: MaterialButton(
                    // padding: EdgeInsets.only(bottom: 60.0),
                    child: shrinkButtonAnimation.value > 75
                        ? Icon(Icons.cancel)
                        : CircularProgressIndicator(
                              strokeWidth: 1.0,
                              valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                              // backgroundColor: Colors.white,
                            ),
                    
                    // ),
                    textColor: sh_white,
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(40.0)),
                    color: fbiru2,
                    onPressed: () {
                      print("woyyy");
                      clickCallback();
                    },
                  ),
                ),
              ));
            // : Container(
            //     width: zoomAnimation.value,
            //     height: zoomAnimation.value,
            //     decoration: BoxDecoration(
            //         shape: zoomAnimation.value < 600
            //             ? BoxShape.circle
            //             : BoxShape.rectangle,
            //         color: fbiru2)));
  }
}
